#include "fivewin.ch"

#define CW_USEDEFAULT      32768
#define SRCCOPY 13369376

#define DT_TOP                      0x00000000
#define DT_LEFT                     0x00000000
#define DT_CENTER                   0x00000001
#define DT_RIGHT                    0x00000002
#define DT_VCENTER                  0x00000004
#define DT_BOTTOM                   0x00000008
#define DT_WORDBREAK                0x00000010
#define DT_SINGLELINE               0x00000020
#define DT_EXPANDTABS               0x00000040
#define DT_TABSTOP                  0x00000080
#define DT_NOCLIP                   0x00000100
#define DT_EXTERNALLEADING          0x00000200
#define DT_CALCRECT                 0x00000400
#define DT_NOPREFIX                 0x00000800
#define DT_INTERNAL                 0x00001000


CLASS TC5Tooltip FROM TWindow

      CLASSDATA lRegistered AS LOGICAL

      DATA lSplitHdr    AS LOGICAL INIT .f.
      DATA lLeft        AS LOGICAL INIT .f.

      DATA lLineHeader  AS LOGICAL INIT .f.
      DATA lLineFoot    AS LOGICAL INIT .F.
      DATA lBorder      AS LOGICAL INIT .t.
      DATA lBtnClose    AS LOGICAL INIT .f.

      DATA cHeader      AS CHARACTER INIT  ""
      DATA cBmpLeft     AS CHARACTER INIT  ""
      DATA cBody        AS CHARACTER INIT  ""
      DATA cBmpFoot     AS CHARACTER INIT  ""
      DATA cFoot        AS CHARACTER INIT  ""
      DATA lRightAlignBody AS LOGICAL INIT .F.

      DATA cLibHeader
      DATA cBmpHeader
      DATA cLibLeft
      DATA cLibFoot
      DATA cTumbNail    AS CHARACTER INIT ""
      DATA cHeader2     AS CHARACTER INIT  space(255)


      DATA nClrPane2
      DATA nClrBorder   AS NUMERIC INIT 0
      DATA nClrSepara1  AS NUMERIC INIT RGB(157,188,219)
      DATA nClrSepara2  AS NUMERIC INIT CLR_WHITE

      DATA nClrTextHeader
      DATA nClrTextBody
      DATA nClrTextFoot

      DATA oFontHdr
      DATA oFontHdr2
      DATA oFontBody
      DATA oFontPie

      DATA aHeader   AS ARRAY INIT {0,0,0,0}
      DATA aHeader2  AS ARRAY INIT {0,0,0,0}
      DATA aBody     AS ARRAY INIT {0,0,0,0}
      DATA aLeft     AS ARRAY INIT {0,0,0,0}
      DATA aRight    AS ARRAY INIT {0,0,0,0}
      DATA aFoot     AS ARRAY INIT {0,0,0,0}
      DATA aBtnClose AS ARRAY INIT {0,0,0,0}

      DATA nWRadio
      DATA nHRadio
      DATA nGetColor

      DATA aOldPos, nOldRow, nOldCol
      DATA hRgn
      DATA nMResize

      DATA bBmpLeft

      DATA nFixWidth
      DATA nFixHeight

      DATA bOwnerDraw

      METHOD New( nTop, nLeft, nWidth, nHeight, oWnd, lDisenio, nClrPane, nClrPane2, nClrText, nWRadio, nHRadio ) CONSTRUCTOR
      METHOD Default ()
      METHOD Destroy()  INLINE ::oFontHdr :End(),;
                               ::oFontHdr2:End(),;
                               ::oFontBody:End(),;
                               ::oFontPie :End(),;
                               DeleteObject( ::hRgn ),;
                               ::Super:Destroy()
      METHOD EndPaint() INLINE ::nPaintCount--,EndPaint( ::hWnd, ::cPS ), ::cPS := nil, ::hDC := nil, 0
      METHOD Display()  INLINE ::BeginPaint(),::Paint(),::EndPaint(),0
      METHOD Paint   ()
      METHOD PaintHdr ( hDC, rc )
      METHOD PaintHdr2( hDC, rc )
      METHOD PaintBody( hDC, rc )
      METHOD PaintFoot( hDC, rc )

      METHOD HandleEvent( nMsg, nWParam, nLParam )
      METHOD ReSize( nSizeType, nWidth, nHeight )
      METHOD lHeader() INLINE !empty( ::cHeader )
      METHOD lFoot()   INLINE !empty( ::cFoot )
      METHOD GetSize()
      METHOD SetSize( nWidth, nHeight ) INLINE ::Super:SetSize( nWidth, nHeight, .t. )

ENDCLASS

*********************************************************************************************************************************
   METHOD New( nTop, nLeft, nWidth, nHeight, oWnd, lDisenio, nClrPane, nClrPane2, nClrText, nWRadio, nHRadio ) CLASS TC5ToolTip
*********************************************************************************************************************************

   DEFAULT nClrPane  := CLR_WHITE
   DEFAULT nClrPane2 := nClrPane
   DEFAULT nClrText  := 0
   DEFAULT nWRadio   := 2
   DEFAULT nHRadio   := 2

   ::oWnd        := oWnd
   ::nStyle      := nOR( WS_POPUP, WS_VISIBLE, WS_BORDER )
   ::nTop        := nTop
   ::nLeft       := nLeft
   ::nBottom     := nTop + nHeight
   ::nRight      := nLeft + nWidth

   ::nClrPane    := nClrPane
   ::nClrPane2   := nClrPane2
   ::nClrText    := nClrText
   ::nClrBorder  := RGB( 118,118,118 )
   ::nWRadio     := nWRadio
   ::nHRadio     := nHRadio

   DEFINE FONT ::oFontHdr   NAME "Verdana" SIZE 0, -11 BOLD
   DEFINE FONT ::oFontHdr2  NAME "Verdana" SIZE 0, -11
   DEFINE FONT ::oFontBody  NAME "Segoe UI" SIZE 0, -11
   DEFINE FONT ::oFontPie   NAME "Verdana" SIZE 0, -11 BOLD

   ::Register( nOR( CS_VREDRAW, CS_HREDRAW ) ) //, 131072

   ::Create()

   ::cTitle    := "Undefined"
   ::hRgn      := nil

   // ::Default(.t.)

return Self

************************************************************************************************************************
 METHOD GetSize() CLASS TC5ToolTip
************************************************************************************************************************

local rc        := 0
local aSize     := { 0, 0 }
local hBmp      := 0
local hDC       := 0
local hOldFont  := 0
local n         := 0
local nHBmp     := 0
local nHText    := 0
local nHeight   := 0
local nLen      := 0
local nW        := 0
local nW2       := 0
local nWB       := 0
local nWBmp     := 0
local nWBodyTxt := 227
local nWF       := 0
local nWH       := 0
local nWidth    := 0

if ::nFixWidth != nil .and. ::nFixHeight != nil
   return {::nFixWidth, ::nFixHeight}
endif

rc        := GetClientRect(::hWnd)
nWidth    := nWBodyTxt

// Cabecera
if !empty( ::cHeader )
   nHeight += 31
   nWH := GetTextWidth( 0, ::cHeader, ::oFontHdr:hFont )+ 16
endif

// Imagen a la izquierda
if !empty( ::cBmpLeft )
   hBmp := LoadImageEx( ::cBmpLeft )
else
   if ::bBmpLeft != nil
      hBmp := eval( ::bBmpLeft, self )
   endif
endif

if hBmp != 0
   nWBmp := BmpWidth ( hBmp )
   nHBmp := BmpHeight( hBmp )
   nWidth += ( 14 + nWBmp )
   DeleteObject( hBmp )
endif

if empty(::cHeader) .and. empty(::cFoot)

   nWidth := 13 + if( nWBmp != 0, (nWBmp+13),0) + GetTextWidth(0,::cBody,::oFontBody:hFont ) + 26

endif

if !empty( ::cFoot )
   nHeight += 30
   nWF := GetTextWidth( 0, ::cFoot, ::oFontPie:hFont ) +22
endif

nWidth :=  max( max( nWidth, nWH ), nWF )

if empty(::cHeader) .and. empty(::cFoot)
   nWidth := min(nWidth,227)
endif

// Si tenemos texto en el cuerpo del tooltip
if !empty( ::cBody )
   hDC      := CreateDC( "DISPLAY",0,0,0)
   hOldFont := SelectObject( hDC, ::oFontBody:hFont )//
   nHText   := DrawText( hDC, alltrim(::cBody), { 0, 12+nWBmp+12+10, 20, nWidth }, nOr(DT_WORDBREAK, 8192, DT_CALCRECT ) )
   //nHText   := DrawText( hDC, alltrim(::cBody), { 0, 6+nWBmp+12+6, 20, nWidth }, nOr(DT_WORDBREAK, 8192, DT_CALCRECT ) )
   SelectObject( hDC, hOldFont )
   DeleteDC( hDC )
   nHeight += nHText
   nHeight+=8
endif

nHeight := max( nHeight, nHBmp )

aSize := { nWidth, nHeight }

return aSize

************************************************************************************************************************
 METHOD Default( lShowDlg ) CLASS TC5ToolTip
************************************************************************************************************************
local rc := {0, 0, ::nHeight, ::nWidth}
Local hRgn
Local hRgn2
Local hRgn3
local o := self

DEFAULT lShowDlg := .F.

   ::hRgn  := CreateRoundRectRgn( rc[2]  ,rc[1], rc[4], rc[3] , ::nWRadio, ::nHRadio )

   SetWindowRgn(::hWnd, ::hRgn, .t. )

   DeleteObject( ::hRgn )

return 0

#define GW_CHILD             5
#define GW_HWNDNEXT          2

************************************************************************************************************************
  METHOD HandleEvent( nMsg, nWParam, nLParam ) CLASS TC5Tooltip
************************************************************************************************************************

  if nMsg == 20
     return 1
  endif

return ::Super:HandleEvent( nMsg, nWParam, nLParam )


************************************************************************************************************************
  METHOD ReSize( nSizeType, nWidth, nHeight ) CLASS TC5Tooltip
************************************************************************************************************************

::Default()
::Refresh()

return ::Super:ReSize( nSizeType, nWidth, nHeight )


************************************************************************************************************************
 METHOD Paint() CLASS TC5ToolTip
************************************************************************************************************************
  local hDCMem   := CreateCompatibleDC    ( ::hDC )
  local rc       := GetClientRect         ( ::hWnd )
  local hBmpMem  := CreateCompatibleBitmap( ::hDC, rc[4]-rc[2], rc[3]-rc[1] )
  local hOldBmp  := SelectObject          ( hDCMem, hBmpMem )
  local nWRadio  := ::nWRadio
  local nHRadio  := ::nHRadio
  local nClrText := SetTextColor( hDCMem, ::nClrText )
  local hBrush
  local hRgn     := CreateRoundRectRgn( rc[2]  ,rc[1], rc[4], rc[3] , ::nWRadio, ::nHRadio )

  rc[3]--; rc[4]--
  nWRadio += 2
  nHRadio += 2

  VerticalGradient( hDCMem, {rc[1]-1,rc[2],rc[3],rc[4]}, ::nClrPane, ::nClrPane2 )

  if ::bOwnerDraw == nil

     ::PaintHdr ( hDCMem, rc )                      //  ::PaintHdr2( hDCMem, rc )
     ::PaintFoot( hDCMem, rc )
     ::PaintBody( hDCMem, rc )

  else

     eval( ::bOwnerDraw, hDCMem )

  endif

  hBrush := CreateSolidBrush( ::nClrBorder )
  FrameRgn( hDCMem, hRgn, hBrush, 1, 1 )
  DeleteObject( hBrush )
  DeleteObject( hRgn )

  SetTextColor( hDCMem, nClrText )

  BitBlt( ::hDC, 0, 0, rc[4]-rc[2], rc[3]-rc[1], hDCMem, 0, 0, SRCCOPY )

  SelectObject( hDCMem, hOldBmp )
  DeleteDC    ( hDCMem )
  DeleteObject( hBmpMem )

return 0


************************************************************************************************************************
   METHOD PaintHdr ( hDC, rc ) CLASS TC5ToolTip
************************************************************************************************************************
local hBmpHdr
local nWBmpHdr := 0
local hOldFont
local nClrText
local lIcon := .f.
local nTop
local nMode

  // 25 pixels
  if ::lHeader

     ::aHeader  := {rc[1],rc[2],rc[1]+25,rc[4]}

     if !empty(::cBmpHeader)
        hBmpHdr := LoadImageEx( ::cBmpHeader )
        if hBmpHdr  != 0
           nWBmpHdr := BmpWidth( hBmpHdr )
           nTop     := (::aHeader[1]+(::aHeader[3]-::aHeader[1])/2)-BmpHeight(hBmpHdr)/2
           nTop     := max( nTop, 5)
           DrawMasked( hDC, hBmpHdr, nTop , 5 )
           DeleteObject( hBmpHdr )
        endif
     endif

     hOldFont := SelectObject( hDC, ::oFontHdr:hFont )
     if ::nClrTextHeader != nil
        nClrText := SetTextColor( hDC, ::nCLrTextHeader )
     endif
     nMode := SetBkMode( hDC, 1 )

     DrawText( hDC, ::cHeader, {::aHeader[1],::aHeader[2]+10+if(hBmpHdr!=0,nWBmpHdr,0),::aHeader[3],::aHeader[4]-10},nOr( DT_VCENTER, DT_SINGLELINE, 8192 ) )

     SetBkMode( hDC, nMode )

     if ::nClrTextHeader != nil
        SetTextColor( hDC, nClrText )
     endif

     SelectObject( hDC, hOldFont )
  else
     ::aHeader  := {rc[1],rc[2],rc[1],rc[4]}
  endif
return 0

************************************************************************************************************************
   METHOD PaintHdr2( hDC, rc ) CLASS TC5ToolTip
************************************************************************************************************************
local hOldFont
local nClrText

  ::aHeader2 := {::aHeader[3],rc[2],::aHeader[3],rc[4]}
  if ::lHeader

     if ::lSplitHdr
        ::aHeader2 := {::aHeader[3],rc[2],::aHeader[3]+25,rc[4]}
     endif

     if ::lLineHeader
        Line( hDC, ::aHeader2[3]  , ::aHeader2[2]+5, ::aHeader2[3]  , ::aHeader2[4]-5, ::nClrSepara1 )
        Line( hDC, ::aHeader2[3]+1, ::aHeader2[2]+5, ::aHeader2[3]+1, ::aHeader2[4]-5, ::nClrSepara2 )
     endif

     hOldFont := SelectObject( hDC, ::oFontHdr2:hFont )
     if ::nClrTextHeader != nil
        nClrText := SetTextColor( hDC, ::nCLrTextHeader )
     endif
     if ::lSplitHdr .and. !empty( ::cHeader2 )
        DrawText( hDC, ::cHeader2, {::aHeader2[1]+1,::aHeader2[2]+20,::aHeader2[3],::aHeader2[4]-2}, nOR(DT_WORDBREAK, 8192) )
     endif
     if ::nClrTextHeader != nil
        SetTextColor( hDC, nClrText )
     endif
     SelectObject( hDC, hOldFont )
  endif

return 0


************************************************************************************************************************
   METHOD PaintBody( hDC, rc ) CLASS TC5ToolTip
************************************************************************************************************************

   local hOldFont
   local nWBmp := 0
   local nClrText
   local lIcon := .f.
   local nMode
   local hBmpLeft := 0
   local n
   local nLen
   local nW
   local nW2
   local aLeft

   ::aLeft := {0,0,0,0}

   ::aBody    := {::aHeader[3],rc[2],::aFoot[1],rc[4]}

   if empty(::cBmpLeft)
      ::aLeft   := {::aBody[1],rc[2],::aBody[3],if(::lLeft,(rc[4]-rc[2])*0.33,rc[2])}
   else
      hBmpLeft := LoadImageEx( ::cBmpLeft )
      if hBmpLeft != 0
         nWBmp := BmpWidth( hBmpLeft )
         ::aLeft   := { ::aBody[1],rc[2],::aBody[3],12+nWBmp+12}
      endif
   endif

   if ::bBmpLeft != nil
      hBmpLeft := eval( ::bBmpLeft, self )
      if hBmpLeft != 0
         nWBmp := BmpWidth( hBmpLeft )
         ::aLeft   := { ::aBody[1],rc[2],::aBody[3],12+nWBmp+12}
      endif
   endif
                                        //
   ::aRight  := {::aBody[1] +3,::aLeft[4]+20,::aBody[3]-3,rc[4]-10}

   hOldFont := SelectObject( hDC, ::oFontBody:hFont )

   nMode := SetBkMode( hDC, 1 )

   DrawText( hDC,alltrim(::cBody), ::aRight, nOr(if( ::lRightAlignBody,DT_RIGHT,DT_LEFT), DT_WORDBREAK ) )

   SetBkMode( hDC, nMode )

   SelectObject( hDC, hOldFont )

   if hBmpLeft != 0
      DrawMasked( hDC, hBmpLeft, ::aLeft[1]+5, ::aLeft[2]+12 )
      DeleteObject( hBmpLeft )
   endif

return 0

************************************************************************************************************************
   METHOD PaintFoot( hDC, rc ) CLASS TC5ToolTip
************************************************************************************************************************
  local hOldFont, hBmpFoot
  local nWFoot := 0
  local nClrText
  local lIcon := .f.
  local nMode

  if ::lFoot
     ::aFoot := {rc[3]-30,rc[2],rc[3],rc[4]}
     if !empty(::cBmpFoot)
        ::aFoot := {rc[3]-30,rc[2],rc[3],rc[4]}
     endif
     hBmpFoot := LoadImageEx( ::cBmpFoot )
     if hBmpFoot != 0
        nWFoot := BmpWidth( hBmpFoot )
        ::aFoot := {rc[3]-30,rc[2],rc[3],rc[4]}
        DrawMasked( hDC, hBmpFoot, (::aFoot[1]+(::aFoot[3]-::aFoot[1])/2)-BmpHeight(hBmpFoot)/2, 5 )
        DeleteObject( hBmpFoot )
     endif
  else
     ::aFoot := {rc[3],rc[2],rc[3],rc[4]}
  endif

  if ::lFoot
     hOldFont := SelectObject( hDC, ::oFontPie:hFont )
     if ::nClrTextFoot != nil
        nClrText := SetTextColor( hDC, ::nClrTextFoot )
     endif
     nMode := SetBkMode( hDC, 1 )
     DrawText( hDC, ::cFoot, {::aFoot[1],::aFoot[2]+ 10+nWFoot,::aFoot[3],::aFoot[4]},nOr( DT_VCENTER, DT_SINGLELINE, 8192 ) )
     SetBkMode( hDC, nMode )
     if ::nClrTextFoot != nil
        SetTextColor( hDC, nClrText )
     endif
     SelectObject( hDC, hOldFont )
  endif

  Line( hDC, ::aFoot[1]  , ::aFoot[2]+5, ::aFoot[1]  , ::aFoot[4]-5, ::nClrSepara1 )
  Line( hDC, ::aFoot[1]+1, ::aFoot[2]+5, ::aFoot[1]+1, ::aFoot[4]-5, ::nClrSepara2 )

return 0

#define WS_EX_LAYERED    524288
#define GWL_EXSTYLE         -20
#define LWA_ALPHA             2

/*
////////////////////////////////////////////////////////////////////
  function LoadImageEx2( cImage, hBmp )
////////////////////////////////////////////////////////////////////
local lIcon := .f.

 hBmp := LoadImageEx( cImage )
 if hBmp == 0
    hBmp := LoadIcon( GetResources(), cImage )
    if hBmp == 0
       hBmp := ExtractIcon( cImage )
       lIcon := hBmp != 0
    else
       lIcon := .t.
    endif
 endif

return lIcon
*/

/***************************************************************************************************************************/
   function Line( hDC, nTop, nLeft, nBottom, nRight, nColor, nGrueso )
/***************************************************************************************************************************/
local hPen, hOldPen
DEFAULT nGrueso := 1
DEFAULT nColor := CLR_BLACK

hPen := CreatePen( PS_SOLID, nGrueso, nColor )
hOldPen := SelectObject( hDC, hPen )
MoveTo( hDC, nLeft,  nTop    )
LineTo( hDC, nRight, nTop    )
SelectObject( hDC, hOldPen )
DeleteObject( hPen )

return 0


#pragma BEGINDUMP
#include "windows.h"
#include "hbapi.h"
#include "hbapiitm.h"

//lngUser32 = LoadLibrary("User32.dll")
//lngSetLayeredWindowAttributes = GetProcAddress(lngUser32, "SetLayeredWindowAttributes")
//If lngSetLayeredWindowAttributes <> 0 Then
//' Set window translucency, if supported
//lngOldStyle = GetWindowLong(hDlg, %GWL_EXSTYLE)
//SetWindowLong hDlg, %GWL_EXSTYLE, lngOldStyle Or %WS_EX_LAYERED
//Call DWord lngSetLayeredWindowAttributes Using SetLayeredWindowAttributes(hDlg, 0, %TRANSLEVEL, %LWA_ALPHA)
//End If

typedef BOOL  ( FAR PASCAL  *LPSETLAYEREDWINDOWATTRIBUTES) ( HWND hWnd, COLORREF crKey, BYTE bAlpha, DWORD dwFlags );

HB_FUNC( SETLAYEREDWINDOWATTRIBUTES )
{

  HINSTANCE hLib;

  LPSETLAYEREDWINDOWATTRIBUTES SetLayeredWindowAttributes;
  hLib = LoadLibrary( "user32.dll" );
  if ( hLib )
  {
      SetLayeredWindowAttributes = (LPSETLAYEREDWINDOWATTRIBUTES) GetProcAddress( hLib, "SetLayeredWindowAttributes" );
      if( SetLayeredWindowAttributes )
      {
          SetLayeredWindowAttributes( ( HWND )     hb_parnl( 1 ),
                                      ( COLORREF ) hb_parnl( 2 ),
                                      ( BYTE )     hb_parni ( 3 ),
                                      ( DWORD )    hb_parnl( 4 ) );
      }
      FreeLibrary( hLib );
  }

  hb_ret();
}


/*
HB_FUNC( CREATERECTRGN )
{
    hb_retnl( (long) CreateRectRgn( hb_parni( 1 ),hb_parni( 2 ),hb_parni( 3 ),hb_parni( 4 )));

}
*/

/*
HB_FUNC( CREATEROUNDRECTRGN )
{
   hb_retnl( (long) CreateRoundRectRgn( hb_parni( 1 ),hb_parni( 2 ),hb_parni( 3 ),hb_parni( 4 ),hb_parni( 5 ),hb_parni( 6 )));
}

HB_FUNC( CREATEELLIPTICRGN )
{
   hb_retnl( (long) CreateEllipticRgn( hb_parni( 1 ),hb_parni( 2 ),hb_parni( 3 ),hb_parni( 4 )));
}

HB_FUNC( COMBINERGN )
{
   HRGN hrgn = CreateRectRgn(0,0,0,0);
   HRGN hrgn1 = (HRGN) hb_parnl( 1 );
   HRGN hrgn2 = (HRGN) hb_parnl( 2 );
   int iRet;

   iRet = CombineRgn( hrgn,      // handle to destination region
                      hrgn1,      // handle to source region
                      hrgn2,      // handle to source region
                      hb_parni( 3 )       // region combining mode
             );


   hb_retni( (long)hrgn );


}
*/

/*
HB_FUNC( SETWINDOWRGN )
{
  hb_retni( SetWindowRgn( (HWND) hb_parnl( 1 ),(HRGN) hb_parnl( 2 ), hb_parl( 3 ) ));
}
*/

HB_FUNC( FRAMERGN )
{
   hb_retl( FrameRgn( (HDC) hb_parnl( 1 ), (HRGN) hb_parnl( 2 ), (HBRUSH) hb_parnl( 3 ), hb_parni( 4 ), hb_parni( 5 )));
}

BOOL Array2Point(PHB_ITEM aPoint, POINT *pt )
{
   if (HB_IS_ARRAY(aPoint) && hb_arrayLen(aPoint) == 2) {
      pt->x = hb_arrayGetNL(aPoint,1);
      pt->y = hb_arrayGetNL(aPoint,2);
      return TRUE ;
   }
   return FALSE;
}

HB_FUNC( CREATEPOLYGONRGN )
{
   POINT * Point ;
   POINT pt ;
   int iCount ;
   int i ;
   PHB_ITEM aParam ;
   PHB_ITEM aSub ;

   if ( hb_param( 1, HB_IT_ARRAY ) != NULL )
   {
       iCount = (int) hb_parinfa( 1, 0 ) ;
       Point = (POINT *) hb_xgrab( iCount * sizeof (POINT) ) ;
       aParam = hb_param(1,HB_IT_ARRAY);

       for ( i = 0 ; i<iCount ; i++ )
       {
          aSub = ( PHB_ITEM ) hb_itemArrayGet( aParam, i+1 );

          if ( Array2Point(aSub, &pt ))
               *(Point+i) = pt ;
          else {
            hb_retnl(0);
            hb_xfree(Point);
            return ;
          }
       }

       hb_retnl( (LONG) CreatePolygonRgn( Point, iCount, hb_parni( 2 ) ) ) ;
       hb_xfree(Point);

   }
   else
    hb_retnl( 0 );

}

HB_FUNC( GETDLGCTRLID )
{
   hb_retni( GetDlgCtrlID( ( HWND ) hb_parnl( 1 ) ) );
}




#pragma ENDDUMP



