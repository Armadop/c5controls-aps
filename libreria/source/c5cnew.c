#include <windows.h>
#include <winuser.h>
#include <wingdi.h>
#include <uxtheme.h>
#include <direct.h>
#include <math.h>
#include <shlobj.h>

#include <hbapi.h>
#include <hbstack.h>

LPWSTR     AnsiToWide( LPSTR cAnsi );
void       C5GradientFill( HDC hDC, RECT rct, COLORREF crStart, COLORREF crEnd, int bVertical );
void       FillSolidRect(HDC hDC, LPCRECT lpRect, COLORREF clr);
extern     HINSTANCE GetInstance( void );
void RegisterResource( HANDLE hRes, LPSTR szType );
static     far char IniDir[] = ".\\";
static     char szDirName[ MAX_PATH ];
HFONT      GetFontMenu( void );
static far char Title[] = "Select the file";

static HCURSOR hC5CursorSep = 0;
static HCURSOR hC5Hand = 0;
static HCURSOR hC5Catch = 0;

static unsigned char XORPlaneSep[128] = {
                0x01, 0xE0, 0x00, 0x00, 0x01, 0x20, 0x00, 0x00, 0x01, 0x20, 0x00, 0x00, 0x01, 0x20, 0x00, 0x00,
                0x01, 0x20, 0x00, 0x00, 0x19, 0x26, 0x00, 0x00, 0x29, 0x25, 0x00, 0x00, 0x4F, 0x3C, 0x80, 0x00,
                0x80, 0x00, 0x40, 0x00, 0x80, 0x00, 0x40, 0x00, 0x4F, 0x3C, 0x80, 0x00, 0x29, 0x25, 0x00, 0x00,
                0x19, 0x26, 0x00, 0x00, 0x01, 0x20, 0x00, 0x00, 0x01, 0x20, 0x00, 0x00, 0x01, 0x20, 0x00, 0x00,
                0x01, 0x20, 0x00, 0x00, 0x01, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
        };

static unsigned char ANDPlaneSep[128] = {
                0xFE, 0x1F, 0xFF, 0xFF, 0xFE, 0x1F, 0xFF, 0xFF, 0xFE, 0x1F, 0xFF, 0xFF, 0xFE, 0x1F, 0xFF, 0xFF,
                0xFE, 0x1F, 0xFF, 0xFF, 0xE6, 0x19, 0xFF, 0xFF, 0xC6, 0x18, 0xFF, 0xFF, 0x80, 0x00, 0x7F, 0xFF,
                0x00, 0x00, 0x3F, 0xFF, 0x00, 0x00, 0x3F, 0xFF, 0x80, 0x00, 0x7F, 0xFF, 0xC6, 0x18, 0xFF, 0xFF,
                0xE6, 0x19, 0xFF, 0xFF, 0xFE, 0x1F, 0xFF, 0xFF, 0xFE, 0x1F, 0xFF, 0xFF, 0xFE, 0x1F, 0xFF, 0xFF,
                0xFE, 0x1F, 0xFF, 0xFF, 0xFE, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
        };

static unsigned char XORPlaneCatch[128] = {
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x1B, 0x60, 0x00, 0x00, 0x1F, 0xE8, 0x00, 0x00, 0x0F, 0xF8, 0x00,
                0x00, 0x0F, 0xF8, 0x00, 0x00, 0x3F, 0xF8, 0x00, 0x00, 0x3F, 0xF0, 0x00, 0x00, 0x1F, 0xF0, 0x00,
                0x00, 0x0F, 0xE0, 0x00, 0x00, 0x07, 0xE0, 0x00, 0x00, 0x07, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

static unsigned char ANDPlaneCatch[128] = {
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xE4, 0x9F, 0xFF, 0xFF, 0xC0, 0x07, 0xFF, 0xFF, 0xC0, 0x03, 0xFF, 0xFF, 0xE0, 0x03, 0xFF,
                0xFF, 0xC0, 0x03, 0xFF, 0xFF, 0x80, 0x03, 0xFF, 0xFF, 0x80, 0x07, 0xFF, 0xFF, 0xC0, 0x07, 0xFF,
                0xFF, 0xE0, 0x0F, 0xFF, 0xFF, 0xF0, 0x0F, 0xFF, 0xFF, 0xF0, 0x0F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF } ;


static unsigned char XORPlaneHand[128] = {
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x00, 0x00, 0x0C, 0xD8, 0x00, 0x00,
                0x0C, 0xD8, 0x00, 0x00, 0x06, 0xD9, 0x00, 0x00, 0x06, 0xDB, 0x00, 0x00, 0x03, 0xFB, 0x00, 0x00,
                0x33, 0xFF, 0x00, 0x00, 0x3B, 0xFE, 0x00, 0x00, 0x1F, 0xFE, 0x00, 0x00, 0x0F, 0xFE, 0x00, 0x00,
                0x0F, 0xFC, 0x00, 0x00, 0x07, 0xFC, 0x00, 0x00, 0x03, 0xF8, 0x00, 0x00, 0x01, 0xF8, 0x00, 0x00,
                0x01, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

static unsigned char ANDPlaneHand[128] = {
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0xFF, 0xFF, 0xF2, 0x07, 0xFF, 0xFF, 0xE0, 0x03, 0xFF, 0xFF,
                0xE0, 0x02, 0xFF, 0xFF, 0xF0, 0x00, 0x7F, 0xFF, 0xF0, 0x00, 0x7F, 0xFF, 0xC8, 0x00, 0x7F, 0xFF,
                0x80, 0x00, 0x7F, 0xFF, 0x80, 0x00, 0xFF, 0xFF, 0xC0, 0x00, 0xFF, 0xFF, 0xE0, 0x00, 0xFF, 0xFF,
                0xE0, 0x01, 0xFF, 0xFF, 0xF0, 0x01, 0xFF, 0xFF, 0xF8, 0x03, 0xFF, 0xFF, 0xFC, 0x03, 0xFF, 0xFF,
                0xFC, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF  } ;

// hDC, nTop, nLeft, nBottom, nRight, nColor, nClrPane
HB_FUNC( ARROWDOWN )
{
     HDC hDC             = (HDC) hb_parnl( 1 );
     int nTop            = hb_parni( 2 );
     int nLeft           = hb_parni( 3 );
     int nBottom         = hb_parni( 4 );
     int nRight          = hb_parni( 5 );
     COLORREF nColor     = hb_parnl( 6 );
     COLORREF nBackColor = hb_parnl( 7 );
     int nMedioH = nTop + (( nBottom - nTop ) / 2);
     int nMedioW = nLeft + (( nRight - nLeft ) / 2) ;
     HPEN hPen, hOldPen;
     HBRUSH hBrush, hOldBrush;

     hPen      = CreatePen( PS_SOLID, 1, nColor );
     hOldPen   = (HPEN) SelectObject( hDC, hPen );

     hBrush    = CreateSolidBrush( nBackColor );
     hOldBrush = (HBRUSH) SelectObject( hDC, hBrush );

     //Ellipse( hDC, nLeft, nTop, nRight, nBottom );

     ( HBRUSH ) SelectObject( hDC, hOldBrush );

     MoveToEx( hDC, nMedioW - 3, nMedioH - 3, NULL );
     LineTo  ( hDC, nMedioW    , nMedioH      );
     LineTo  ( hDC, nMedioW + 4, nMedioH - 4  );

     MoveToEx( hDC, nMedioW - 2, nMedioH - 3 , NULL);
     LineTo  ( hDC, nMedioW    , nMedioH - 1 );
     LineTo  ( hDC, nMedioW + 3, nMedioH - 4   );

     nMedioH += 4;

     MoveToEx( hDC, nMedioW - 3, nMedioH - 3, NULL );
     LineTo  ( hDC, nMedioW    , nMedioH      );
     LineTo  ( hDC, nMedioW + 4, nMedioH - 4  );

     MoveToEx( hDC, nMedioW - 2, nMedioH - 3 , NULL);
     LineTo  ( hDC, nMedioW    , nMedioH - 1 );
     LineTo  ( hDC, nMedioW + 3, nMedioH - 4   );

     ( HPEN )   SelectObject( hDC, hOldPen );

     DeleteObject( hBrush );

     DeleteObject( hPen );


     hb_ret();
}

// hDC, nTop, nLeft, nBottom, nRight, nColor, nClrPane
HB_FUNC( ARROWUP )
{
     HDC hDC             = (HDC) hb_parnl( 1 );
     int nTop            = hb_parni( 2 );
     int nLeft           = hb_parni( 3 );
     int nBottom         = hb_parni( 4 );
     int nRight          = hb_parni( 5 );
     COLORREF nColor     = hb_parnl( 6 );
     COLORREF nBackColor = hb_parnl( 7 );
     int nMedioH = nTop + (( nBottom - nTop ) / 2)-1 ;
     int nMedioW = nLeft + (( nRight - nLeft ) / 2) ;
     HPEN hPen, hOldPen;
     HBRUSH hBrush, hOldBrush;

     hPen      = CreatePen( PS_SOLID, 1, nColor );
     hOldPen   = (HPEN) SelectObject( hDC, hPen );

     hBrush    = CreateSolidBrush( nBackColor );
     hOldBrush = (HBRUSH) SelectObject( hDC, hBrush );

     //Ellipse( hDC, nLeft, nTop, nRight, nBottom );

     ( HBRUSH ) SelectObject( hDC, hOldBrush );

     MoveToEx( hDC, nMedioW - 3, nMedioH, NULL );
     LineTo( hDC, nMedioW    , nMedioH - 3 );
     LineTo( hDC, nMedioW + 4, nMedioH + 1  );

     MoveToEx( hDC, nMedioW - 2, nMedioH, NULL);
     LineTo( hDC, nMedioW    , nMedioH - 2 );
     LineTo( hDC, nMedioW + 3, nMedioH +1   );

     nMedioH += 4;

     MoveToEx( hDC, nMedioW - 3, nMedioH, NULL);
     LineTo( hDC, nMedioW    , nMedioH - 3 );
     LineTo( hDC, nMedioW + 4, nMedioH + 1 );

     MoveToEx( hDC, nMedioW - 2, nMedioH, NULL);
     LineTo( hDC, nMedioW    , nMedioH - 2 );
     LineTo( hDC, nMedioW + 3, nMedioH + 1 );

     ( HPEN )   SelectObject( hDC, hOldPen );

     DeleteObject( hBrush );

     DeleteObject( hPen );
     hb_ret();
}

// hBmp
HB_FUNC(  BMPHEIGHT )
{
    BITMAP bm;
    if( hb_parnl( 1 ) )
    {
       GetObject( ( HGDIOBJ ) hb_parnl( 1 ), sizeof( BITMAP ), ( LPSTR ) &bm );
       hb_retnl( bm.bmHeight );
    }
    else
    {
       hb_retnl(0);
    }
}


#define ROSA RGB(255,0,255)

HB_FUNC( BMPTOGRAY )
{
   //DrawGrayScale( hDC, nImage, nTop, nLeft, nWidth, nHeight )

   HBITMAP hBmpMem, hOldBmp, hOldBmp2;
   COLORREF nColor;
   int nRojo, nVerde, nAzul;
   int nX;
   int nY;

   HDC hDC     = CreateDC("DISPLAY", 0, 0, 0);
   HDC hDCMem  = CreateCompatibleDC( hDC );
   HDC hDCMem2 = CreateCompatibleDC( hDC );
   HBITMAP hBitmap = (HBITMAP) hb_parnl( 1 );

   BITMAP bm;
   GetObject( ( HGDIOBJ ) hBitmap, sizeof( BITMAP ), ( LPSTR ) &bm );

   hBmpMem = CreateCompatibleBitmap( hDC, bm.bmWidth, bm.bmHeight );
   hOldBmp = (HBITMAP) SelectObject( hDCMem, hBmpMem );

   hOldBmp2 = (HBITMAP) SelectObject( hDCMem2, hBitmap );

   BitBlt( hDCMem, 0,0,bm.bmWidth, bm.bmHeight, hDCMem2, 0, 0, SRCCOPY );

   for (nX = 0; nX < bm.bmWidth ; nX++ )
   {
       for (nY = 0; nY < bm.bmHeight ; nY++ )
      {
           nColor = GetPixel( hDCMem, nX, nY );
           if( nColor != ROSA )
           {
              nRojo  = GetRValue( nColor );
              nVerde = GetGValue( nColor );
              nAzul  = GetBValue( nColor );
              nColor = max( nRojo, max( nVerde, nAzul ) );
              //nColor = min( nColor + iMinus, 255 );

              //nColor = (COLORREF) (nRojo*0.3 + nVerde*0.59 + nAzul*0.11);
              SetPixel( hDCMem, nX, nY, RGB( nColor, nColor, nColor) );
           }
       }
   }

   SelectObject( hDCMem, hOldBmp );
   SelectObject( hDCMem2, hOldBmp2 );
   DeleteDC( hDCMem );
   DeleteDC( hDCMem2 );
   DeleteDC( hDC );

   hb_retnl( (LONG) hBmpMem );
}

// hBmp
HB_FUNC( BMPWIDTH )
{
    BITMAP bm;

    if( hb_parnl( 1 ) )
    {
       GetObject( ( HGDIOBJ ) hb_parnl( 1 ), sizeof( BITMAP ), ( LPSTR ) &bm );
       hb_retnl( bm.bmWidth );
    }
    else
    {
       hb_retnl(0);
    }
}
HB_FUNC( BOX )
{
      HDC hDC = (HDC) hb_parnl( 1 );
      HPEN hPen;
      HPEN hOldPen;
      RECT rc;

      if( hb_pcount() > 3 )
      {
         hPen = CreatePen( hb_parni(4),1, (COLORREF)hb_parnl( 3 ));
      }
      else
      {
         hPen = CreatePen( PS_SOLID,1, (COLORREF)hb_parnl( 3 ));
      }
      rc.top    = hb_parvni( 2, 1);
      rc.left   = hb_parvni( 2, 2);
      rc.bottom = hb_parvni( 2, 3);
      rc.right  = hb_parvni( 2, 4);
      hOldPen = (HPEN) SelectObject( hDC, hPen );
      MoveToEx( hDC, rc.left, rc.top, NULL );
      LineTo( hDC, rc.right, rc.top );
      LineTo( hDC, rc.right, rc.bottom );
      LineTo( hDC, rc.left, rc.bottom );
      LineTo( hDC, rc.left, rc.top );
      SelectObject( hDC, hOldPen );
      DeleteObject( hPen );
}

HB_FUNC( BOXEX )
{

   HDC hDC     = (HDC) hb_parnl( 1 );
   RECT rc;
   HBRUSH hOldBrush;
   HPEN hPen = NULL;
   HPEN hOldPen;

   rc.top    = hb_parvni( 2, 1);
   rc.left   = hb_parvni( 2, 2);
   rc.bottom = hb_parvni( 2, 3);
   rc.right  = hb_parvni( 2, 4);

   if( hb_pcount() > 2 )
   {
        hPen = CreatePen( PS_SOLID, 1, hb_parnl( 3 ) );
        hOldPen = (HPEN)SelectObject( hDC, (HPEN) hPen );
   }

   hOldBrush =  (HBRUSH)SelectObject( hDC, (HBRUSH) GetStockObject( NULL_BRUSH ) );
   Rectangle( hDC, rc.left, rc.top, rc.right, rc.bottom );
   SelectObject( hDC, (HBRUSH) hOldBrush );

   if( hPen != NULL )
   {
       SelectObject( hDC, (HPEN) hOldPen );
       DeleteObject( hPen );
   }
    hb_ret();
}

// hDC, aRect, nClr1, nClr2
HB_FUNC( C5DEGRADA )
{
        RECT rct;

        rct.top    = hb_parvni( 2, 1 );
        rct.left   = hb_parvni( 2, 2 );
        rct.bottom = hb_parvni( 2, 3 );
        rct.right  = hb_parvni( 2, 4 );

        C5GradientFill( ( HDC ) hb_parnl( 1 ) , rct, hb_parnl( 3 ), hb_parnl( 4 ), hb_parl(5) );
}

HB_FUNC( COMPATBMP )
{
         hb_retnl( (LONG) CreateCompatibleBitmap( ( HDC ) hb_parnl( 1 ), hb_parni( 2 ), hb_parni( 3 ) ));

}

HB_FUNC( COMPATDC )
{
         hb_retnl( (LONG) CreateCompatibleDC( ( HDC ) hb_parnl( 1 )));

}

HB_FUNC( CREATEHGRIP )
{
    HDC hDC = CreateDC("DISPLAY", NULL, NULL, NULL);
    HDC hDCMem = CreateCompatibleDC(hDC);
    HBITMAP hBmpMem = CreateCompatibleBitmap(hDC,40,5);
    HBITMAP hOldBmp = (HBITMAP) SelectObject( hDCMem, hBmpMem );
    RECT rc;
    int i;
    rc.top    = 0;
    rc.left   = 0;
    rc.bottom = 5;
    rc.right  = 40;
    FillSolidRect( hDCMem, &rc, RGB(255,0,0));


    rc.top    = 2;
    rc.left   = 2;
    rc.bottom = 4;
    rc.right  = 4;
    for( i = 0; i < 9; i++)
    {
       FillSolidRect( hDCMem, &rc, RGB(255,255,255));
       OffsetRect( &rc, 4, 0 );
    }

    rc.top    = 1;
    rc.left   = 1;
    rc.bottom = 3;
    rc.right  = 3;
    for( i = 0; i < 9; i++)
    {
       FillSolidRect( hDCMem, &rc, (COLORREF) hb_parnl(1) );
       OffsetRect( &rc, 4, 0 );
    }
    (HBITMAP) SelectObject( hDCMem, hOldBmp );
    DeleteDC( hDCMem );
    DeleteDC( hDC );
    hb_retnl( (long) hBmpMem );

}

HB_FUNC( CREATEVGRIP )
{
    HDC hDC = CreateDC("DISPLAY", NULL, NULL, NULL);
    HDC hDCMem = CreateCompatibleDC(hDC);
    int iHeight = hb_parni( 2 )-8;
    int iTimes =  iHeight / 6;
    HBITMAP hBmpMem = CreateCompatibleBitmap(hDC,5,iHeight);
    HBITMAP hOldBmp = (HBITMAP) SelectObject( hDCMem, hBmpMem );
    RECT rc;
    int i;
    char sz[MAX_PATH];
    sprintf( sz, "%d %d",iHeight,iTimes);
    MessageBox( 0, sz, "000",0);


    rc.top    = 0;
    rc.left   = 0;
    rc.bottom = iHeight;
    rc.right  = 5;
    FillSolidRect( hDCMem, &rc, RGB(255,0,0));



    rc.top    = 2;
    rc.left   = 2;
    rc.bottom = 4;
    rc.right  = 4;
    for( i = 0; i < iTimes; i++)
    {
       FillSolidRect( hDCMem, &rc, RGB(255,255,255));
       OffsetRect( &rc, 0, 4 );
    }

    rc.top    = 1;
    rc.left   = 1;
    rc.bottom = 3;
    rc.right  = 3;
    for( i = 0; i < iTimes; i++)
    {
       FillSolidRect( hDCMem, &rc, (COLORREF) hb_parnl(1) );
       OffsetRect( &rc, 0, 4 );
    }
    (HBITMAP) SelectObject( hDCMem, hOldBmp );
    DeleteDC( hDCMem );
    DeleteDC( hDC );
    hb_retnl( (long) hBmpMem );

}



HB_FUNC( CREATEFONTUNDERLINE )
{

   LOGFONT lf;
   TEXTMETRIC tm;
   HFONT hFont    = ( HFONT ) hb_parnl( 1 );
   HWND hWnd      = GetActiveWindow();
   HDC hDC        = GetDC( hWnd );
   HFONT hOldFont = ( HFONT ) SelectObject( hDC, hFont );
   char cName[ 80 ];

   ZeroMemory( &lf,sizeof( LOGFONT ) );

   GetTextMetrics( hDC, &tm );
   GetTextFace( hDC, sizeof( cName ), cName );
   SelectObject( hDC, hOldFont );
   ReleaseDC( hWnd, hDC );


   lf.lfHeight         = tm.tmHeight;
   lf.lfWidth          = tm.tmAveCharWidth;
   lf.lfWeight         = tm.tmWeight;
   lf.lfItalic         = ( BYTE ) tm.tmItalic;
   lf.lfUnderline      = ( BYTE ) TRUE ;
   lf.lfStrikeOut      = ( BYTE ) tm.tmStruckOut;
   lf.lfCharSet        = ( BYTE ) tm.tmCharSet;
   strcpy( ( char * ) &( lf.lfFaceName ), cName );

   hb_retnl( (LONG) CreateFontIndirect( &lf ) );
}


HB_FUNC( CREAFUNDER )
{
   LOGFONT lf;
   TEXTMETRIC tm;
   HFONT hFont    = ( HFONT ) hb_parnl( 1 );
   HWND hWnd      = GetActiveWindow();
   HDC hDC        = GetDC( hWnd );
   HFONT hOldFont = ( HFONT ) SelectObject( hDC, hFont );
   char cName[ 80 ];

   ZeroMemory( &lf,sizeof( LOGFONT ) );

   GetTextMetrics( hDC, &tm );
   GetTextFace( hDC, sizeof( cName ), cName );
   SelectObject( hDC, hOldFont );
   ReleaseDC( hWnd, hDC );


   lf.lfHeight         = tm.tmHeight;
   lf.lfWidth          = tm.tmAveCharWidth;
   lf.lfWeight         = tm.tmWeight;
   lf.lfItalic         = ( BYTE ) tm.tmItalic;
   lf.lfUnderline      = ( BYTE ) TRUE ;
   lf.lfStrikeOut      = ( BYTE ) tm.tmStruckOut;
   lf.lfCharSet        = ( BYTE ) tm.tmCharSet;
   strcpy( ( char * ) &( lf.lfFaceName ), cName );

   hb_retnl( (LONG) CreateFontIndirect( &lf ) );}


HB_FUNC( CREATEFONTBOLD )
{
   TEXTMETRIC tm;
   HFONT hFont    = ( HFONT ) hb_parnl( 1 );
   HWND hWnd      = GetActiveWindow();
   HDC hDC        = GetDC( hWnd );
   HFONT hOldFont = ( HFONT ) SelectObject( hDC, hFont );
   LOGFONT lf;
   char cName[ 80 ];

   ZeroMemory( &lf,sizeof( LOGFONT ) );



   GetTextMetrics( hDC, &tm );
   GetTextFace( hDC, sizeof( cName ), cName );
   SelectObject( hDC, hOldFont );
   ReleaseDC( hWnd, hDC );

   lf.lfHeight         = tm.tmHeight;
   lf.lfWidth          = tm.tmAveCharWidth;
   lf.lfWeight         = 700;
   lf.lfItalic         = ( BYTE ) tm.tmItalic;
   lf.lfUnderline      = ( BYTE ) tm.tmUnderlined;
   lf.lfStrikeOut      = ( BYTE ) tm.tmStruckOut;
   lf.lfCharSet        = ( BYTE ) tm.tmCharSet;
   strcpy( ( char * ) &( lf.lfFaceName ), cName );

   hb_retnl( (LONG) CreateFontIndirect( &lf ) );
}

HB_FUNC( CREAFBOLD )
{
   TEXTMETRIC tm;
   HFONT hFont    = ( HFONT ) hb_parnl( 1 );
   HWND hWnd      = GetActiveWindow();
   HDC hDC        = GetDC( hWnd );
   HFONT hOldFont = ( HFONT ) SelectObject( hDC, hFont );
   LOGFONT lf;
   char cName[ 80 ];

   ZeroMemory( &lf,sizeof( LOGFONT ) );

   GetTextMetrics( hDC, &tm );
   GetTextFace( hDC, sizeof( cName ), cName );
   SelectObject( hDC, hOldFont );
   ReleaseDC( hWnd, hDC );

   lf.lfHeight         = tm.tmHeight;
   lf.lfWidth          = tm.tmAveCharWidth;
   lf.lfWeight         = 700;
   lf.lfItalic         = ( BYTE ) tm.tmItalic;
   lf.lfUnderline      = ( BYTE ) tm.tmUnderlined;
   lf.lfStrikeOut      = ( BYTE ) tm.tmStruckOut;
   lf.lfCharSet        = ( BYTE ) tm.tmCharSet;
   strcpy( ( char * ) &( lf.lfFaceName ), cName );

   hb_retnl( (LONG) CreateFontIndirect( &lf ) );
}


HB_FUNC( CREATECAPTIONFONT )
{
   NONCLIENTMETRICS info;
   info.cbSize = sizeof(info);
   SystemParametersInfo( SPI_GETNONCLIENTMETRICS, sizeof(info), &info, 0 );
   hb_retnl( (LONG ) CreateFontIndirect( &info.lfCaptionFont ) );
}

HB_FUNC( CREAFCAPT )
{
   NONCLIENTMETRICS info;
   info.cbSize = sizeof(info);
   SystemParametersInfo( SPI_GETNONCLIENTMETRICS, sizeof(info), &info, 0 );
   hb_retnl( (LONG ) CreateFontIndirect( &info.lfCaptionFont ) );
}

HB_FUNC( FWDELETEFILE )
{
   hb_retl( DeleteFile( hb_parc( 1 )));
}


HB_FUNC( DRAWEDGE )
{
  RECT rc;
  rc.top    = hb_parvni( 2, 1);
  rc.left   = hb_parvni( 2, 2);
  rc.bottom = hb_parvni( 2, 3);
  rc.right  = hb_parvni( 2, 4);

  hb_retl( DrawEdge( ( HDC ) hb_parnl( 1 ),       // handle to device context
                      &rc,                        // rectangle coordinates
                      hb_parnl( 3 ),              // type of edge
                      hb_parnl( 4 )               // type of border
                    ));
}


HB_FUNC( DRAWFRAMECONTROL )
{
      RECT rc;
      rc.top    = hb_parvni( 2, 1);
      rc.left   = hb_parvni( 2, 2);
      rc.bottom = hb_parvni( 2, 3);
      rc.right  = hb_parvni( 2, 4);

       hb_retl( DrawFrameControl( (HDC) hb_parnl( 1 ), &rc, hb_parni( 3 ), hb_parni( 4 ) ) );
}

HB_FUNC( EQUALRECT )
{

   RECT rc1;
   RECT rc2;

  rc1.top    = hb_parvni(1,1);
  rc1.left   = hb_parvni(1,2);
  rc1.bottom = hb_parvni(1,3);
  rc1.right  = hb_parvni(1,4);

  rc2.top    = hb_parvni(2,1);
  rc2.left   = hb_parvni(2,2);
  rc2.bottom = hb_parvni(2,3);
  rc2.right  = hb_parvni(2,4);

  hb_retni( EqualRect( &rc1, &rc2 ) );

}

HB_FUNC( EXCLUDECLIPRECT )
{
  hb_retni( ExcludeClipRect( (HDC) hb_parnl( 1 ), hb_parni( 3 ),hb_parni( 2 ),hb_parni( 5 ),hb_parni( 4 ) ));
}

void FillSolidRect(HDC hDC, LPCRECT lpRect, COLORREF clr)
{
   SetBkColor(hDC, clr);
   ExtTextOut(hDC, 0, 0, ETO_OPAQUE, lpRect, NULL, 0, NULL);
}

HB_FUNC( FILLSOLIRC )
{
    RECT rct;
    COLORREF nColor = hb_parnl( 3 );
    rct.top    = hb_parvni( 2, 1 );
    rct.left   = hb_parvni( 2, 2 );
    rct.bottom = hb_parvni( 2, 3 );
    rct.right  = hb_parvni( 2, 4 );

    FillSolidRect( (HDC) hb_parnl( 1 ), &rct, nColor );
}


HB_FUNC( FILLSOLIDRECT )
{
    RECT rct;
    COLORREF nColor;
    HPEN hPen, hOldPen;
    HDC hDC = ( HDC ) hb_parnl( 1 );
    rct.top    = hb_parvni( 2, 1 );
    rct.left   = hb_parvni( 2, 2 );
    rct.bottom = hb_parvni( 2, 3 );
    rct.right  = hb_parvni( 2, 4 );

    nColor = SetBkColor( hDC, hb_parnl( 3 ) );
    ExtTextOut( hDC, 0, 0, ETO_OPAQUE, &rct, NULL, 0, NULL);
    SetBkColor( hDC, nColor );

    if( hb_pcount()  > 3 )
    {
       hPen = CreatePen( PS_SOLID, 1,(COLORREF)hb_parnl( 4 ));
       hOldPen = (HPEN) SelectObject( hDC, hPen );
       MoveToEx( hDC, rct.left, rct.top, NULL );
       LineTo( hDC, rct.right-3, rct.top );
       LineTo( hDC, rct.right-3, rct.bottom );
       LineTo( hDC, rct.left, rct.bottom );
       LineTo( hDC, rct.left, rct.top );
       SelectObject( hDC, hOldPen );
       DeleteObject( hPen );
    }
}

HB_FUNC( GETCURRENTOBJECT )
{
   // HGDIOBJ GetCurrentObject(  HDC hdc,           // handle to DC
   //                           UINT uObjectType   // object type );
   hb_retnl( (long) GetCurrentObject( (HDC) hb_parnl( 1 ), hb_parni( 2 )));
}


HB_FUNC( GETFONTMENU )
{
   NONCLIENTMETRICS info;
   HFONT hFont;
   info.cbSize = sizeof(info);
   SystemParametersInfo( SPI_GETNONCLIENTMETRICS, sizeof(info), &info, 0 );
   hFont = CreateFontIndirect( &info.lfMenuFont );
   hb_retnl( (LONG) hFont );
}

HB_FUNC( GETFONTHEIGHT )
{
   LOGFONT lf;
   GetObject( (HFONT) hb_parnl( 1 ) , sizeof( LOGFONT ), &lf );
   hb_retni( lf.lfHeight );
}


//CtlType        aStruct[1 ]
//CtlID          aStruct[2 ]
//itemID         aStruct[3 ]
//itemAction     aStruct[4 ]
//itemState      aStruct[5 ]
//hwndItem       aStruct[6 ]
//hDC            aStruct[7 ]
//nTop           aStruct[8 ]
//nLeft          aStruct[9 ]
//nBottom        aStruct[10]
//nRight         aStruct[11]
//itemData       aStruct[12]

HFONT GetFontMenu()
{
   NONCLIENTMETRICS info;
   HFONT hFont;
   info.cbSize = sizeof(info);
   SystemParametersInfo( SPI_GETNONCLIENTMETRICS, sizeof(info), &info, 0 );
   hFont = CreateFontIndirect( &info.lfMenuFont );
   return hFont;
}



HB_FUNC( GETTEXTLEN )
{
   SIZE sz;
   GetTextExtentPoint32( (HDC) hb_parnl( 1 ), hb_parc( 2 ), hb_parclen( 2 ), &sz );
   hb_retni( sz.cx );
}

HB_FUNC( ICONTOGRAY )
{

   HBITMAP hBmpMem, hOldBmp;
   COLORREF nColor;
   int nRojo, nVerde, nAzul;
   int nX;
   int nY;
   RECT rc;
   HDC hDC     = CreateDC("DISPLAY", 0, 0, 0);
   HDC hDCMem  = CreateCompatibleDC( hDC );
   HICON hIcon = (HICON) hb_parnl( 1 );

   rc.top = 0;
   rc.left = 0;
   rc.bottom = 33;
   rc.right = 33;


   hBmpMem = CreateCompatibleBitmap( hDC, 33, 33 );
   hOldBmp = (HBITMAP) SelectObject( hDCMem, hBmpMem );
   FillSolidRect( hDCMem, &rc, RGB(255,0,255) );
   DrawIcon( hDCMem, 0, 0, hIcon );
   for (nX = 0; nX < 33 ; nX++ )
   {
       for (nY = 0; nY < 33 ; nY++ )
       {
           nColor = GetPixel( hDCMem, nX, nY );
           if( nColor != ROSA )
           {
              nRojo  = GetRValue( nColor );
              nVerde = GetGValue( nColor );
              nAzul  = GetBValue( nColor );
              //nColor = (COLORREF) (nRojo*0.3 + nVerde*0.59 + nAzul*0.11);
              nColor = max( nRojo, max( nVerde, nAzul ) );

              SetPixel( hDCMem, nX, nY, RGB( nColor, nColor, nColor) );
           }
       }
   }

   SelectObject( hDCMem, hOldBmp );
   DeleteDC( hDCMem );

   hb_retnl( (LONG) hBmpMem );
}

HB_FUNC( ICONTOBMP )
{

   HBITMAP hBmpMem, hOldBmp;
   COLORREF nColor;
   int nRojo, nVerde, nAzul;
   int nX;
   int nY;
   RECT rc;
   HDC hDC     = CreateDC("DISPLAY", 0, 0, 0);
   HDC hDCMem  = CreateCompatibleDC( hDC );
   HICON hIcon = (HICON) hb_parnl( 1 );

   rc.top = 0;
   rc.left = 0;
   rc.bottom = 33;
   rc.right = 33;


   hBmpMem = CreateCompatibleBitmap( hDC, 33, 33 );
   hOldBmp = (HBITMAP) SelectObject( hDCMem, hBmpMem );
   FillSolidRect( hDCMem, &rc, RGB(255,0,255) );
   DrawIcon( hDCMem, 0, 0, hIcon );
   SelectObject( hDCMem, hOldBmp );
   DeleteDC( hDCMem );

   hb_retnl( (LONG) hBmpMem );
}

HB_FUNC( INTRECT )
{

   RECT rc;
   RECT rc1;
   RECT rc2;

  rc.top    = 0;
  rc.left   = 0;
  rc.bottom = 0;
  rc.right  = 0;

  rc1.top    = hb_parvni(1,1);
  rc1.left   = hb_parvni(1,2);
  rc1.bottom = hb_parvni(1,3);
  rc1.right  = hb_parvni(1,4);

  rc2.top    = hb_parvni(2,1);
  rc2.left   = hb_parvni(2,2);
  rc2.bottom = hb_parvni(2,3);
  rc2.right  = hb_parvni(2,4);

  IntersectRect( &rc, &rc1, &rc2 );

  hb_reta(4);

  hb_storvni(rc.top, -1, 1 );
  hb_storvni(rc.left, -1, 2 );
  hb_storvni(rc.bottom, -1, 3 );
  hb_storvni(rc.right, -1, 4 );

}

HB_FUNC( PTINRECT )
{
   POINT pt;
   RECT  rct;

   pt.y = hb_parnl( 1 );
   pt.x = hb_parnl( 2 );

   rct.top    = hb_parvni( 3, 1 );
   rct.left   = hb_parvni( 3, 2 );
   rct.bottom = hb_parvni( 3, 3 );
   rct.right  = hb_parvni( 3, 4 );

   hb_retl( PtInRect( &rct, pt ) );
}

HB_FUNC( ROUNDRECTEX )
{
  HDC hDC = (HDC) hb_parnl( 1 );
  HPEN hPen = NULL;
  HPEN hOldPen;
  if( hb_pcount() > 7 )
  {
     hPen = CreatePen( PS_SOLID, 1, (COLORREF) hb_parnl( 8 ));
     hOldPen = (HPEN)SelectObject( hDC, hPen );
  }

  hb_retl( RoundRect( hDC, hb_parni( 3 ), hb_parni( 2 ), hb_parni( 5 ), hb_parni( 4 ), hb_parni( 6 ), hb_parni( 7 )  ));

  if( hPen != NULL )
  {
     SelectObject( hDC, hOldPen );
     DeleteObject( hPen );
  }
}


HB_FUNC ( SETBRUSHORG )
{
    HDC hDC  = (HDC) hb_parnl( 1 );
    UnrealizeObject( ( HGDIOBJ ) hb_parnl( 2 ) );
    SetBrushOrgEx( hDC, hb_parni( 3 ), hb_parni( 4 ), NULL );
}

HB_FUNC( SETMENUITEMBITMAPS )
{
  hb_retl( SetMenuItemBitmaps( (HMENU) hb_parnl(1), hb_parni(2), hb_parni(3), (HBITMAP) hb_parnl(4), (HBITMAP)  hb_parnl(5)) );
}

HB_FUNC( SETSTRETCHBLTMODE )
{
    hb_retni( SetStretchBltMode( ( HDC ) hb_parnl( 1 ),  hb_parni( 2 ) ));
}

HB_FUNC( VALIDATERECT )
{
  RECT rc;

  rc.top    = hb_parvni(2,1);
  rc.left   = hb_parvni(2,2);
  rc.bottom = hb_parvni(2,3);
  rc.right  = hb_parvni(2,4);

  #ifdef _WIN64
     hb_retl( ValidateRect(  ( HWND ) hb_parnll( 1 ), &rc ));
  #else   
     hb_retl( ValidateRect(  ( HWND ) hb_parnl( 1 ), &rc ));
  #endif
}

void DrawMaskedColor( HDC hdc, HBITMAP hbm, WORD y, WORD x, COLORREF clrMask )
{
   HDC         hdcMask, hdcBmp;
   HBITMAP     hbOld, hbOld1, hbMask, hbmp;
   BITMAP      bm;
   COLORREF    bBack, bFore;

   hdcMask  = CreateCompatibleDC( hdc );
   hdcBmp   = CreateCompatibleDC( hdc );
   GetObject( hbm, sizeof( BITMAP ), ( LPSTR ) &bm );

   hbmp     = CreateCompatibleBitmap( hdc, bm.bmWidth, bm.bmHeight );
   hbMask   = CreateBitmap( bm.bmWidth, bm.bmHeight, 1, 1, NULL );

   hbOld  = ( HBITMAP ) SelectObject( hdcBmp, hbmp );
   hbOld1 = ( HBITMAP ) SelectObject( hdcMask, hbm );
   BitBlt( hdcBmp, 0, 0, bm.bmWidth, bm.bmHeight, hdcMask, 0, 0, SRCCOPY );
   SelectObject( hdcMask, hbMask );

   SetBkColor( hdcBmp, clrMask );
   BitBlt( hdcMask, 0, 0, bm.bmWidth, bm.bmHeight, hdcBmp, 0, 0, SRCCOPY );
   BitBlt( hdcBmp, 0, 0, bm.bmWidth, bm.bmHeight, hdcMask, 0, 0, 0x220326 );
   bBack = SetBkColor( hdc, RGB( 255, 255, 255 ) );
   bFore = SetTextColor( hdc, 0 );
   BitBlt( hdc, x, y, bm.bmWidth, bm.bmHeight, hdcMask, 0, 0, SRCAND );
   BitBlt( hdc, x, y, bm.bmWidth, bm.bmHeight, hdcBmp, 0, 0, SRCPAINT );
   SetBkColor( hdc, bBack );
   SetTextColor( hdc, bFore );

   SelectObject( hdcBmp, hbOld );
   SelectObject( hdcMask, hbOld1 );
   DeleteObject( hbmp );
   DeleteObject( hbMask );
   DeleteDC( hdcMask );
   DeleteDC( hdcBmp );
}


HB_FUNC( DRAWMASKEDCOLOR ) // ( hDC, hBitmap, nRow, nCol, nColor ) --> nil
{
   DrawMaskedColor( ( HDC ) hb_parnl( 1 ), ( HBITMAP ) hb_parnl( 2 ),
                    ( WORD ) hb_parni( 3 ), ( WORD ) hb_parni( 4 ), ( COLORREF ) hb_parnl( 5 ) );
}

   //--------------------------------------------------------------------------

   static LPSTR WideToAnsi( LPWSTR cWide )
   {
      WORD wLen;
      LPSTR cString = NULL;

      wLen = WideCharToMultiByte( CP_ACP, 0, cWide, -1, cString, 0, NULL, NULL );

      cString = (LPSTR) hb_xgrab( wLen );
      WideCharToMultiByte( CP_ACP, 0, cWide, -1, cString, wLen, NULL, NULL );

      return ( cString );
   }

   //--------------------------------------------------------------------------
   //
   // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   // ~~~~~~~ MANEJO DE TEMAS ~~~~~~
   // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   //
   // C5_ISTHEMEACTIVE
   // C5_OPENTHEMEDATA
   // C5_CLOSETHEMEDATA
   // C5_GETCURRENTTHEMENAME
   // C5_DRAWTHEMEBACKGROUND
   
   typedef HANDLE HTHEME;
   
   typedef BOOL ( * PISAPPTHEMED ) ( void ); 
   
   BOOL C5_IsAppThemed( void )
   {
      typedef BOOL (CALLBACK* LPFNDLLFUNC2)();
      HINSTANCE hLib;
      LPFNDLLFUNC2 IsAppThemed;
      BOOL bIsAppThemed = FALSE;

      hLib = LoadLibrary( "uxtheme.dll" );
      if( hLib )
      {
          IsAppThemed = ((LPFNDLLFUNC2) GetProcAddress( hLib, "IsAppThemed" ));
          bIsAppThemed = ( ( PISAPPTHEMED ) IsAppThemed )();
          FreeLibrary( hLib );
      }
      return bIsAppThemed;
   }


   HB_FUNC( C5_ISAPPTHEMED )
   {
      hb_retl(C5_IsAppThemed());
   }

   typedef BOOL ( * PISTHEMEACTIVE )( void );

   BOOL C5_IsThemeActive( void )
   {
      typedef BOOL (CALLBACK* LPFNDLLFUNC2)();
      HINSTANCE hLib;
      LPFNDLLFUNC2 IsThemeActive;
      BOOL bIsActive = FALSE;

      hLib = LoadLibrary( "uxtheme.dll" );
      if( hLib )
      {
          IsThemeActive = ((LPFNDLLFUNC2) GetProcAddress( hLib, "IsThemeActive" ));
          bIsActive = ( ( PISTHEMEACTIVE ) IsThemeActive )();
          FreeLibrary( hLib );
      }
      return bIsActive;
   }


   HB_FUNC( C5_ISTHEMEACTIVE ) // --> BOOL
   {
      hb_retl(C5_IsThemeActive());
   }

   HTHEME C5_OpenThemeData( HWND hWnd, LPTSTR szTheme )
   {
     typedef HTHEME ( FAR PASCAL  *LPOPENTHEMEDATA )( HWND hwnd, LPCSTR pszClassList );
     LPWSTR cTheme = AnsiToWide( szTheme );
     HINSTANCE hLib;
     LPOPENTHEMEDATA OpenThemeData;
     HTHEME hTheme = NULL;

     hLib = LoadLibrary( "uxtheme.dll" );
     if ( hLib )
     {
         OpenThemeData = (LPOPENTHEMEDATA) GetProcAddress( hLib, "OpenThemeData" );
         hTheme = OpenThemeData( hWnd, ( LPCSTR ) cTheme ) ;
         FreeLibrary( hLib );
     }
     hb_xfree( cTheme );
     return hTheme;
   }


   HB_FUNC( C5_OPENTHEMEDATA ) //( ::hWnd, cTheme )
   {
   	  #ifdef _WIN64
         hb_retnl( ( LONG ) C5_OpenThemeData( ( HWND ) hb_parnll( 1 ) , ( LPTSTR ) hb_parc( 2 ) ) ); //
      #else   
         hb_retnl( ( LONG ) C5_OpenThemeData( ( HWND ) hb_parnl( 1 ) , ( LPTSTR ) hb_parc( 2 ) ) ); //
      #endif
   }

   HRESULT C5_CloseThemeData( HTHEME hTheme )
   {
      typedef HRESULT  ( FAR PASCAL  *LPCLOSETHEMEDATA )     ( HTHEME hTheme );
      HINSTANCE hLib ;
      LPCLOSETHEMEDATA CloseThemeData;
      HRESULT hr = 0;
      hLib = LoadLibrary( "uxtheme.dll" );
      if ( hLib )
      {
          CloseThemeData = (LPCLOSETHEMEDATA) GetProcAddress( hLib, "CloseThemeData" );
          hr = CloseThemeData( hTheme );
          FreeLibrary( hLib );
      }
      return hr;
   }

   HB_FUNC( C5_CLOSETHEMEDATA )
   {
      hb_retni((int) C5_CloseThemeData( (HTHEME) hb_parnl( 1 )));
   }


   HB_FUNC( C5_GETCURRENTTHEMENAME )
   {
     typedef HRESULT  ( FAR PASCAL  *LPGETCURRENTTHEMENAME) ( LPWSTR pszThemeFileName, int cchMaxNameChars, LPWSTR pszColorBuff, int cchMaxColorChars, LPWSTR pszSizeBuff, int cchMaxSizeChars);
     WCHAR pszThemeFileName[MAX_PATH];
     HINSTANCE hLib;
     LPGETCURRENTTHEMENAME GetCurrentThemeName;
     LPSTR cOut;

     hLib = LoadLibrary( "uxtheme.dll" );
     if ( hLib )
     {
         GetCurrentThemeName = (LPGETCURRENTTHEMENAME) GetProcAddress( hLib, "GetCurrentThemeName" );
         GetCurrentThemeName( (LPWSTR) pszThemeFileName, MAX_PATH, NULL, 0, NULL, 0 );
         cOut = WideToAnsi( pszThemeFileName );
         hb_retc( cOut );
         hb_xfree( cOut );
         FreeLibrary( hLib );
     }
   }


   HRESULT C5_DrawThemeBackground( HTHEME hTheme, HDC hDC, int iPartId, int iStateId, RECT* pRect, RECT* pClipRect) //( ::hTheme, hDC, nPartID, nStateID, pRect, pClipRect )
   {
     typedef HRESULT  ( FAR PASCAL  *LPDRAWTHEMEBACKGROUND )( HTHEME hTheme, HDC hdc, int iPartId, int iStateId, const RECT *pRect, const RECT *pClipRect );
     HINSTANCE hLib;
     LPDRAWTHEMEBACKGROUND DrawThemeBackground;
     HRESULT hr = 0;

     HB_SYMBOL_UNUSED( pClipRect );

     hLib = LoadLibrary( "uxtheme.dll" );
     if ( hLib )
     {
         DrawThemeBackground = (LPDRAWTHEMEBACKGROUND) GetProcAddress( hLib, "DrawThemeBackground" );
         hr = DrawThemeBackground( ( HTHEME ) hTheme,
                                          (HDC) hDC,
                                                iPartId,
                                                iStateId,
                                                pRect,
                                                NULL );
         FreeLibrary( hLib );
     }
     return hr;
   }

   HB_FUNC( C5_DRAWTHEMEBACKGROUND ) //( ::hTheme, hDC, nPartID, nStateID, pRect, pClipRect )
   {
     RECT pRect;

     pRect.top    = hb_parvnl( 5, 1 );
     pRect.left   = hb_parvnl( 5, 2 );
     pRect.bottom = hb_parvnl( 5, 3 );
     pRect.right  = hb_parvnl( 5, 4 );

         hb_retni( (int) C5_DrawThemeBackground( ( HTHEME ) hb_parnl( 1 ),
                                          (HDC) hb_parnl( 2 ),
                                                hb_parni( 3 ),
                                                hb_parni( 4 ),
                                                &pRect,
                                                NULL ));
   }

/*HRESULT DrawThemeIcon(          HTHEME hTheme,
    HDC hdc,
    int iPartId,
    int iStateId,
    const RECT *pRect,
    HIMAGELIST himl,
    int iImageIndex
);*/

   HB_FUNC( C5_DRAWTHEMEICON ) //( ::hTheme, hDC, nPartID, nStateID, pRect, himl, index )
   {
     typedef HRESULT  ( FAR PASCAL  *LPDRAWTHEMEICON )( HTHEME hTheme, HDC hdc, int iPartId, int iStateId, const RECT *pRect, HIMAGELIST il, int index );
     HINSTANCE hLib;
     LPDRAWTHEMEICON DrawThemeIcon;
     int iRet;
     RECT pRect;

     pRect.top    = hb_parvnl( 5, 1 );
     pRect.left   = hb_parvnl( 5, 2 );
     pRect.bottom = hb_parvnl( 5, 3 );
     pRect.right  = hb_parvnl( 5, 4 );

     hLib = LoadLibrary( "uxtheme.dll" );
     if ( hLib )
     {
         DrawThemeIcon = (LPDRAWTHEMEICON) GetProcAddress( hLib, "DrawThemeIcon" );
         iRet = DrawThemeIcon( ( HTHEME )     hb_parnl( 1 ),
                               ( HDC )        hb_parnl( 2 ),
                                              hb_parni( 3 ),
                                              hb_parni( 4 ),
                                              &pRect,
                               ( HIMAGELIST ) hb_parnl( 6 ),
                                              hb_parni( 7 ));
         FreeLibrary( hLib );
     }
     hb_retni( iRet );
   }

/*HRESULT DrawThemeText(          HTHEME hTheme,
    HDC hdc,
    int iPartId,
    int iStateId,
    LPCWSTR pszText,
    int iCharCount,
    DWORD dwTextFlags,
    DWORD dwTextFlags2,
    const RECT *pRect
);*/


   HB_FUNC( C5_DRAWTHEMETEXT ) //( ::hWnd, cTheme )
   {
     typedef HRESULT ( FAR PASCAL  *LPDRAWTHEMETEXT )( HTHEME hTheme, HDC hdc, int iPartId, int iStateId,
                                                        LPCWSTR pszText, int iCharCount, DWORD dwTextFlags,
                                                        DWORD dwTextFlags2, RECT *rc );

     LPCWSTR pszText = ( LPCWSTR ) AnsiToWide( ( char * ) hb_parc( 5 ) );
     HINSTANCE hLib;
     LPDRAWTHEMETEXT DrawThemeText;
     RECT rc;

     rc.top     = hb_parvnl( 8, 1 );
     rc.left    = hb_parvnl( 8, 2 );
     rc.bottom  = hb_parvnl( 8, 3 );
     rc.right   = hb_parvnl( 8, 4 );


     hLib = LoadLibrary( "uxtheme.dll" );

     if ( hLib )
     {
         DrawThemeText = (LPDRAWTHEMETEXT) GetProcAddress( hLib, "DrawThemeText" );
         hb_retni( ( LONG ) DrawThemeText( ( HTHEME ) hb_parnl( 1 ),
                            ( HDC )    hb_parnl( 2 ),
                            hb_parni( 3 ),
                            hb_parni( 4 ),
                 (LPCWSTR)  pszText,
                            -1,
                            hb_parnl( 6 ),
                            hb_parnl( 7 ),
                            &rc ) );          //
         FreeLibrary( hLib );
     }
     hb_xfree( (LPSTR)pszText );
   }

/*HRESULT GetThemeBackgroundContentRect(          HTHEME hTheme,
    HDC hdc,
    int iPartId,
    int iStateId,
    const RECT *pBoundingRect,
    RECT *pContentRect
);
*/

/*
HRESULT GetThemeColor(          HTHEME hTheme,
    int iPartId,
    int iStateId,
    int iPropId,
    COLORREF *pColor
);
*/


   HB_FUNC( C5_GETTHEMECOLOR )
   {
     typedef HRESULT ( FAR PASCAL  *LPGETTHEMECOLOR )( HTHEME hTheme, int iPartId, int iStateId, int iPropId, COLORREF *pColor );

     HINSTANCE hLib;
     LPGETTHEMECOLOR GetThemeColor;
     COLORREF color = RGB(255,255,255);

     hLib = LoadLibrary( "uxtheme.dll" );
     if ( hLib )
     {
         GetThemeColor = (LPGETTHEMECOLOR) GetProcAddress( hLib, "GetThemeColor" );
         GetThemeColor( ( HTHEME ) hb_parnl( 1 ),
                            hb_parni( 2 ),
                            hb_parni( 3 ),
                            hb_parni( 4 ),
                            &color  );
         FreeLibrary( hLib );
     }
     hb_retnl( (LONG) color );
   }
/*HRESULT GetThemeFont(          HTHEME hTheme,
    HDC hdc,
    int iPartId,
    int iStateId,
    int iPropId,
    LOGFONTW *pFont
);
*/
   HB_FUNC( C5_GETTHEMEFONT )
   {
     typedef HRESULT  ( FAR PASCAL  *LPGETTHEMEFONT) ( HTHEME hTheme, HDC hDc, int iPartId, int iStateId, int iPropId, LOGFONT *pFont );

     HINSTANCE hLib;

     LPGETTHEMEFONT GetThemeFont;
     LOGFONT lf;
     BOOL bUnder = hb_parl( 5 );

     ZeroMemory( &lf,sizeof( LOGFONT ) );

     hLib = LoadLibrary( "uxtheme.dll" );
     if ( hLib )
     {
         GetThemeFont = (LPGETTHEMEFONT) GetProcAddress( hLib, "GetThemeFont" );
         GetThemeFont( ( HTHEME ) hb_parnl( 1 ),
                       ( HDC )    hb_parnl( 2 ),
                                  5,  //hb_parni( 3 ),
                                  ( int ) NULL,  //hb_parni( 4 ),
                                  210,
                                  &lf );
         FreeLibrary( hLib );
     }
     lf.lfUnderline = (BYTE) bUnder;
     hb_retnl( (LONG) CreateFontIndirect( &lf ) );

   }
/*HRESULT GetThemeTextExtent(          HTHEME hTheme,
    HDC hdc,
    int iPartId,
    int iStateId,
    LPCWSTR pszText,
    int iCharCount,
    DWORD dwTextFlags,
    LPCRECT pBoundingRect,
    LPRECT pExtentRect
);
*/





// ImageList

HB_FUNC( IMAGELIST_LOADIMAGE )
{
   hb_retnl( (LONG) ImageList_LoadImageA( (HINSTANCE) hb_parnl(1),
                                           HB_ISCHAR(2)?(LPCSTR) hb_parc(2) : MAKEINTRESOURCE(hb_parni(2))   ,
                                           hb_parni(3)           ,
                                           hb_parni(4)           ,
                                           (COLORREF) hb_parnl(5),
                                           (UINT) hb_parni(6)    ,
                                           (UINT) hb_parni(7)))  ;
}


HB_FUNC( IMAGELIST_CREATE )
{
   HIMAGELIST imagelist;
   imagelist = ImageList_Create( hb_parni(1), hb_parni(2), hb_parnl(3), hb_parni(4), hb_parni(5));
   hb_retnl((LONG) imagelist);
}

HB_FUNC( IMAGELIST_ADD )
{
   hb_retni( ImageList_Add( (HIMAGELIST) hb_parnl( 1 ), (HBITMAP) hb_parnl( 2 ), (HBITMAP) hb_parnl( 3 ))) ;
}

HB_FUNC( IMAGELIST_ADDMASKED )
{
   hb_retni( ImageList_AddMasked( (HIMAGELIST) hb_parnl( 1 ), (HBITMAP) hb_parnl( 2 ), (COLORREF) hb_parnl( 3 )));
}

HB_FUNC( IMAGELIST_DESTROY )
{
   hb_retl( ImageList_Destroy( (HIMAGELIST) hb_parnl(1) ) );
}

HB_FUNC( IMAGELIST_GETICONSIZE )
{
   int cx = 0;
   int cy = 0;

   ImageList_GetIconSize( (HIMAGELIST) hb_parnl(1), &cx, &cy );
   hb_reta( 2 );
   hb_storvni( cx, 1 );
   hb_storvni( cy, 2 );
}

HB_FUNC( IMAGELIST_DRAW )
{
   hb_retl( ImageList_Draw((HIMAGELIST) hb_parnl( 1 ),
                                        hb_parni(2)    ,
                                        (HDC) hb_parnl(3), hb_parni(4), hb_parni(5),
                                       (UINT) hb_parni(6)))                       ;

}




// Get the starting RGB values and calculate the incremental
// changes to be applied.
void GradientFill0( HDC hDC, RECT* rct, COLORREF crStart, COLORREF crEnd, int bVertical )
{
   int nSegments; // = 80;
   COLORREF cr;
   int nR  = GetRValue(crStart);
   int nG  = GetGValue(crStart);
   int nB  = GetBValue(crStart);
   int neB = GetBValue(crEnd);
   int neG = GetGValue(crEnd);
   int neR = GetRValue(crEnd);
   int ndR;
   int ndG;
   int ndB;
   int nCX;
   int nCY;
   int nTop;
   int nBottom;
   int nLeft;
   int nRight;
   int nDiffR = (neR - nR);
   int nDiffG = (neG - nG);
   int nDiffB = (neB - nB);
   int i;
   RECT rc;

   if( bVertical )
   {
      nSegments = ( rct->bottom - rct->top );
   }else
   {
      nSegments = ( rct->right - rct->left );
   }
   if( nSegments > 150 )
       nSegments = 150;


   ndR = 256 * (nDiffR) / (max(nSegments,1));
   ndG = 256 * (nDiffG) / (max(nSegments,1));
   ndB = 256 * (nDiffB) / (max(nSegments,1));

   nCX = (rct->right-rct->left) / max(nSegments,1);
   nCY = (rct->bottom-rct->top) / max(nSegments,1);
   nTop = rct->top;
   nBottom = rct->bottom;
   nLeft = rct->left;
   nRight = rct->right;

   HB_SYMBOL_UNUSED( nRight );
   HB_SYMBOL_UNUSED( nBottom );

   nR *= 256;
   nG *= 256;
   nB *= 256;

   for (i = 0; i < nSegments; i++, nR += ndR, nG += ndG, nB += ndB)
   {
      // Use special code for the last segment to avoid any problems
      // with integer division.

      if (i == (nSegments - 1))
      {
         nRight  = rct->right;
         nBottom = rct->bottom;
      }
      else
      {
         nBottom = nTop + nCY;
         nRight = nLeft + nCX;
      }

      cr = RGB(nR / 256, nG / 256, nB / 256);

      {

         if( bVertical )
         {
            rc.top = nTop;
            rc.left = rct->left;
            rc.bottom = nBottom + 1;
            rc.right = rct->right;
            FillSolidRect(hDC, &rc, cr );
         }
         else
         {
            rc.top = rct->top;
            rc.left = nLeft;
            rc.bottom = rct->bottom;
            rc.right = nRight + 1;
            FillSolidRect(hDC, &rc, cr );
         }

         //(HBRUSH) SelectObject( hDC, pbrOld );
         //DeleteObject( hBrush );
      }

      // Reset the left side of the drawing rectangle.

      nLeft = nRight;
      nTop = nBottom;
   }

}

HB_FUNC( DEGRADASO )
{
        RECT rct;

        rct.top    = hb_parvni( 2, 1 );
        rct.left   = hb_parvni( 2, 2 );
        rct.bottom = hb_parvni( 2, 3 );
        rct.right  = hb_parvni( 2, 4 );

        GradientFill0( ( HDC ) hb_parnl( 1 ) , &rct, hb_parnl( 3 ), hb_parnl( 4 ), hb_parl(5));

}

// BOOL WINAPI _TrackMouseEvent( LPTRACKMOUSEEVENT lpEventTrack );

BOOL StartTrackMouseLeave( HWND m_hWnd )
{
           TRACKMOUSEEVENT tme;
           tme.cbSize = sizeof(tme);
           tme.dwFlags = TME_LEAVE;
           tme.hwndTrack = m_hWnd;
           return _TrackMouseEvent(&tme);
}


HB_FUNC( STARTTRACKMOUSELEAVE )
{
  hb_retl( StartTrackMouseLeave( (HWND) hb_parnl( 1 ) ));

}


void C5GradientFill( HDC hDC, RECT rct, COLORREF crStart, COLORREF crEnd, int bVertical )
{
// Get the starting RGB values and calculate the incremental
// changes to be applied.

   int nSegments = 100;
   COLORREF cr;

   int nR      = GetRValue(crStart);
   int nG      = GetGValue(crStart);
   int nB      = GetBValue(crStart);
   int neB     = GetBValue(crEnd);
   int neG     = GetGValue(crEnd);
   int neR     = GetRValue(crEnd);
   int nDiffR  = (neR - nR);
   int nDiffG  = (neG - nG);
   int nDiffB  = (neB - nB);
   int ndR ;
   int ndG ;
   int ndB ;
   int nCX ;
   int nCY ;
   int nTop    = rct.top;
   int nBottom; //  = rct.bottom;
   int nLeft   = rct.left;
   int nRight; //   = rct.right;
   HBRUSH hBrush;
   RECT rc;

   int i;

   if( !bVertical )
   {
      if( nSegments > ( rct.right - rct.left ) )
      {
        nSegments = ( rct.right - rct.left );
      }
   }
   else
   {
      if( nSegments > ( rct.bottom - rct.top ) )
      {
        nSegments = ( rct.bottom - rct.top );
      }
   }

   ndR = 256 * (nDiffR) / (max(nSegments,1));
   ndG = 256 * (nDiffG) / (max(nSegments,1));
   ndB = 256 * (nDiffB) / (max(nSegments,1));
   nCX = (rct.right-rct.left) / max(nSegments,1);
   nCY = (rct.bottom-rct.top) / max(nSegments,1);



   nR *= 256;
   nG *= 256;
   nB *= 256;

   for (i = 0; i < nSegments; i++, nR += ndR, nG += ndG, nB += ndB)
   {

           if(i == (nSegments - 1))
           {
              nRight  = rct.right;
              nBottom = rct.bottom;
           }
           else
           {
              nBottom = nTop + nCY;
              nRight = nLeft + nCX;
           }

           cr = RGB(nR / 256, nG / 256, nB / 256);

           {
              hBrush = CreateSolidBrush( cr );
              if( bVertical )
              {
                 rc.top    = nTop;
                 rc.left   = rct.left;
                 rc.bottom = nBottom + 1;
                 rc.right  = rct.right;
              }
              else
              {
                 rc.top    = rct.top;
                 rc.left   = nLeft;
                 rc.bottom = rct.bottom;
                 rc.right  = nRight+1;
              }
              FillRect(hDC, &rc, hBrush );
              DeleteObject( hBrush );
           }

           nLeft = nRight;
           nTop = nBottom;
   }
}


#ifndef MIIM_FTYPE
   #define MIIM_FTYPE       0x00000100
#endif
   
#define OBM_CHECK           32760

void FillSolidRect(HDC hDC, LPCRECT lpRect, COLORREF clr);
void DrawGrayed( HDC hdc, HBITMAP hbm, int y, int x );
void DrawMasked( HDC hDC, HBITMAP hBmp, WORD wRow, WORD wCol );
void RectDisable( HDC hDC, LPRECT rc );
void WindowInset( HDC hDC, RECT * pRect );
LPSTR StrToken( LPSTR szText, WORD wOcurrence, BYTE bSeparator, LPWORD pwLen );
BOOL bStrAt( BYTE bChar, LPSTR szText );


HB_FUNC( MENUDRAWITEM2 )

{  // MenuDrawItem2( pDrawItemStruct, cPrompt, lTop, hBmp )

   LPDRAWITEMSTRUCT lpdis = (LPDRAWITEMSTRUCT) hb_parnl( 1 );
   LPSTR szPrompt = ( char * ) hb_parc( 2 );
   BOOL bTab = bStrAt( 9, szPrompt );
   //BOOL bTop = hb_parl( 3 );
   WORD wLen;
   HBITMAP hBmp = (HBITMAP) hb_parnl( 4 );
   //COLORREF nClr1 = (COLORREF) hb_parnl( 5 );
   //COLORREF nClr2 = (COLORREF) hb_parnl( 6 );
   HFONT hFont;
   HFONT hOldFont;
   BOOL bLook07 = hb_parl( 7 );
   COLORREF clrBack = ( bLook07 ? RGB(255,255,255):GetSysColor( COLOR_MENU ) );
   COLORREF clrSelect;
   int iMode;
   RECT rct;
   MENUITEMINFO    itemInfo;
   ZeroMemory(&itemInfo, sizeof(itemInfo));
   itemInfo.cbSize = sizeof(itemInfo);
   itemInfo.fMask = MIIM_ID | MIIM_TYPE;
   GetMenuItemInfo((HMENU)lpdis->hwndItem, lpdis->itemID, FALSE, &itemInfo);

   switch( lpdis->itemAction )
   {
      case ODA_DRAWENTIRE:
      case ODA_SELECT:

           FillSolidRect( lpdis->hDC, &lpdis->rcItem, clrBack );
           if( lpdis->itemState & ODS_SELECTED && !( lpdis->itemState & ODS_GRAYED ) )
           {
              if( bLook07 )
              {
                 clrSelect = RGB(255,238,194);
              }
              else
              {
                 clrSelect = GetSysColor( COLOR_HIGHLIGHT );
              }
              lpdis->rcItem.left += 19;
           }
           else
           {
              clrSelect = clrBack;
           }
           FillSolidRect( lpdis->hDC, &lpdis->rcItem, clrSelect );

           if( itemInfo.fType & MFT_SEPARATOR)
           {
                        MoveToEx(lpdis->hDC, lpdis->rcItem.left+(lpdis->rcItem.bottom-lpdis->rcItem.top)+ 5, lpdis->rcItem.top+3, NULL );
                        LineTo(lpdis->hDC, lpdis->rcItem.right, lpdis->rcItem.top+3 );
           }


           if( lpdis->itemState & ODS_SELECTED && !( lpdis->itemState & ODS_GRAYED ) )
              lpdis->rcItem.left -= 19;

           rct.top    = lpdis->rcItem.top;
           rct.left   = lpdis->rcItem.left;
           rct.right  = 17;
           rct.bottom = lpdis->rcItem.bottom - 1;

           if( lpdis->itemState & ODS_SELECTED )

              if( ! ( lpdis->itemState & ODS_GRAYED ) && !( lpdis->itemState & ODS_CHECKED ) )
                 if( hBmp )
                 {
                   // WindowRaised( lpdis->hDC, &rct );
                 }
                 else
                 {
                     rct.right += 2;
                     rct.bottom++;
                     FillSolidRect( lpdis->hDC, &rct, clrSelect );
                     rct.right -= 2;
                     rct.bottom--;
                 }

           if( lpdis->itemState & ODS_CHECKED )
           {
              HBITMAP hBmp = LoadBitmap( 0, MAKEINTRESOURCE( OBM_CHECK ) );
              if( ! ( lpdis->itemState & ODS_SELECTED ) )
                 DrawGrayed( lpdis->hDC, hBmp, lpdis->rcItem.top + 1,
                             lpdis->rcItem.left + 1 );
              else
                 DrawMasked( ( HDC ) lpdis->hDC, ( HBITMAP ) hBmp, ( WORD ) ( lpdis->rcItem.top + 1 ),
                             ( WORD ) ( lpdis->rcItem.left + 1 ) );

              DeleteObject( hBmp );
              WindowInset( lpdis->hDC, &rct );
           }

           if( hBmp )
           {
              if( ! ( lpdis->itemState & ODS_CHECKED ) )
                 DrawMasked( ( HDC ) lpdis->hDC, ( HBITMAP ) hBmp, ( WORD ) ( lpdis->rcItem.top + 1 ),
                             ( WORD ) ( lpdis->rcItem.left + 1 ) );
              else
                 if( ! ( lpdis->itemState & ODS_SELECTED ) )
                    DrawGrayed( lpdis->hDC, hBmp, lpdis->rcItem.top + 1,
                                lpdis->rcItem.left + 1 );
                 else
                    DrawMasked( ( HDC ) lpdis->hDC, ( HBITMAP ) hBmp, ( WORD ) ( lpdis->rcItem.top + 1 ),
                                ( WORD ) ( lpdis->rcItem.left + 1 ) );
           }

           lpdis->rcItem.top  += 2;
           lpdis->rcItem.left += 21;

           iMode = SetBkMode( lpdis->hDC, TRANSPARENT );
           hFont = GetFontMenu();
           hOldFont = (HFONT) SelectObject( lpdis->hDC, hFont );
           if( !bTab )
           {
              DrawText( lpdis->hDC, szPrompt, -1, &lpdis->rcItem, DT_LEFT | DT_SINGLELINE | DT_VCENTER );
           }
           else
           {
              lpdis->rcItem.right -= 21;
              StrToken( szPrompt, 1, 9, &wLen ); // 32 bits does not fill wLen before
              DrawText( lpdis->hDC, StrToken(szPrompt, 1, 9, &wLen), wLen, &lpdis->rcItem, DT_LEFT );
              StrToken( szPrompt, 2, 9, &wLen ); // 32 bits does not fill wLen before
              DrawText( lpdis->hDC, StrToken(szPrompt, 2, 9, &wLen), wLen, &lpdis->rcItem, DT_RIGHT );
              lpdis->rcItem.right += 21;
           }
           SelectObject( lpdis->hDC, hOldFont );
           DeleteObject( hFont );
           lpdis->rcItem.top  -= 2;
           lpdis->rcItem.left -= 21;
           SetBkMode( lpdis->hDC, iMode);
           if( lpdis->itemState & ODS_GRAYED )
              RectDisable( lpdis->hDC, &lpdis->rcItem );

           hb_retl( TRUE );

       break;

      case ODA_FOCUS:
           hb_retl( FALSE );
           break;
   }
}


HB_FUNC( MENUMEASUREITEM2 ) // ( pMeasureItemStruct, nLen, hMenu )
{
   LPMEASUREITEMSTRUCT lp = ( LPMEASUREITEMSTRUCT ) hb_parnl( 1 );
   HMENU hMenu = (HMENU) hb_parnl( 3 );
   MENUITEMINFO    itemInfo;
   ZeroMemory(&itemInfo, sizeof(itemInfo));
   itemInfo.cbSize = sizeof(itemInfo);
   itemInfo.fMask = MIIM_ID | MIIM_TYPE;
   GetMenuItemInfo(hMenu, lp->itemID, FALSE, &itemInfo);

   lp->itemWidth  = hb_parni( 2 )+20;
   if( itemInfo.fType & MFT_SEPARATOR)
      lp->itemHeight = 5;
   else
      lp->itemHeight = GetSystemMetrics( SM_CYMENU )+1;
}

void DrawGradientFill( HDC hDC, RECT rct, COLORREF crStart, COLORREF crEnd, int nSegments, int bVertical )
{
        // Get the starting RGB values and calculate the incremental
        // changes to be applied.

        COLORREF cr;
        int nR = GetRValue(crStart);
        int nG = GetGValue(crStart);
        int nB = GetBValue(crStart);

        int neB = GetBValue(crEnd);
        int neG = GetGValue(crEnd);
        int neR = GetRValue(crEnd);


        int nDiffR = (neR - nR);
        int nDiffG = (neG - nG);
        int nDiffB = (neB - nB);

        int ndR = 256 * (nDiffR) / (max(nSegments,1));
        int ndG = 256 * (nDiffG) / (max(nSegments,1));
        int ndB = 256 * (nDiffB) / (max(nSegments,1));

        int nCX = (rct.right-rct.left) / max(nSegments,1);
        int nCY = (rct.bottom-rct.top) / max(nSegments,1);
        int nTop = rct.top;
        int nBottom; //  = rct.bottom;
        int nLeft = rct.left;
        int nRight; //  = rct.right;

        HPEN hPen;
        HPEN hOldPen;
        HBRUSH hBrush;
        HBRUSH pbrOld;

        int i;

        if(nSegments > ( rct.right - rct.left ) )
                nSegments = ( rct.right - rct.left );


        nR *= 256;
        nG *= 256;
        nB *= 256;

        hPen    = CreatePen( PS_NULL, 1, 0 );
        hOldPen = (HPEN) SelectObject( hDC, hPen );

        for (i = 0; i < nSegments; i++, nR += ndR, nG += ndG, nB += ndB)
        {
                // Use special code for the last segment to avoid any problems
                // with integer division.

                if (i == (nSegments - 1))
                {
                        nRight  = rct.right;
                        nBottom = rct.bottom;
                }
                else
                {
                        nBottom = nTop + nCY;
                        nRight = nLeft + nCX;
                }

                cr = RGB(nR / 256, nG / 256, nB / 256);

                {

                        hBrush = CreateSolidBrush( cr );
                        pbrOld = (HBRUSH) SelectObject( hDC, hBrush );

                        if( bVertical )
                           Rectangle(hDC, rct.left, nTop, rct.right, nBottom + 1 );
                        else
                           Rectangle(hDC, nLeft, rct.top, nRight + 1, rct.bottom);

                        (HBRUSH) SelectObject( hDC, pbrOld );
                        DeleteObject( hBrush );
                }

                // Reset the left side of the drawing rectangle.

                nLeft = nRight;
                nTop = nBottom;
        }

        (HPEN) SelectObject( hDC, hOldPen );
        DeleteObject( hPen );
}


HB_FUNC( DRAWGRADIENTFILL )
{
        RECT rct;

        rct.top    = hb_parvni( 2, 1 );
        rct.left   = hb_parvni( 2, 2 );
        rct.bottom = hb_parvni( 2, 3 );
        rct.right  = hb_parvni( 2, 4 );

        DrawGradientFill( ( HDC ) hb_parnl( 1 ) , rct, hb_parnl( 3 ), hb_parnl( 4 ), hb_parni(5), hb_parl( 6 ) );

}



HB_FUNC( VGRADIENTFILL )
{

        RECT rct;

        rct.top    = hb_parvni( 2, 1 );
        rct.left   = hb_parvni( 2, 2 );
        rct.bottom = hb_parvni( 2, 3 );
        rct.right  = hb_parvni( 2, 4 );

        DrawGradientFill( ( HDC ) hb_parnl( 1 ) , rct, hb_parnl( 3 ), hb_parnl( 4 ), hb_parni(5), TRUE );


}
HB_FUNC( HGRADIENTFILL )
{
        RECT rct;

        rct.top    = hb_parvni( 2, 1 );
        rct.left   = hb_parvni( 2, 2 );
        rct.bottom = hb_parvni( 2, 3 );
        rct.right  = hb_parvni( 2, 4 );

        DrawGradientFill( ( HDC ) hb_parnl( 1 ) , rct, hb_parnl( 3 ), hb_parnl( 4 ), hb_parni(5), FALSE );


}

HB_FUNC( CHGMENUMEA )
{
   LPMEASUREITEMSTRUCT lp = ( LPMEASUREITEMSTRUCT ) hb_parnl( 1 );

   lp->itemWidth  = hb_parni( 2 ) ;
   lp->itemHeight = hb_parni( 3 ) ;

}


HB_FUNC( GETDRAWISR ) // ( pDrawitemstruct )
{
   LPDRAWITEMSTRUCT lp = ( LPDRAWITEMSTRUCT ) hb_parnl( 1 );

   hb_reta( 12 );

   hb_storvni( lp->CtlType          , -1, 1  );
   hb_storvni( lp->CtlID            , -1, 2  );
   hb_storvni( lp->itemID           , -1, 3  );
   hb_storvni( lp->itemAction       , -1, 4  );
   hb_storvni( lp->itemState        , -1, 5  );
   hb_storvni( ( LONG ) lp->hwndItem, -1, 6  );
   hb_storvni( ( LONG ) lp->hDC     , -1, 7  );
   hb_storvni( lp->rcItem.top       , -1, 8  );
   hb_storvni( lp->rcItem.left      , -1, 9  );
   hb_storvni( lp->rcItem.bottom    , -1, 10 );
   hb_storvni( lp->rcItem.right     , -1, 11 );
   hb_storvnd( lp->itemData         , -1, 12 );

}

HB_FUNC( GETMEAUSUR ) // ( pMeasureItemStruct )
{
   LPMEASUREITEMSTRUCT lp = ( LPMEASUREITEMSTRUCT ) hb_parnl( 1 );

   hb_reta( 6 );

   hb_storvni( lp->CtlType      ,    -1, 1  );
   hb_storvni( lp->CtlID        ,    -1, 2  );
   hb_storvni( lp->itemID       ,    -1, 3  );
   hb_storvni( lp->itemWidth    ,    -1, 4  );
   hb_storvni( lp->itemHeight   ,    -1, 5  );
   hb_storvnd( lp->itemData     ,    -1, 12 );

}

HB_FUNC( REGISTERWINDOWMESSAGE )
{
     hb_retni( RegisterWindowMessage( ( LPCTSTR ) hb_parc(1)));
}
HB_FUNC( GETSCROLLINFOPOS )
{
   SCROLLINFO si ;
   si.cbSize = sizeof(SCROLLINFO) ;
   si.fMask  = SIF_TRACKPOS ;

   if ( GetScrollInfo( (HWND) hb_parnl( 1 ), hb_parni( 2 ), &si ) )
      hb_retni( si.nTrackPos );
   else
      hb_retni( 0 );

}

HB_FUNC( GETBMPICONEXT )
{

   SHFILEINFO    sfi;
   HDC dcMem;
   HDC hDC;
   HBITMAP bmpMem, hOldBmp;
   RECT rct;
   COLORREF nColor;

        SHGetFileInfo(
                   hb_parc( 1 ),
                   FILE_ATTRIBUTE_NORMAL,
                   &sfi,
                   sizeof(SHFILEINFO),
                   SHGFI_ICON | SHGFI_SMALLICON | SHGFI_SYSICONINDEX | SHGFI_EXETYPE);
                   //SHGFI_ICON | SHGFI_SMALLICON | SHGFI_USEFILEATTRIBUTES | SHGFI_SYSICONINDEX);


   hDC = CreateDC( "DISPLAY","","",NULL);

   dcMem = CreateCompatibleDC( hDC );
   bmpMem = CreateCompatibleBitmap( hDC, 16, 16 );
   hOldBmp = (HBITMAP) SelectObject( dcMem, bmpMem );
   rct.top    = 0;
   rct.left   = 0;
   rct.bottom = 16;
   rct.right  = 16;

   nColor = SetBkColor( dcMem, RGB(255,255,255) );
   ExtTextOut( dcMem, 0, 0, ETO_OPAQUE, &rct, NULL, 0, NULL);
   SetBkColor( dcMem, nColor );

   DrawIconEx( dcMem, 0, 0, sfi.hIcon, 16, 16, 0, NULL, DI_NORMAL );
   DestroyIcon( sfi.hIcon );
   SelectObject( dcMem, hOldBmp );
   DeleteDC( dcMem );
   DeleteDC( hDC );
   hb_retnl( (LONG) bmpMem );
}

void DrawBitmapEx( HDC hdc, HBITMAP hbm, WORD wCol, WORD wRow, WORD wWidth,
                 WORD wHeight, DWORD dwRaster )
{
    HDC       hDcMem, hDcMemX;
    BITMAP    bm, bmx;
    HBITMAP   hBmpOld, hbmx, hBmpOldX;

    if( !hdc || !hbm )
       return;

    hDcMem  = CreateCompatibleDC( hdc );
    hBmpOld = ( HBITMAP ) SelectObject( hDcMem, hbm );

    if( ! dwRaster )
       dwRaster = SRCCOPY;

    GetObject( hbm, sizeof( BITMAP ), ( LPSTR ) &bm );

    if( ! wWidth || ! wHeight )
       BitBlt( hdc, wRow, wCol, bm.bmWidth, bm.bmHeight, hDcMem, 0, 0, dwRaster );
    else
    {
       hDcMemX          = CreateCompatibleDC( hdc );
       bmx              = bm;
       bmx.bmWidth      = wWidth;
       bmx.bmHeight     = wHeight;

       bmx.bmWidthBytes = ( bmx.bmWidth * bmx.bmBitsPixel + 15 ) / 16 * 2;

       hbmx = CreateBitmapIndirect( &bmx );

       SetStretchBltMode (hDcMemX, COLORONCOLOR);
       hBmpOldX = ( HBITMAP ) SelectObject( hDcMemX, hbmx );
       StretchBlt( hDcMemX, 0, 0, wWidth, wHeight, hDcMem, 0, 0,
                   bm.bmWidth, bm.bmHeight, dwRaster );
       BitBlt( hdc, wRow, wCol, wWidth, wHeight, hDcMemX, 0, 0, dwRaster );
       SelectObject( hDcMemX, hBmpOldX );
       DeleteDC( hDcMemX );
       DeleteObject( hbmx );
    }

    SelectObject( hDcMem, hBmpOld );
    DeleteDC( hDcMem );
}

HB_FUNC( DRAWBITMAPEX ) //  hDC, hBitmap, nRow, nCol, nWidth, nHeight, nRaster
{
   DrawBitmapEx( ( HDC ) hb_parnl( 1 ), ( HBITMAP ) hb_parnl( 2 ),
                 ( WORD ) hb_parni( 3 ), ( WORD ) hb_parni( 4 ),
                 ( WORD ) hb_parni( 5 ), ( WORD ) hb_parni( 6 ), hb_parnl( 7 ) );
}

static DWORD netdrive [16][16]={
        {/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xff00ff,/*6,0*/0xff00ff,/*7,0*/0xff00ff,/*8,0*/0xff00ff,/*9,0*/0xff00ff,/*10,0*/0xff00ff,/*11,0*/0xff00ff,/*12,0*/0xff00ff,/*13,0*/0xff00ff,/*14,0*/0xff00ff,/*15,0*/0xff00ff},
        {/*0,1*/0xff00ff,/*1,1*/0xff00ff,/*2,1*/0xff00ff,/*3,1*/0xff00ff,/*4,1*/0xdbdbdb,/*5,1*/0xc8c8c8,/*6,1*/0xd5d5d5,/*7,1*/0xff00ff,/*8,1*/0xcdcdcd,/*9,1*/0xd4d4d4,/*10,1*/0xd4d4d4,/*11,1*/0xff00ff,/*12,1*/0xff00ff,/*13,1*/0xff00ff,/*14,1*/0xff00ff,/*15,1*/0xff00ff},
        {/*0,2*/0xe3e3e3,/*1,2*/0xe0e0e0,/*2,2*/0xbbbbbb,/*3,2*/0x797979,/*4,2*/0x6c6c6c,/*5,2*/0xb8b8b8,/*6,2*/0xff00ff,/*7,2*/0xff00ff,/*8,2*/0xff00ff,/*9,2*/0xdfdfdf,/*10,2*/0xf7f7f7,/*11,2*/0xcacaca,/*12,2*/0xc1c1c1,/*13,2*/0xcbcbcb,/*14,2*/0xff00ff,/*15,2*/0xff00ff},
        {/*0,3*/0xbababa,/*1,3*/0xd3d3d3,/*2,3*/0xdedede,/*3,3*/0xc7c7c7,/*4,3*/0xe1e1e1,/*5,3*/0xdfdfdf,/*6,3*/0xff00ff,/*7,3*/0xff00ff,/*8,3*/0xff00ff,/*9,3*/0xdfdfdf,/*10,3*/0xf7f7f7,/*11,3*/0xeaeaee,/*12,3*/0xccccde,/*13,3*/0x8a8a8f,/*14,3*/0x89898d,/*15,3*/0xff00ff},
        {/*0,4*/0xa0a0a0,/*1,4*/0x9c9c9c,/*2,4*/0xb5b5b5,/*3,4*/0xc7c7c7,/*4,4*/0xe1e1e1,/*5,4*/0xe6e6e6,/*6,4*/0xd0d0d0,/*7,4*/0xd8d8d8,/*8,4*/0xd1d1d1,/*9,4*/0xc3c3c3,/*10,4*/0x99999a,/*11,4*/0x78787d,/*12,4*/0x434346,/*13,4*/0x818193,/*14,4*/0x767684,/*15,4*/0x7d7d7d},
        {/*0,5*/0xa6a6a6,/*1,5*/0xaeaeae,/*2,5*/0x989898,/*3,5*/0x828282,/*4,5*/0xa4a4a4,/*5,5*/0xb9b9b9,/*6,5*/0xd1d1d1,/*7,5*/0x8e8e8e,/*8,5*/0x797979,/*9,5*/0x6c6c6c,/*10,5*/0x818181,/*11,5*/0x626262,/*12,5*/0x343434,/*13,5*/0x9898b7,/*14,5*/0x77778a,/*15,5*/0x6b6b6b},
        {/*0,6*/0xff00ff,/*1,6*/0x898989,/*2,6*/0x898989,/*3,6*/0xc9c9c9,/*4,6*/0xa3a3a3,/*5,6*/0x808080,/*6,6*/0x828282,/*7,6*/0x7c7c7d,/*8,6*/0x4e4e4e,/*9,6*/0x717171,/*10,6*/0x7d7d7d,/*11,6*/0x6e6e74,/*12,6*/0x656574,/*13,6*/0x515156,/*14,6*/0x525252,/*15,6*/0x7b7b7b},
        {/*0,7*/0xff00ff,/*1,7*/0xff00ff,/*2,7*/0xc0c0c0,/*3,7*/0x818181,/*4,7*/0x757575,/*5,7*/0x8d8d8d,/*6,7*/0xa5a5a5,/*7,7*/0x707075,/*8,7*/0x3c3c3f,/*9,7*/0x444449,/*10,7*/0x4f4f58,/*11,7*/0x4c4c4e,/*12,7*/0x565656,/*13,7*/0x727272,/*14,7*/0xa8a8a8,/*15,7*/0xdcdcdc},
        {/*0,8*/0xff00ff,/*1,8*/0xff00ff,/*2,8*/0xff00ff,/*3,8*/0xff00ff,/*4,8*/0xcfcfcf,/*5,8*/0x939393,/*6,8*/0x505050,/*7,8*/0x81828e,/*8,8*/0x303034,/*9,8*/0x1e1e1e,/*10,8*/0x545454,/*11,8*/0x898989,/*12,8*/0xc0c0c0,/*13,8*/0xe5e5e5,/*14,8*/0xff00ff,/*15,8*/0xff00ff},
        {/*0,9*/0xff00ff,/*1,9*/0xff00ff,/*2,9*/0xff00ff,/*3,9*/0xff00ff,/*4,9*/0xff00ff,/*5,9*/0xff00ff,/*6,9*/0x828282,/*7,9*/0x95a1aa,/*8,9*/0x151515,/*9,9*/0x393939,/*10,9*/0xbdbdbd,/*11,9*/0xff00ff,/*12,9*/0xff00ff,/*13,9*/0xff00ff,/*14,9*/0xff00ff,/*15,9*/0xff00ff},
        {/*0,10*/0xff00ff,/*1,10*/0xff00ff,/*2,10*/0xff00ff,/*3,10*/0xff00ff,/*4,10*/0xff00ff,/*5,10*/0xd2a726,/*6,10*/0xd8b141,/*7,10*/0xd2b049,/*8,10*/0xc69f2c,/*9,10*/0xc8a645,/*10,10*/0xcecece,/*11,10*/0xff00ff,/*12,10*/0xff00ff,/*13,10*/0xff00ff,/*14,10*/0xff00ff,/*15,10*/0xff00ff},
        {/*0,11*/0x999999,/*1,11*/0x999999,/*2,11*/0x999999,/*3,11*/0x999999,/*4,11*/0x999999,/*5,11*/0xd8b64e,/*6,11*/0xefebda,/*7,11*/0xffffff,/*8,11*/0xf5eed4,/*9,11*/0xbe9016,/*10,11*/0x999999,/*11,11*/0x999999,/*12,11*/0x999999,/*13,11*/0x999999,/*14,11*/0x999999,/*15,11*/0xff00ff},
        {/*0,12*/0x999999,/*1,12*/0xdff1ff,/*2,12*/0xdff1ff,/*3,12*/0xe0f1ff,/*4,12*/0xe0f1ff,/*5,12*/0xd5ad33,/*6,12*/0xefe5bb,/*7,12*/0xfff7cd,/*8,12*/0xf2e4af,/*9,12*/0xbd8f1a,/*10,12*/0xdff1ff,/*11,12*/0xdff1ff,/*12,12*/0xdff1ff,/*13,12*/0xdff1ff,/*14,12*/0x272727,/*15,12*/0x595959},
        {/*0,13*/0x272727,/*1,13*/0x272727,/*2,13*/0x272727,/*3,13*/0x272727,/*4,13*/0x272727,/*5,13*/0xd9ad16,/*6,13*/0xebc12c,/*7,13*/0xdfb82b,/*8,13*/0xeac029,/*9,13*/0xaf7f06,/*10,13*/0x272727,/*11,13*/0x272727,/*12,13*/0x272727,/*13,13*/0x272727,/*14,13*/0x272727,/*15,13*/0x414141},
        {/*0,14*/0xff00ff,/*1,14*/0x717171,/*2,14*/0x595959,/*3,14*/0x595959,/*4,14*/0x595959,/*5,14*/0xa97702,/*6,14*/0xa37202,/*7,14*/0xa37202,/*8,14*/0xa87603,/*9,14*/0xa27002,/*10,14*/0x414141,/*11,14*/0x595959,/*12,14*/0x595959,/*13,14*/0x595959,/*14,14*/0x595959,/*15,14*/0x595959},
        {/*0,15*/0xff00ff,/*1,15*/0xff00ff,/*2,15*/0xff00ff,/*3,15*/0xff00ff,/*4,15*/0xff00ff,/*5,15*/0xd0d0d0,/*6,15*/0x717171,/*7,15*/0x595959,/*8,15*/0x595959,/*9,15*/0x595959,/*10,15*/0x717171,/*11,15*/0xff00ff,/*12,15*/0xff00ff,/*13,15*/0xff00ff,/*14,15*/0xff00ff,/*15,15*/0xff00ff}
};

static DWORD hddisk [16][16]={
        {/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xff00ff,/*6,0*/0xff00ff,/*7,0*/0xff00ff,/*8,0*/0xff00ff,/*9,0*/0xff00ff,/*10,0*/0xff00ff,/*11,0*/0xff00ff,/*12,0*/0xff00ff,/*13,0*/0xff00ff,/*14,0*/0xff00ff,/*15,0*/0xff00ff},
        {/*0,1*/0xff00ff,/*1,1*/0xff00ff,/*2,1*/0xff00ff,/*3,1*/0xff00ff,/*4,1*/0xff00ff,/*5,1*/0xff00ff,/*6,1*/0xff00ff,/*7,1*/0xff00ff,/*8,1*/0xff00ff,/*9,1*/0xff00ff,/*10,1*/0xff00ff,/*11,1*/0xff00ff,/*12,1*/0xff00ff,/*13,1*/0xff00ff,/*14,1*/0xff00ff,/*15,1*/0xff00ff},
        {/*0,2*/0xff00ff,/*1,2*/0xff00ff,/*2,2*/0xff00ff,/*3,2*/0xff00ff,/*4,2*/0xff00ff,/*5,2*/0xff00ff,/*6,2*/0xff00ff,/*7,2*/0xff00ff,/*8,2*/0xff00ff,/*9,2*/0xff00ff,/*10,2*/0xff00ff,/*11,2*/0xff00ff,/*12,2*/0xff00ff,/*13,2*/0xff00ff,/*14,2*/0xff00ff,/*15,2*/0xff00ff},
        {/*0,3*/0xff00ff,/*1,3*/0xff00ff,/*2,3*/0xff00ff,/*3,3*/0xececec,/*4,3*/0xdbdbdb,/*5,3*/0xc8c8c8,/*6,3*/0xd5d5d5,/*7,3*/0xe6e6e6,/*8,3*/0xcdcdcd,/*9,3*/0xd4d4d4,/*10,3*/0xd4d4d4,/*11,3*/0xff00ff,/*12,3*/0xff00ff,/*13,3*/0xff00ff,/*14,3*/0xff00ff,/*15,3*/0xff00ff},
        {/*0,4*/0xe3e3e3,/*1,4*/0xe0e0e0,/*2,4*/0xbbbbbb,/*3,4*/0x797979,/*4,4*/0x6c6c6c,/*5,4*/0xb8b8b8,/*6,4*/0xf2f2f2,/*7,4*/0xfcfcfc,/*8,4*/0xececec,/*9,4*/0xdfdfdf,/*10,4*/0xf7f7f7,/*11,4*/0xcacaca,/*12,4*/0xc1c1c1,/*13,4*/0xcbcbcb,/*14,4*/0xff00ff,/*15,4*/0xff00ff},
        {/*0,5*/0xbababa,/*1,5*/0xd3d3d3,/*2,5*/0xdedede,/*3,5*/0xc7c7c7,/*4,5*/0xe1e1e1,/*5,5*/0xdfdfdf,/*6,5*/0xf2f2f2,/*7,5*/0xfcfcfc,/*8,5*/0xececec,/*9,5*/0xdfdfdf,/*10,5*/0xf7f7f7,/*11,5*/0xeaeaee,/*12,5*/0xccccde,/*13,5*/0x8a8a8f,/*14,5*/0x89898d,/*15,5*/0xe7e7e7},
        {/*0,6*/0xa0a0a0,/*1,6*/0x9c9c9c,/*2,6*/0xb5b5b5,/*3,6*/0xc7c7c7,/*4,6*/0xe1e1e1,/*5,6*/0xe6e6e6,/*6,6*/0xd0d0d0,/*7,6*/0xd8d8d8,/*8,6*/0xd1d1d1,/*9,6*/0xc3c3c3,/*10,6*/0x99999a,/*11,6*/0x78787d,/*12,6*/0x434346,/*13,6*/0x818193,/*14,6*/0x767684,/*15,6*/0x7d7d7d},
        {/*0,7*/0xa6a6a6,/*1,7*/0xaeaeae,/*2,7*/0x989898,/*3,7*/0x828282,/*4,7*/0xa4a4a4,/*5,7*/0xb9b9b9,/*6,7*/0xd1d1d1,/*7,7*/0x8e8e8e,/*8,7*/0x797979,/*9,7*/0x6c6c6c,/*10,7*/0x818181,/*11,7*/0x626262,/*12,7*/0x343434,/*13,7*/0x9898b7,/*14,7*/0x77778a,/*15,7*/0x6b6b6b},
        {/*0,8*/0xf1f1f1,/*1,8*/0x898989,/*2,8*/0x898989,/*3,8*/0xc9c9c9,/*4,8*/0xa3a3a3,/*5,8*/0x808080,/*6,8*/0x828282,/*7,8*/0x7c7c7d,/*8,8*/0x4e4e4e,/*9,8*/0x717171,/*10,8*/0x7d7d7d,/*11,8*/0x6e6e74,/*12,8*/0x656574,/*13,8*/0x515156,/*14,8*/0x525252,/*15,8*/0x7b7b7b},
        {/*0,9*/0xff00ff,/*1,9*/0xebebeb,/*2,9*/0xc0c0c0,/*3,9*/0x818181,/*4,9*/0x757575,/*5,9*/0x8d8d8d,/*6,9*/0xa5a5a5,/*7,9*/0x707075,/*8,9*/0x3c3c3f,/*9,9*/0x444449,/*10,9*/0x52525a,/*11,9*/0x4c4c4e,/*12,9*/0x565656,/*13,9*/0x727272,/*14,9*/0xa8a8a8,/*15,9*/0xdcdcdc},
        {/*0,10*/0xff00ff,/*1,10*/0xff00ff,/*2,10*/0xff00ff,/*3,10*/0xededed,/*4,10*/0xcfcfcf,/*5,10*/0x939393,/*6,10*/0x676767,/*7,10*/0x84848e,/*8,10*/0x4d4d52,/*9,10*/0x4a4a4a,/*10,10*/0x5d5d5d,/*11,10*/0x898989,/*12,10*/0xc0c0c0,/*13,10*/0xe5e5e5,/*14,10*/0xff00ff,/*15,10*/0xff00ff},
        {/*0,11*/0xff00ff,/*1,11*/0xff00ff,/*2,11*/0xff00ff,/*3,11*/0xff00ff,/*4,11*/0xff00ff,/*5,11*/0xf1f1f1,/*6,11*/0xd9d9d9,/*7,11*/0xababab,/*8,11*/0x8a8a8a,/*9,11*/0xa3a3a3,/*10,11*/0xd0d0d0,/*11,11*/0xededed,/*12,11*/0xff00ff,/*13,11*/0xff00ff,/*14,11*/0xff00ff,/*15,11*/0xff00ff},
        {/*0,12*/0xff00ff,/*1,12*/0xff00ff,/*2,12*/0xff00ff,/*3,12*/0xff00ff,/*4,12*/0xff00ff,/*5,12*/0xff00ff,/*6,12*/0xff00ff,/*7,12*/0xff00ff,/*8,12*/0xeeeeee,/*9,12*/0xff00ff,/*10,12*/0xff00ff,/*11,12*/0xff00ff,/*12,12*/0xff00ff,/*13,12*/0xff00ff,/*14,12*/0xff00ff,/*15,12*/0xff00ff},
        {/*0,13*/0xff00ff,/*1,13*/0xff00ff,/*2,13*/0xff00ff,/*3,13*/0xff00ff,/*4,13*/0xff00ff,/*5,13*/0xff00ff,/*6,13*/0xff00ff,/*7,13*/0xff00ff,/*8,13*/0xff00ff,/*9,13*/0xff00ff,/*10,13*/0xff00ff,/*11,13*/0xff00ff,/*12,13*/0xff00ff,/*13,13*/0xff00ff,/*14,13*/0xff00ff,/*15,13*/0xff00ff},
        {/*0,14*/0xff00ff,/*1,14*/0xff00ff,/*2,14*/0xff00ff,/*3,14*/0xff00ff,/*4,14*/0xff00ff,/*5,14*/0xff00ff,/*6,14*/0xff00ff,/*7,14*/0xff00ff,/*8,14*/0xff00ff,/*9,14*/0xff00ff,/*10,14*/0xff00ff,/*11,14*/0xff00ff,/*12,14*/0xff00ff,/*13,14*/0xff00ff,/*14,14*/0xff00ff,/*15,14*/0xff00ff},
        {/*0,15*/0xff00ff,/*1,15*/0xff00ff,/*2,15*/0xff00ff,/*3,15*/0xff00ff,/*4,15*/0xff00ff,/*5,15*/0xff00ff,/*6,15*/0xff00ff,/*7,15*/0xff00ff,/*8,15*/0xff00ff,/*9,15*/0xff00ff,/*10,15*/0xff00ff,/*11,15*/0xff00ff,/*12,15*/0xff00ff,/*13,15*/0xff00ff,/*14,15*/0xff00ff,/*15,15*/0xff00ff}
};

static DWORD carpeta [16][16]={
        {/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xff00ff,/*6,0*/0xff00ff,/*7,0*/0xff00ff,/*8,0*/0xff00ff,/*9,0*/0xff00ff,/*10,0*/0xff00ff,/*11,0*/0xff00ff,/*12,0*/0xff00ff,/*13,0*/0xff00ff,/*14,0*/0xff00ff,/*15,0*/0xff00ff},
        {/*0,1*/0xff00ff,/*1,1*/0xff00ff,/*2,1*/0xff00ff,/*3,1*/0x808000,/*4,1*/0x808000,/*5,1*/0x808000,/*6,1*/0x808000,/*7,1*/0x808000,/*8,1*/0x0,/*9,1*/0xff00ff,/*10,1*/0xff00ff,/*11,1*/0xff00ff,/*12,1*/0xff00ff,/*13,1*/0xff00ff,/*14,1*/0xff00ff,/*15,1*/0xff00ff},
        {/*0,2*/0xff00ff,/*1,2*/0xff00ff,/*2,2*/0x808000,/*3,2*/0xffffff,/*4,2*/0xffffff,/*5,2*/0xffffff,/*6,2*/0xffff80,/*7,2*/0xffff80,/*8,2*/0x808000,/*9,2*/0x0,/*10,2*/0xff00ff,/*11,2*/0xff00ff,/*12,2*/0xff00ff,/*13,2*/0xff00ff,/*14,2*/0xff00ff,/*15,2*/0xff00ff},
        {/*0,3*/0xff00ff,/*1,3*/0x808000,/*2,3*/0xc0c040,/*3,3*/0xc0c040,/*4,3*/0xc0c040,/*5,3*/0xc0c040,/*6,3*/0xc0c040,/*7,3*/0xc0c040,/*8,3*/0xc0c040,/*9,3*/0x808000,/*10,3*/0x808000,/*11,3*/0x808000,/*12,3*/0x808000,/*13,3*/0x808000,/*14,3*/0x808000,/*15,3*/0xff00ff},
        {/*0,4*/0xff00ff,/*1,4*/0x808000,/*2,4*/0xffffff,/*3,4*/0xffffff,/*4,4*/0xffffff,/*5,4*/0xffffff,/*6,4*/0xffffff,/*7,4*/0xffffff,/*8,4*/0xffffff,/*9,4*/0xffffff,/*10,4*/0xffffff,/*11,4*/0xffffff,/*12,4*/0xffffff,/*13,4*/0xffff80,/*14,4*/0xc0c040,/*15,4*/0x0},
        {/*0,5*/0xff00ff,/*1,5*/0x808000,/*2,5*/0xffffff,/*3,5*/0xffff80,/*4,5*/0xffff80,/*5,5*/0xffff80,/*6,5*/0xffff80,/*7,5*/0xffff80,/*8,5*/0xffff80,/*9,5*/0xffff80,/*10,5*/0xffff80,/*11,5*/0xffff80,/*12,5*/0xffff80,/*13,5*/0xffc080,/*14,5*/0xc0c040,/*15,5*/0x0},
        {/*0,6*/0xff00ff,/*1,6*/0x808000,/*2,6*/0xffffff,/*3,6*/0xffff80,/*4,6*/0xffff80,/*5,6*/0xffff80,/*6,6*/0xffff80,/*7,6*/0xffff80,/*8,6*/0xffff80,/*9,6*/0xffff80,/*10,6*/0xffc080,/*11,6*/0xffff80,/*12,6*/0xffc080,/*13,6*/0xffff80,/*14,6*/0xc0c040,/*15,6*/0x0},
        {/*0,7*/0xff00ff,/*1,7*/0x808000,/*2,7*/0xffffff,/*3,7*/0xffff80,/*4,7*/0xffff80,/*5,7*/0xffff80,/*6,7*/0xffff80,/*7,7*/0xffff80,/*8,7*/0xffff80,/*9,7*/0xffff80,/*10,7*/0xffff80,/*11,7*/0xffc080,/*12,7*/0xffff80,/*13,7*/0xffc080,/*14,7*/0xc0c040,/*15,7*/0x0},
        {/*0,8*/0xff00ff,/*1,8*/0x808000,/*2,8*/0xffffff,/*3,8*/0xffff80,/*4,8*/0xffff80,/*5,8*/0xffff80,/*6,8*/0xffff80,/*7,8*/0xffff80,/*8,8*/0xffc080,/*9,8*/0xffff80,/*10,8*/0xffc080,/*11,8*/0xffff80,/*12,8*/0xffc080,/*13,8*/0xffff80,/*14,8*/0xc0c040,/*15,8*/0x0},
        {/*0,9*/0xff00ff,/*1,9*/0x808000,/*2,9*/0xffffff,/*3,9*/0xffff80,/*4,9*/0xffff80,/*5,9*/0xffff80,/*6,9*/0xffff80,/*7,9*/0xffff80,/*8,9*/0xffff80,/*9,9*/0xffc080,/*10,9*/0xffff80,/*11,9*/0xffc080,/*12,9*/0xffff80,/*13,9*/0xffc080,/*14,9*/0xc0c040,/*15,9*/0x0},
        {/*0,10*/0xff00ff,/*1,10*/0x808000,/*2,10*/0xffffff,/*3,10*/0xffff80,/*4,10*/0xffff80,/*5,10*/0xffff80,/*6,10*/0xffc080,/*7,10*/0xffff80,/*8,10*/0xffc080,/*9,10*/0xffff80,/*10,10*/0xffc080,/*11,10*/0xffff80,/*12,10*/0xffc080,/*13,10*/0xffc080,/*14,10*/0xc0c040,/*15,10*/0x0},
        {/*0,11*/0xff00ff,/*1,11*/0x808000,/*2,11*/0xffffff,/*3,11*/0xffc080,/*4,11*/0xffff80,/*5,11*/0xffc080,/*6,11*/0xffff80,/*7,11*/0xffc080,/*8,11*/0xffff80,/*9,11*/0xffc080,/*10,11*/0xffff80,/*11,11*/0xffc080,/*12,11*/0xffc080,/*13,11*/0xffc080,/*14,11*/0xc0c040,/*15,11*/0x0},
        {/*0,12*/0xff00ff,/*1,12*/0x808000,/*2,12*/0xc0c040,/*3,12*/0xc0c040,/*4,12*/0xc0c040,/*5,12*/0xc0c040,/*6,12*/0xc0c040,/*7,12*/0xc0c040,/*8,12*/0xc0c040,/*9,12*/0xc0c040,/*10,12*/0xc0c040,/*11,12*/0xc0c040,/*12,12*/0xc0c040,/*13,12*/0xc0c040,/*14,12*/0xc0c040,/*15,12*/0x0},
        {/*0,13*/0xff00ff,/*1,13*/0xff00ff,/*2,13*/0x0,/*3,13*/0x0,/*4,13*/0x0,/*5,13*/0x0,/*6,13*/0x0,/*7,13*/0x0,/*8,13*/0x0,/*9,13*/0x0,/*10,13*/0x0,/*11,13*/0x0,/*12,13*/0x0,/*13,13*/0x0,/*14,13*/0x0,/*15,13*/0x0},
        {/*0,14*/0xff00ff,/*1,14*/0xff00ff,/*2,14*/0xff00ff,/*3,14*/0xff00ff,/*4,14*/0xff00ff,/*5,14*/0xff00ff,/*6,14*/0xff00ff,/*7,14*/0xff00ff,/*8,14*/0xff00ff,/*9,14*/0xff00ff,/*10,14*/0xff00ff,/*11,14*/0xff00ff,/*12,14*/0xff00ff,/*13,14*/0xff00ff,/*14,14*/0xff00ff,/*15,14*/0xff00ff},
        {/*0,15*/0xff00ff,/*1,15*/0xff00ff,/*2,15*/0xff00ff,/*3,15*/0xff00ff,/*4,15*/0xff00ff,/*5,15*/0xff00ff,/*6,15*/0xff00ff,/*7,15*/0xff00ff,/*8,15*/0xff00ff,/*9,15*/0xff00ff,/*10,15*/0xff00ff,/*11,15*/0xff00ff,/*12,15*/0xff00ff,/*13,15*/0xff00ff,/*14,15*/0xff00ff,/*15,15*/0xff00ff}
};

static DWORD cdroom [16][16]={
        {/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xd0d0d0,/*6,0*/0xc2c2c5,/*7,0*/0xceced5,/*8,0*/0xb9b9c1,/*9,0*/0xcecece,/*10,0*/0xff00ff,/*11,0*/0xff00ff,/*12,0*/0xff00ff,/*13,0*/0xff00ff,/*14,0*/0xff00ff,/*15,0*/0xff00ff},
        {/*0,1*/0xff00ff,/*1,1*/0xff00ff,/*2,1*/0xff00ff,/*3,1*/0xff00ff,/*4,1*/0xbdbdbd,/*5,1*/0xececec,/*6,1*/0xf4f4ff,/*7,1*/0xebebff,/*8,1*/0xe3e3ff,/*9,1*/0xc5c5e2,/*10,1*/0xa5a5a8,/*11,1*/0xf1f1f1,/*12,1*/0xff00ff,/*13,1*/0xff00ff,/*14,1*/0xff00ff,/*15,1*/0xff00ff},
        {/*0,2*/0xff00ff,/*1,2*/0xff00ff,/*2,2*/0xff00ff,/*3,2*/0xc6c6c6,/*4,2*/0xf5f5f5,/*5,2*/0xffffff,/*6,2*/0xf3f3ff,/*7,2*/0xe3e3ff,/*8,2*/0xdadaff,/*9,2*/0xd2d2ff,/*10,2*/0xb5b5d9,/*11,2*/0xa4a4a4,/*12,2*/0xff00ff,/*13,2*/0xff00ff,/*14,2*/0xff00ff,/*15,2*/0xff00ff},
        {/*0,3*/0xff00ff,/*1,3*/0xff00ff,/*2,3*/0xff00ff,/*3,3*/0xb5b5c5,/*4,3*/0xf5f5ff,/*5,3*/0xffffff,/*6,3*/0xfde9ff,/*7,3*/0xf7daff,/*8,3*/0xe3deff,/*9,3*/0xcfcfff,/*10,3*/0xd6d6ff,/*11,3*/0x7d7d82,/*12,3*/0xd0d0d0,/*13,3*/0xff00ff,/*14,3*/0xff00ff,/*15,3*/0xff00ff},
        {/*0,4*/0xff00ff,/*1,4*/0xff00ff,/*2,4*/0xededed,/*3,4*/0xb3b3d9,/*4,4*/0xccccff,/*5,4*/0xe6e2ff,/*6,4*/0xcac0cd,/*7,4*/0x7f7e8b,/*8,4*/0xf5f0ff,/*9,4*/0xd2d2ff,/*10,4*/0xdadaff,/*11,4*/0x83838e,/*12,4*/0xa5a5a5,/*13,4*/0xff00ff,/*14,4*/0xff00ff,/*15,4*/0xff00ff},
        {/*0,5*/0xff00ff,/*1,5*/0xff00ff,/*2,5*/0xd8d8d8,/*3,5*/0xd2d2f5,/*4,5*/0xdadaff,/*5,5*/0xe9e5ff,/*6,5*/0xb5aec0,/*7,5*/0x727072,/*8,5*/0xf9f4ff,/*9,5*/0xccccff,/*10,5*/0xccccff,/*11,5*/0x72727f,/*12,5*/0x8d8d8d,/*13,5*/0xf0f0f0,/*14,5*/0xff00ff,/*15,5*/0xff00ff},
        {/*0,6*/0xff00ff,/*1,6*/0xff00ff,/*2,6*/0xff00ff,/*3,6*/0xbbbbcf,/*4,6*/0xdadaff,/*5,6*/0xd4d3ff,/*6,6*/0xf3d9ff,/*7,6*/0xf7d8ff,/*8,6*/0xfffaff,/*9,6*/0xf9f9ff,/*10,6*/0xe9e9ff,/*11,6*/0x5c5c5f,/*12,6*/0x999999,/*13,6*/0xff00ff,/*14,6*/0xff00ff,/*15,6*/0xff00ff},
        {/*0,7*/0xff00ff,/*1,7*/0xff00ff,/*2,7*/0xff00ff,/*3,7*/0xb2b2be,/*4,7*/0xd1d1ff,/*5,7*/0xceceff,/*6,7*/0xd7d7ff,/*7,7*/0xdfdfff,/*8,7*/0xfafaff,/*9,7*/0xffffff,/*10,7*/0xadadad,/*11,7*/0x545454,/*12,7*/0xb5b5b5,/*13,7*/0xff00ff,/*14,7*/0xff00ff,/*15,7*/0xff00ff},
        {/*0,8*/0xededed,/*1,8*/0xe1e1e1,/*2,8*/0xdfdfdf,/*3,8*/0xcbcbcb,/*4,8*/0xa9a9c5,/*5,8*/0xd7d7ff,/*6,8*/0xdfdfff,/*7,8*/0xe8e8ff,/*8,8*/0xf5f5ff,/*9,8*/0xcdcdcd,/*10,8*/0xb5b5b5,/*11,8*/0xa0a0a0,/*12,8*/0xbcbcbc,/*13,8*/0xd0d0d0,/*14,8*/0xff00ff,/*15,8*/0xff00ff},
        {/*0,9*/0xbebebe,/*1,9*/0xe1e1e1,/*2,9*/0xf2f2f2,/*3,9*/0xf4f4f4,/*4,9*/0xdfdfdf,/*5,9*/0xb6b6be,/*6,9*/0xc6c6d2,/*7,9*/0xbebec6,/*8,9*/0x9f9fa0,/*9,9*/0xbbbbbb,/*10,9*/0xd6d6d6,/*11,9*/0xd0d0d0,/*12,9*/0xbbbbbb,/*13,9*/0x7a7a7c,/*14,9*/0x838387,/*15,9*/0xebebeb},
        {/*0,10*/0xb9b9b9,/*1,10*/0xc2c2c2,/*2,10*/0xbebebe,/*3,10*/0xcecece,/*4,10*/0xe5e5e5,/*5,10*/0xeaeaea,/*6,10*/0xe1e1e1,/*7,10*/0xc5c5c5,/*8,10*/0xc0c0c0,/*9,10*/0xc5c5c5,/*10,10*/0x969697,/*11,10*/0x86868c,/*12,10*/0x888896,/*13,10*/0x88948a,/*14,10*/0x596655,/*15,10*/0x959595},
        {/*0,11*/0xa5a5a5,/*1,11*/0xbdbdbd,/*2,11*/0xd2d2d2,/*3,11*/0xc5c5c5,/*4,11*/0xbababa,/*5,11*/0xbebebe,/*6,11*/0xb4b4b4,/*7,11*/0x8f8f8f,/*8,11*/0x878787,/*9,11*/0x78787e,/*10,11*/0x888890,/*11,11*/0xbdbdc3,/*12,11*/0xdadadc,/*13,11*/0xd1d1d1,/*14,11*/0x6c6c72,/*15,11*/0x7e7e7e},
        {/*0,12*/0xf0f0f0,/*1,12*/0x8d8d8d,/*2,12*/0x888888,/*3,12*/0xc9c9c9,/*4,12*/0xd5d5d5,/*5,12*/0xc9c9c9,/*6,12*/0x7b7b7b,/*7,12*/0x8b8b8c,/*8,12*/0xa0a0a2,/*9,12*/0xc7c7c8,/*10,12*/0xd3d3d3,/*11,12*/0xa2a2a8,/*12,12*/0x84848c,/*13,12*/0x585862,/*14,12*/0x4f4f4f,/*15,12*/0x8c8c8c},
        {/*0,13*/0xff00ff,/*1,13*/0xeaeaea,/*2,13*/0xc0c0c0,/*3,13*/0x818181,/*4,13*/0x757575,/*5,13*/0x8d8d8d,/*6,13*/0x686868,/*7,13*/0x99999f,/*8,13*/0x9b9b9d,/*9,13*/0x858590,/*10,13*/0x5a5a62,/*11,13*/0x4b4b4d,/*12,13*/0x545454,/*13,13*/0x717171,/*14,13*/0xa0a0a0,/*15,13*/0xd6d6d6},
        {/*0,14*/0xff00ff,/*1,14*/0xff00ff,/*2,14*/0xff00ff,/*3,14*/0xededed,/*4,14*/0xcfcfcf,/*5,14*/0x939393,/*6,14*/0x545454,/*7,14*/0x7d7d84,/*8,14*/0x4d4d52,/*9,14*/0x4a4a4a,/*10,14*/0x5d5d5d,/*11,14*/0x878787,/*12,14*/0xbababa,/*13,14*/0xe4e4e4,/*14,14*/0xff00ff,/*15,14*/0xff00ff},
        {/*0,15*/0xff00ff,/*1,15*/0xff00ff,/*2,15*/0xff00ff,/*3,15*/0xff00ff,/*4,15*/0xff00ff,/*5,15*/0xf1f1f1,/*6,15*/0xdbdbdb,/*7,15*/0xb1b1b1,/*8,15*/0x8c8c8c,/*9,15*/0xa3a3a3,/*10,15*/0xd0d0d0,/*11,15*/0xededed,/*12,15*/0xff00ff,/*13,15*/0xff00ff,/*14,15*/0xff00ff,/*15,15*/0xff00ff}
};

static DWORD explorer [16][16]={
        {/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xff00ff,/*6,0*/0xff00ff,/*7,0*/0xff00ff,/*8,0*/0xff00ff,/*9,0*/0xff00ff,/*10,0*/0xff00ff,/*11,0*/0xccdde7,/*12,0*/0x87cae8,/*13,0*/0x99cce7,/*14,0*/0xb6cbda,/*15,0*/0xff00ff},
        {/*0,1*/0xff00ff,/*1,1*/0xff00ff,/*2,1*/0xff00ff,/*3,1*/0xff00ff,/*4,1*/0xff00ff,/*5,1*/0xd0dce3,/*6,1*/0xb8cdd9,/*7,1*/0x98b8cd,/*8,1*/0x8fb1c9,/*9,1*/0x64a7cc,/*10,1*/0x83d0ed,/*11,1*/0x7bc4e8,/*12,1*/0x8ebcd2,/*13,1*/0xbad5e3,/*14,1*/0x84c0df,/*15,1*/0xc8d6e1},
        {/*0,2*/0xff00ff,/*1,2*/0xff00ff,/*2,2*/0xff00ff,/*3,2*/0xd2dee4,/*4,2*/0x99ccdd,/*5,2*/0x99e0f2,/*6,2*/0x81e9fa,/*7,2*/0x6be9fb,/*8,2*/0x45ccf4,/*9,2*/0x4bcff8,/*10,2*/0x4fb2dc,/*11,2*/0x6d95b2,/*12,2*/0xbccbd6,/*13,2*/0xf7f7f7,/*14,2*/0x88c7e5,/*15,2*/0xc0d2de},
        {/*0,3*/0xff00ff,/*1,3*/0xff00ff,/*2,3*/0xcedce3,/*3,3*/0xabdce8,/*4,3*/0x91ddf6,/*5,3*/0x9ddcf4,/*6,3*/0x67d4e7,/*7,3*/0x47d1f6,/*8,3*/0x32b7f1,/*9,3*/0x2baced,/*10,3*/0x30adee,/*11,3*/0x37a6e8,/*12,3*/0x6691b4,/*13,3*/0x9ab7cb,/*14,3*/0xd6e1e9,/*15,3*/0xecf0f3},
        {/*0,4*/0xff00ff,/*1,4*/0xff00ff,/*2,4*/0xa8d0de,/*3,4*/0x89c9e4,/*4,4*/0xd0e8f0,/*5,4*/0x8dd9f0,/*6,4*/0x3bb2e1,/*7,4*/0x2581b6,/*8,4*/0x258fcd,/*9,4*/0x29a0e5,/*10,4*/0x289ce4,/*11,4*/0x2ea0ea,/*12,4*/0x3aa6e9,/*13,4*/0x6383a1,/*14,4*/0xced5dd,/*15,4*/0xf7f7f7},
        {/*0,5*/0xff00ff,/*1,5*/0x8abacf,/*2,5*/0x90ceea,/*3,5*/0xf7f7f7,/*4,5*/0xa4e1f1,/*5,5*/0x3bafe0,/*6,5*/0x28506f,/*7,5*/0x2f4259,/*8,5*/0x375570,/*9,5*/0x2579b6,/*10,5*/0x2a98e6,/*11,5*/0x2d97e7,/*12,5*/0x39a4f1,/*13,5*/0x306da3,/*14,5*/0x9babbc,/*15,5*/0xe7eaed},
        {/*0,6*/0xff00ff,/*1,6*/0x7cadc8,/*2,6*/0xf7f7f7,/*3,6*/0x81cfe8,/*4,6*/0x42c6f5,/*5,6*/0x2984b8,/*6,6*/0x25486b,/*7,6*/0xff00ff,/*8,6*/0xff00ff,/*9,6*/0x688dad,/*10,6*/0x2e83bf,/*11,6*/0x2e96e8,/*12,6*/0x359bee,/*13,6*/0x378bcf,/*14,6*/0x4e6783,/*15,6*/0xc0c7cf},
        {/*0,7*/0xdde4e9,/*1,7*/0xf7f7f7,/*2,7*/0xa4d3e8,/*3,7*/0x43c4f4,/*4,7*/0x2eacec,/*5,7*/0x279fe5,/*6,7*/0x2896de,/*7,7*/0x2995e0,/*8,7*/0x2d94e0,/*9,7*/0x3694dc,/*10,7*/0x2e93e7,/*11,7*/0x3295e9,/*12,7*/0x3a9af0,/*13,7*/0x42a1ed,/*14,7*/0x364d6d,/*15,7*/0xa5b0bf},
        {/*0,8*/0xf7f7f7,/*1,8*/0xbecfd8,/*2,8*/0x3fbdf4,/*3,8*/0x2eabed,/*4,8*/0x279de5,/*5,8*/0x2c80d0,/*6,8*/0x2c80d0,/*7,8*/0x2c80d0,/*8,8*/0x2c80d0,/*9,8*/0x2c80d0,/*10,8*/0x2c80d0,/*11,8*/0x2c80d0,/*12,8*/0x2c80d0,/*13,8*/0x2c80d0,/*14,8*/0x334864,/*15,8*/0x8094ab},
        {/*0,9*/0xeff1f2,/*1,9*/0x65b5e0,/*2,9*/0x36b4f4,/*3,9*/0x2ba0e8,/*4,9*/0x2695e2,/*5,9*/0x2067a4,/*6,9*/0x1e324e,/*7,9*/0x364f6a,/*8,9*/0x3d5470,/*9,9*/0x38567c,/*10,9*/0x2f5a8c,/*11,9*/0x2d5a8b,/*12,9*/0x306191,/*13,9*/0x416b90,/*14,9*/0x486381,/*15,9*/0x879cb3},
        {/*0,10*/0xb0cde2,/*1,10*/0x3db5f6,/*2,10*/0x34acf3,/*3,10*/0x2fa0ea,/*4,10*/0x2b96e5,/*5,10*/0x298ada,/*6,10*/0x2c5784,/*7,10*/0xff00ff,/*8,10*/0xff00ff,/*9,10*/0xa6c1de,/*10,10*/0x3286d4,/*11,10*/0x3ca8f4,/*12,10*/0x6ed1fc,/*13,10*/0x7db6d6,/*14,10*/0x4d6a84,/*15,10*/0xbac6d1},
        {/*0,11*/0x5fadde,/*1,11*/0x2e99dd,/*2,11*/0x2b7dbb,/*3,11*/0x37a6f2,/*4,11*/0x329beb,/*5,11*/0x2f93e7,/*6,11*/0x3090e7,/*7,11*/0x276db3,/*8,11*/0x2b73ba,/*9,11*/0x3195e6,/*10,11*/0x42b1f4,/*11,11*/0x6cd4fb,/*12,11*/0x94d8f4,/*13,11*/0x395470,/*14,11*/0x5a748b,/*15,11*/0xc5cfd7},
        {/*0,12*/0x4cb2eb,/*1,12*/0x4279a9,/*2,12*/0x2c486a,/*3,12*/0x2975b6,/*4,12*/0x3798e7,/*5,12*/0x399bf0,/*6,12*/0x3797ef,/*7,12*/0x379aef,/*8,12*/0x3da7f2,/*9,12*/0x52bdf7,/*10,12*/0x77d4fa,/*11,12*/0x8acaea,/*12,12*/0x3a5d78,/*13,12*/0x37495f,/*14,12*/0xa4b7c2,/*15,12*/0xff00ff},
        {/*0,13*/0x46afed,/*1,13*/0x99b2c8,/*2,13*/0xd1d8df,/*3,13*/0x758ea7,/*4,13*/0x2a5789,/*5,13*/0x347ec3,/*6,13*/0x3c94de,/*7,13*/0x47a8ea,/*8,13*/0x54b4f0,/*9,13*/0x64b3df,/*10,13*/0x5187a9,/*11,13*/0x2d4862,/*12,13*/0x374a60,/*13,13*/0x89a3af,/*14,13*/0xff00ff,/*15,13*/0xff00ff},
        {/*0,14*/0x66afe0,/*1,14*/0x729abc,/*2,14*/0xff00ff,/*3,14*/0xff00ff,/*4,14*/0x5e96c4,/*5,14*/0x4975a2,/*6,14*/0x304f74,/*7,14*/0x294667,/*8,14*/0x2b4665,/*9,14*/0x2f435c,/*10,14*/0x374b62,/*11,14*/0x5a7688,/*12,14*/0xa8bac3,/*13,14*/0xff00ff,/*14,14*/0xff00ff,/*15,14*/0xff00ff},
        {/*0,15*/0xb8ccdd,/*1,15*/0x58a1d7,/*2,15*/0x4b9ad4,/*3,15*/0x64a3d5,/*4,15*/0x9bbad4,/*5,15*/0xdae1e8,/*6,15*/0xbbc6d2,/*7,15*/0x9babbd,/*8,15*/0x9bacbc,/*9,15*/0xa3b4c2,/*10,15*/0xc5cfd6,/*11,15*/0xdbdfe3,/*12,15*/0xff00ff,/*13,15*/0xff00ff,/*14,15*/0xff00ff,/*15,15*/0xff00ff}
};

static DWORD btndbar [10][10]={
        {/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xff00ff,/*6,0*/0xff00ff,/*7,0*/0xff00ff,/*8,0*/0xff00ff,/*9,0*/0xff00ff},
        {/*0,1*/0xff00ff,/*1,1*/0x668eaf,/*2,1*/0x668eaf,/*3,1*/0x668eaf,/*4,1*/0x668eaf,/*5,1*/0x668eaf,/*6,1*/0x668eaf,/*7,1*/0xff00ff,/*8,1*/0xff00ff,/*9,1*/0xff00ff},
        {/*0,2*/0xff00ff,/*1,2*/0x668eaf,/*2,2*/0xfefeff,/*3,2*/0xfefeff,/*4,2*/0xfefeff,/*5,2*/0xfefeff,/*6,2*/0xfefeff,/*7,2*/0xff00ff,/*8,2*/0xff00ff,/*9,2*/0xff00ff},
        {/*0,3*/0xff00ff,/*1,3*/0x668eaf,/*2,3*/0xfefeff,/*3,3*/0xff00ff,/*4,3*/0xff00ff,/*5,3*/0xff00ff,/*6,3*/0xff00ff,/*7,3*/0xff00ff,/*8,3*/0xff00ff,/*9,3*/0xff00ff},
        {/*0,4*/0xff00ff,/*1,4*/0x668eaf,/*2,4*/0xfefeff,/*3,4*/0xff00ff,/*4,4*/0x668eaf,/*5,4*/0xfefeff,/*6,4*/0xff00ff,/*7,4*/0x668eaf,/*8,4*/0xfefeff,/*9,4*/0xff00ff},
        {/*0,5*/0xff00ff,/*1,5*/0x668eaf,/*2,5*/0xfefeff,/*3,5*/0xff00ff,/*4,5*/0xff00ff,/*5,5*/0x668eaf,/*6,5*/0x668eaf,/*7,5*/0x668eaf,/*8,5*/0xfefeff,/*9,5*/0xff00ff},
        {/*0,6*/0xff00ff,/*1,6*/0x668eaf,/*2,6*/0xfefeff,/*3,6*/0xff00ff,/*4,6*/0xff00ff,/*5,6*/0x668eaf,/*6,6*/0x668eaf,/*7,6*/0x668eaf,/*8,6*/0xfefeff,/*9,6*/0xff00ff},
        {/*0,7*/0xff00ff,/*1,7*/0xff00ff,/*2,7*/0xff00ff,/*3,7*/0xff00ff,/*4,7*/0x668eaf,/*5,7*/0x668eaf,/*6,7*/0x668eaf,/*7,7*/0x668eaf,/*8,7*/0xfefeff,/*9,7*/0xff00ff},
        {/*0,8*/0xff00ff,/*1,8*/0xff00ff,/*2,8*/0xff00ff,/*3,8*/0xff00ff,/*4,8*/0xff00ff,/*5,8*/0xfefeff,/*6,8*/0xfefeff,/*7,8*/0xfefeff,/*8,8*/0xfefeff,/*9,8*/0xff00ff},
        {/*0,9*/0xff00ff,/*1,9*/0xff00ff,/*2,9*/0xff00ff,/*3,9*/0xff00ff,/*4,9*/0xff00ff,/*5,9*/0xff00ff,/*6,9*/0xff00ff,/*7,9*/0xff00ff,/*8,9*/0xff00ff,/*9,9*/0xff00ff}
};

static DWORD ardodone [6][7]={
        {/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xff00ff,/*6,0*/0xff00ff},
        {/*0,1*/0xff00ff,/*1,1*/0x567db1,/*2,1*/0x567db1,/*3,1*/0x567db1,/*4,1*/0x567db1,/*5,1*/0x567db1,/*6,1*/0xff00ff},
        {/*0,2*/0xff00ff,/*1,2*/0xffffff,/*2,2*/0x567db1,/*3,2*/0x567db1,/*4,2*/0x567db1,/*5,2*/0xffffff,/*6,2*/0xff00ff},
        {/*0,3*/0xff00ff,/*1,3*/0xff00ff,/*2,3*/0xffffff,/*3,3*/0x567db1,/*4,3*/0xffffff,/*5,3*/0xff00ff,/*6,3*/0xff00ff},
        {/*0,4*/0xff00ff,/*1,4*/0xff00ff,/*2,4*/0xff00ff,/*3,4*/0xffffff,/*4,4*/0xff00ff,/*5,4*/0xff00ff,/*6,4*/0xff00ff},
        {/*0,5*/0xff00ff,/*1,5*/0xff00ff,/*2,5*/0xff00ff,/*3,5*/0xff00ff,/*4,5*/0xff00ff,/*5,5*/0xff00ff,/*6,5*/0xff00ff}
};

static DWORD WhiteArrows [16][16]={
        {/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xff00ff,/*6,0*/0xff00ff,/*7,0*/0xff00ff,/*8,0*/0xff00ff,/*9,0*/0xff00ff,/*10,0*/0xff00ff,/*11,0*/0xff00ff,/*12,0*/0xff00ff,/*13,0*/0xff00ff,/*14,0*/0xff00ff,/*15,0*/0xff00ff},
        {/*0,1*/0xff00ff,/*1,1*/0xff00ff,/*2,1*/0xff00ff,/*3,1*/0xff00ff,/*4,1*/0xff00ff,/*5,1*/0xff00ff,/*6,1*/0xff00ff,/*7,1*/0xff00ff,/*8,1*/0xff00ff,/*9,1*/0xff00ff,/*10,1*/0xff00ff,/*11,1*/0xff00ff,/*12,1*/0xff00ff,/*13,1*/0xff00ff,/*14,1*/0xff00ff,/*15,1*/0xff00ff},
        {/*0,2*/0xff00ff,/*1,2*/0xff00ff,/*2,2*/0xff00ff,/*3,2*/0xff00ff,/*4,2*/0xff00ff,/*5,2*/0xff00ff,/*6,2*/0xff00ff,/*7,2*/0xff00ff,/*8,2*/0xff00ff,/*9,2*/0xff00ff,/*10,2*/0xff00ff,/*11,2*/0xff00ff,/*12,2*/0xff00ff,/*13,2*/0xff00ff,/*14,2*/0xff00ff,/*15,2*/0xff00ff},
        {/*0,3*/0xff00ff,/*1,3*/0xff00ff,/*2,3*/0xff00ff,/*3,3*/0xff00ff,/*4,3*/0xff00ff,/*5,3*/0xff00ff,/*6,3*/0xff00ff,/*7,3*/0xff00ff,/*8,3*/0xff00ff,/*9,3*/0xff00ff,/*10,3*/0xff00ff,/*11,3*/0xff00ff,/*12,3*/0xff00ff,/*13,3*/0xff00ff,/*14,3*/0xff00ff,/*15,3*/0xff00ff},
        {/*0,4*/0xff00ff,/*1,4*/0xff00ff,/*2,4*/0xff00ff,/*3,4*/0xff00ff,/*4,4*/0xffffff,/*5,4*/0xffffff,/*6,4*/0xff00ff,/*7,4*/0xff00ff,/*8,4*/0xff00ff,/*9,4*/0xffffff,/*10,4*/0xffffff,/*11,4*/0xff00ff,/*12,4*/0xff00ff,/*13,4*/0xff00ff,/*14,4*/0xff00ff,/*15,4*/0xff00ff},
        {/*0,5*/0xff00ff,/*1,5*/0xff00ff,/*2,5*/0xff00ff,/*3,5*/0xff00ff,/*4,5*/0xff00ff,/*5,5*/0xffffff,/*6,5*/0xffffff,/*7,5*/0xff00ff,/*8,5*/0xffffff,/*9,5*/0xffffff,/*10,5*/0xff00ff,/*11,5*/0xff00ff,/*12,5*/0xff00ff,/*13,5*/0xff00ff,/*14,5*/0xff00ff,/*15,5*/0xff00ff},
        {/*0,6*/0xff00ff,/*1,6*/0xff00ff,/*2,6*/0xff00ff,/*3,6*/0xff00ff,/*4,6*/0xff00ff,/*5,6*/0xff00ff,/*6,6*/0xffffff,/*7,6*/0xffffff,/*8,6*/0xffffff,/*9,6*/0xff00ff,/*10,6*/0xff00ff,/*11,6*/0xff00ff,/*12,6*/0xff00ff,/*13,6*/0xff00ff,/*14,6*/0xff00ff,/*15,6*/0xff00ff},
        {/*0,7*/0xff00ff,/*1,7*/0xff00ff,/*2,7*/0xff00ff,/*3,7*/0xff00ff,/*4,7*/0xff00ff,/*5,7*/0xff00ff,/*6,7*/0xff00ff,/*7,7*/0xffffff,/*8,7*/0xff00ff,/*9,7*/0xff00ff,/*10,7*/0xff00ff,/*11,7*/0xff00ff,/*12,7*/0xff00ff,/*13,7*/0xff00ff,/*14,7*/0xff00ff,/*15,7*/0xff00ff},
        {/*0,8*/0xff00ff,/*1,8*/0xff00ff,/*2,8*/0xff00ff,/*3,8*/0xff00ff,/*4,8*/0xffffff,/*5,8*/0xffffff,/*6,8*/0xff00ff,/*7,8*/0xff00ff,/*8,8*/0xff00ff,/*9,8*/0xffffff,/*10,8*/0xffffff,/*11,8*/0xff00ff,/*12,8*/0xff00ff,/*13,8*/0xff00ff,/*14,8*/0xff00ff,/*15,8*/0xff00ff},
        {/*0,9*/0xff00ff,/*1,9*/0xff00ff,/*2,9*/0xff00ff,/*3,9*/0xff00ff,/*4,9*/0xff00ff,/*5,9*/0xffffff,/*6,9*/0xffffff,/*7,9*/0xff00ff,/*8,9*/0xffffff,/*9,9*/0xffffff,/*10,9*/0xff00ff,/*11,9*/0xff00ff,/*12,9*/0xff00ff,/*13,9*/0xff00ff,/*14,9*/0xff00ff,/*15,9*/0xff00ff},
        {/*0,10*/0xff00ff,/*1,10*/0xff00ff,/*2,10*/0xff00ff,/*3,10*/0xff00ff,/*4,10*/0xff00ff,/*5,10*/0xff00ff,/*6,10*/0xffffff,/*7,10*/0xffffff,/*8,10*/0xffffff,/*9,10*/0xff00ff,/*10,10*/0xff00ff,/*11,10*/0xff00ff,/*12,10*/0xff00ff,/*13,10*/0xff00ff,/*14,10*/0xff00ff,/*15,10*/0xff00ff},
        {/*0,11*/0xff00ff,/*1,11*/0xff00ff,/*2,11*/0xff00ff,/*3,11*/0xff00ff,/*4,11*/0xff00ff,/*5,11*/0xff00ff,/*6,11*/0xff00ff,/*7,11*/0xffffff,/*8,11*/0xff00ff,/*9,11*/0xff00ff,/*10,11*/0xff00ff,/*11,11*/0xff00ff,/*12,11*/0xff00ff,/*13,11*/0xff00ff,/*14,11*/0xff00ff,/*15,11*/0xff00ff},
        {/*0,12*/0xff00ff,/*1,12*/0xff00ff,/*2,12*/0xff00ff,/*3,12*/0xff00ff,/*4,12*/0xff00ff,/*5,12*/0xff00ff,/*6,12*/0xff00ff,/*7,12*/0xff00ff,/*8,12*/0xff00ff,/*9,12*/0xff00ff,/*10,12*/0xff00ff,/*11,12*/0xff00ff,/*12,12*/0xff00ff,/*13,12*/0xff00ff,/*14,12*/0xff00ff,/*15,12*/0xff00ff},
        {/*0,13*/0xff00ff,/*1,13*/0xff00ff,/*2,13*/0xff00ff,/*3,13*/0xff00ff,/*4,13*/0xff00ff,/*5,13*/0xff00ff,/*6,13*/0xff00ff,/*7,13*/0xff00ff,/*8,13*/0xff00ff,/*9,13*/0xff00ff,/*10,13*/0xff00ff,/*11,13*/0xff00ff,/*12,13*/0xff00ff,/*13,13*/0xff00ff,/*14,13*/0xff00ff,/*15,13*/0xff00ff},
        {/*0,14*/0xff00ff,/*1,14*/0xff00ff,/*2,14*/0xff00ff,/*3,14*/0xff00ff,/*4,14*/0xff00ff,/*5,14*/0xff00ff,/*6,14*/0xff00ff,/*7,14*/0xff00ff,/*8,14*/0xff00ff,/*9,14*/0xff00ff,/*10,14*/0xff00ff,/*11,14*/0xff00ff,/*12,14*/0xff00ff,/*13,14*/0xff00ff,/*14,14*/0xff00ff,/*15,14*/0xff00ff},
        {/*0,15*/0xff00ff,/*1,15*/0xff00ff,/*2,15*/0xff00ff,/*3,15*/0xff00ff,/*4,15*/0xff00ff,/*5,15*/0xff00ff,/*6,15*/0xff00ff,/*7,15*/0xff00ff,/*8,15*/0xff00ff,/*9,15*/0xff00ff,/*10,15*/0xff00ff,/*11,15*/0xff00ff,/*12,15*/0xff00ff,/*13,15*/0xff00ff,/*14,15*/0xff00ff,/*15,15*/0xff00ff}
};

static DWORD BlackArrows [16][16]={
        {/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xff00ff,/*6,0*/0xff00ff,/*7,0*/0xff00ff,/*8,0*/0xff00ff,/*9,0*/0xff00ff,/*10,0*/0xff00ff,/*11,0*/0xff00ff,/*12,0*/0xff00ff,/*13,0*/0xff00ff,/*14,0*/0xff00ff,/*15,0*/0xff00ff},
        {/*0,1*/0xff00ff,/*1,1*/0xff00ff,/*2,1*/0xff00ff,/*3,1*/0xff00ff,/*4,1*/0xff00ff,/*5,1*/0xff00ff,/*6,1*/0xff00ff,/*7,1*/0xff00ff,/*8,1*/0xff00ff,/*9,1*/0xff00ff,/*10,1*/0xff00ff,/*11,1*/0xff00ff,/*12,1*/0xff00ff,/*13,1*/0xff00ff,/*14,1*/0xff00ff,/*15,1*/0xff00ff},
        {/*0,2*/0xff00ff,/*1,2*/0xff00ff,/*2,2*/0xff00ff,/*3,2*/0xff00ff,/*4,2*/0xff00ff,/*5,2*/0xff00ff,/*6,2*/0xff00ff,/*7,2*/0xff00ff,/*8,2*/0xff00ff,/*9,2*/0xff00ff,/*10,2*/0xff00ff,/*11,2*/0xff00ff,/*12,2*/0xff00ff,/*13,2*/0xff00ff,/*14,2*/0xff00ff,/*15,2*/0xff00ff},
        {/*0,3*/0xff00ff,/*1,3*/0xff00ff,/*2,3*/0xff00ff,/*3,3*/0xff00ff,/*4,3*/0xff00ff,/*5,3*/0xff00ff,/*6,3*/0xff00ff,/*7,3*/0xff00ff,/*8,3*/0xff00ff,/*9,3*/0xff00ff,/*10,3*/0xff00ff,/*11,3*/0xff00ff,/*12,3*/0xff00ff,/*13,3*/0xff00ff,/*14,3*/0xff00ff,/*15,3*/0xff00ff},
        {/*0,4*/0xff00ff,/*1,4*/0xff00ff,/*2,4*/0xff00ff,/*3,4*/0xff00ff,/*4,4*/0x0,/*5,4*/0x0,/*6,4*/0xff00ff,/*7,4*/0xff00ff,/*8,4*/0xff00ff,/*9,4*/0x0,/*10,4*/0x0,/*11,4*/0xff00ff,/*12,4*/0xff00ff,/*13,4*/0xff00ff,/*14,4*/0xff00ff,/*15,4*/0xff00ff},
        {/*0,5*/0xff00ff,/*1,5*/0xff00ff,/*2,5*/0xff00ff,/*3,5*/0xff00ff,/*4,5*/0xff00ff,/*5,5*/0x0,/*6,5*/0x0,/*7,5*/0xff00ff,/*8,5*/0x0,/*9,5*/0x0,/*10,5*/0xff00ff,/*11,5*/0xff00ff,/*12,5*/0xff00ff,/*13,5*/0xff00ff,/*14,5*/0xff00ff,/*15,5*/0xff00ff},
        {/*0,6*/0xff00ff,/*1,6*/0xff00ff,/*2,6*/0xff00ff,/*3,6*/0xff00ff,/*4,6*/0xff00ff,/*5,6*/0xff00ff,/*6,6*/0x0,/*7,6*/0x0,/*8,6*/0x0,/*9,6*/0xff00ff,/*10,6*/0xff00ff,/*11,6*/0xff00ff,/*12,6*/0xff00ff,/*13,6*/0xff00ff,/*14,6*/0xff00ff,/*15,6*/0xff00ff},
        {/*0,7*/0xff00ff,/*1,7*/0xff00ff,/*2,7*/0xff00ff,/*3,7*/0xff00ff,/*4,7*/0xff00ff,/*5,7*/0xff00ff,/*6,7*/0xff00ff,/*7,7*/0x0,/*8,7*/0xff00ff,/*9,7*/0xff00ff,/*10,7*/0xff00ff,/*11,7*/0xff00ff,/*12,7*/0xff00ff,/*13,7*/0xff00ff,/*14,7*/0xff00ff,/*15,7*/0xff00ff},
        {/*0,8*/0xff00ff,/*1,8*/0xff00ff,/*2,8*/0xff00ff,/*3,8*/0xff00ff,/*4,8*/0x0,/*5,8*/0x0,/*6,8*/0xff00ff,/*7,8*/0xff00ff,/*8,8*/0xff00ff,/*9,8*/0x0,/*10,8*/0x0,/*11,8*/0xff00ff,/*12,8*/0xff00ff,/*13,8*/0xff00ff,/*14,8*/0xff00ff,/*15,8*/0xff00ff},
        {/*0,9*/0xff00ff,/*1,9*/0xff00ff,/*2,9*/0xff00ff,/*3,9*/0xff00ff,/*4,9*/0xff00ff,/*5,9*/0x0,/*6,9*/0x0,/*7,9*/0xff00ff,/*8,9*/0x0,/*9,9*/0x0,/*10,9*/0xff00ff,/*11,9*/0xff00ff,/*12,9*/0xff00ff,/*13,9*/0xff00ff,/*14,9*/0xff00ff,/*15,9*/0xff00ff},
        {/*0,10*/0xff00ff,/*1,10*/0xff00ff,/*2,10*/0xff00ff,/*3,10*/0xff00ff,/*4,10*/0xff00ff,/*5,10*/0xff00ff,/*6,10*/0x0,/*7,10*/0x0,/*8,10*/0x0,/*9,10*/0xff00ff,/*10,10*/0xff00ff,/*11,10*/0xff00ff,/*12,10*/0xff00ff,/*13,10*/0xff00ff,/*14,10*/0xff00ff,/*15,10*/0xff00ff},
        {/*0,11*/0xff00ff,/*1,11*/0xff00ff,/*2,11*/0xff00ff,/*3,11*/0xff00ff,/*4,11*/0xff00ff,/*5,11*/0xff00ff,/*6,11*/0xff00ff,/*7,11*/0x0,/*8,11*/0xff00ff,/*9,11*/0xff00ff,/*10,11*/0xff00ff,/*11,11*/0xff00ff,/*12,11*/0xff00ff,/*13,11*/0xff00ff,/*14,11*/0xff00ff,/*15,11*/0xff00ff},
        {/*0,12*/0xff00ff,/*1,12*/0xff00ff,/*2,12*/0xff00ff,/*3,12*/0xff00ff,/*4,12*/0xff00ff,/*5,12*/0xff00ff,/*6,12*/0xff00ff,/*7,12*/0xff00ff,/*8,12*/0xff00ff,/*9,12*/0xff00ff,/*10,12*/0xff00ff,/*11,12*/0xff00ff,/*12,12*/0xff00ff,/*13,12*/0xff00ff,/*14,12*/0xff00ff,/*15,12*/0xff00ff},
        {/*0,13*/0xff00ff,/*1,13*/0xff00ff,/*2,13*/0xff00ff,/*3,13*/0xff00ff,/*4,13*/0xff00ff,/*5,13*/0xff00ff,/*6,13*/0xff00ff,/*7,13*/0xff00ff,/*8,13*/0xff00ff,/*9,13*/0xff00ff,/*10,13*/0xff00ff,/*11,13*/0xff00ff,/*12,13*/0xff00ff,/*13,13*/0xff00ff,/*14,13*/0xff00ff,/*15,13*/0xff00ff},
        {/*0,14*/0xff00ff,/*1,14*/0xff00ff,/*2,14*/0xff00ff,/*3,14*/0xff00ff,/*4,14*/0xff00ff,/*5,14*/0xff00ff,/*6,14*/0xff00ff,/*7,14*/0xff00ff,/*8,14*/0xff00ff,/*9,14*/0xff00ff,/*10,14*/0xff00ff,/*11,14*/0xff00ff,/*12,14*/0xff00ff,/*13,14*/0xff00ff,/*14,14*/0xff00ff,/*15,14*/0xff00ff},
        {/*0,15*/0xff00ff,/*1,15*/0xff00ff,/*2,15*/0xff00ff,/*3,15*/0xff00ff,/*4,15*/0xff00ff,/*5,15*/0xff00ff,/*6,15*/0xff00ff,/*7,15*/0xff00ff,/*8,15*/0xff00ff,/*9,15*/0xff00ff,/*10,15*/0xff00ff,/*11,15*/0xff00ff,/*12,15*/0xff00ff,/*13,15*/0xff00ff,/*14,15*/0xff00ff,/*15,15*/0xff00ff}
};

static DWORD ArrowUpWhite [10][9]={
	{/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xff00ff,/*6,0*/0xff00ff,/*7,0*/0xff00ff,/*8,0*/0xff00ff},
	{/*0,1*/0xff00ff,/*1,1*/0xff00ff,/*2,1*/0xff00ff,/*3,1*/0xff00ff,/*4,1*/0xffffff,/*5,1*/0xff00ff,/*6,1*/0xff00ff,/*7,1*/0xff00ff,/*8,1*/0xff00ff},
	{/*0,2*/0xff00ff,/*1,2*/0xff00ff,/*2,2*/0xff00ff,/*3,2*/0xffffff,/*4,2*/0xffffff,/*5,2*/0xffffff,/*6,2*/0xff00ff,/*7,2*/0xff00ff,/*8,2*/0xff00ff},
	{/*0,3*/0xff00ff,/*1,3*/0xff00ff,/*2,3*/0xffffff,/*3,3*/0xffffff,/*4,3*/0xff00ff,/*5,3*/0xffffff,/*6,3*/0xffffff,/*7,3*/0xff00ff,/*8,3*/0xff00ff},
	{/*0,4*/0xff00ff,/*1,4*/0xffffff,/*2,4*/0xffffff,/*3,4*/0xff00ff,/*4,4*/0xff00ff,/*5,4*/0xff00ff,/*6,4*/0xffffff,/*7,4*/0xffffff,/*8,4*/0xff00ff},
	{/*0,5*/0xff00ff,/*1,5*/0xff00ff,/*2,5*/0xff00ff,/*3,5*/0xff00ff,/*4,5*/0xffffff,/*5,5*/0xff00ff,/*6,5*/0xff00ff,/*7,5*/0xff00ff,/*8,5*/0xff00ff},
	{/*0,6*/0xff00ff,/*1,6*/0xff00ff,/*2,6*/0xff00ff,/*3,6*/0xffffff,/*4,6*/0xffffff,/*5,6*/0xffffff,/*6,6*/0xff00ff,/*7,6*/0xff00ff,/*8,6*/0xff00ff},
	{/*0,7*/0xff00ff,/*1,7*/0xff00ff,/*2,7*/0xffffff,/*3,7*/0xffffff,/*4,7*/0xff00ff,/*5,7*/0xffffff,/*6,7*/0xffffff,/*7,7*/0xff00ff,/*8,7*/0xff00ff},
	{/*0,8*/0xff00ff,/*1,8*/0xffffff,/*2,8*/0xffffff,/*3,8*/0xff00ff,/*4,8*/0xff00ff,/*5,8*/0xff00ff,/*6,8*/0xffffff,/*7,8*/0xffffff,/*8,8*/0xff00ff},
	{/*0,9*/0xff00ff,/*1,9*/0xff00ff,/*2,9*/0xff00ff,/*3,9*/0xff00ff,/*4,9*/0xff00ff,/*5,9*/0xff00ff,/*6,9*/0xff00ff,/*7,9*/0xff00ff,/*8,9*/0xff00ff}
};

static DWORD ArrowUpBlack [10][9]={
	{/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xff00ff,/*6,0*/0xff00ff,/*7,0*/0xff00ff,/*8,0*/0xff00ff},
	{/*0,1*/0xff00ff,/*1,1*/0xff00ff,/*2,1*/0xff00ff,/*3,1*/0xff00ff,/*4,1*/0x0,/*5,1*/0xff00ff,/*6,1*/0xff00ff,/*7,1*/0xff00ff,/*8,1*/0xff00ff},
	{/*0,2*/0xff00ff,/*1,2*/0xff00ff,/*2,2*/0xff00ff,/*3,2*/0x0,/*4,2*/0x0,/*5,2*/0x0,/*6,2*/0xff00ff,/*7,2*/0xff00ff,/*8,2*/0xff00ff},
	{/*0,3*/0xff00ff,/*1,3*/0xff00ff,/*2,3*/0x0,/*3,3*/0x0,/*4,3*/0xff00ff,/*5,3*/0x0,/*6,3*/0x0,/*7,3*/0xff00ff,/*8,3*/0xff00ff},
	{/*0,4*/0xff00ff,/*1,4*/0x0,/*2,4*/0x0,/*3,4*/0xff00ff,/*4,4*/0xff00ff,/*5,4*/0xff00ff,/*6,4*/0x0,/*7,4*/0x0,/*8,4*/0xff00ff},
	{/*0,5*/0xff00ff,/*1,5*/0xff00ff,/*2,5*/0xff00ff,/*3,5*/0xff00ff,/*4,5*/0x0,/*5,5*/0xff00ff,/*6,5*/0xff00ff,/*7,5*/0xff00ff,/*8,5*/0xff00ff},
	{/*0,6*/0xff00ff,/*1,6*/0xff00ff,/*2,6*/0xff00ff,/*3,6*/0x0,/*4,6*/0x0,/*5,6*/0x0,/*6,6*/0xff00ff,/*7,6*/0xff00ff,/*8,6*/0xff00ff},
	{/*0,7*/0xff00ff,/*1,7*/0xff00ff,/*2,7*/0x0,/*3,7*/0x0,/*4,7*/0xff00ff,/*5,7*/0x0,/*6,7*/0x0,/*7,7*/0xff00ff,/*8,7*/0xff00ff},
	{/*0,8*/0xff00ff,/*1,8*/0x0,/*2,8*/0x0,/*3,8*/0xff00ff,/*4,8*/0xff00ff,/*5,8*/0xff00ff,/*6,8*/0x0,/*7,8*/0x0,/*8,8*/0xff00ff},
	{/*0,9*/0xff00ff,/*1,9*/0xff00ff,/*2,9*/0xff00ff,/*3,9*/0xff00ff,/*4,9*/0xff00ff,/*5,9*/0xff00ff,/*6,9*/0xff00ff,/*7,9*/0xff00ff,/*8,9*/0xff00ff}
};

static DWORD ArrowDownWhite [10][9]={
	{/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xff00ff,/*6,0*/0xff00ff,/*7,0*/0xff00ff,/*8,0*/0xff00ff},
	{/*0,1*/0xff00ff,/*1,1*/0xffffff,/*2,1*/0xffffff,/*3,1*/0xff00ff,/*4,1*/0xff00ff,/*5,1*/0xff00ff,/*6,1*/0xffffff,/*7,1*/0xffffff,/*8,1*/0xff00ff},
	{/*0,2*/0xff00ff,/*1,2*/0xff00ff,/*2,2*/0xffffff,/*3,2*/0xffffff,/*4,2*/0xff00ff,/*5,2*/0xffffff,/*6,2*/0xffffff,/*7,2*/0xff00ff,/*8,2*/0xff00ff},
	{/*0,3*/0xff00ff,/*1,3*/0xff00ff,/*2,3*/0xff00ff,/*3,3*/0xffffff,/*4,3*/0xffffff,/*5,3*/0xffffff,/*6,3*/0xff00ff,/*7,3*/0xff00ff,/*8,3*/0xff00ff},
	{/*0,4*/0xff00ff,/*1,4*/0xff00ff,/*2,4*/0xff00ff,/*3,4*/0xff00ff,/*4,4*/0xffffff,/*5,4*/0xff00ff,/*6,4*/0xff00ff,/*7,4*/0xff00ff,/*8,4*/0xff00ff},
	{/*0,5*/0xff00ff,/*1,5*/0xffffff,/*2,5*/0xffffff,/*3,5*/0xff00ff,/*4,5*/0xff00ff,/*5,5*/0xff00ff,/*6,5*/0xffffff,/*7,5*/0xffffff,/*8,5*/0xff00ff},
	{/*0,6*/0xff00ff,/*1,6*/0xff00ff,/*2,6*/0xffffff,/*3,6*/0xffffff,/*4,6*/0xff00ff,/*5,6*/0xffffff,/*6,6*/0xffffff,/*7,6*/0xff00ff,/*8,6*/0xff00ff},
	{/*0,7*/0xff00ff,/*1,7*/0xff00ff,/*2,7*/0xff00ff,/*3,7*/0xffffff,/*4,7*/0xffffff,/*5,7*/0xffffff,/*6,7*/0xff00ff,/*7,7*/0xff00ff,/*8,7*/0xff00ff},
	{/*0,8*/0xff00ff,/*1,8*/0xff00ff,/*2,8*/0xff00ff,/*3,8*/0xff00ff,/*4,8*/0xffffff,/*5,8*/0xff00ff,/*6,8*/0xff00ff,/*7,8*/0xff00ff,/*8,8*/0xff00ff},
	{/*0,9*/0xff00ff,/*1,9*/0xff00ff,/*2,9*/0xff00ff,/*3,9*/0xff00ff,/*4,9*/0xff00ff,/*5,9*/0xff00ff,/*6,9*/0xff00ff,/*7,9*/0xff00ff,/*8,9*/0xff00ff}
};

static DWORD ArrowDownBlack [10][9]={
	{/*0,0*/0xff00ff,/*1,0*/0xff00ff,/*2,0*/0xff00ff,/*3,0*/0xff00ff,/*4,0*/0xff00ff,/*5,0*/0xff00ff,/*6,0*/0xff00ff,/*7,0*/0xff00ff,/*8,0*/0xff00ff},
	{/*0,1*/0xff00ff,/*1,1*/0x0,/*2,1*/0x0,/*3,1*/0xff00ff,/*4,1*/0xff00ff,/*5,1*/0xff00ff,/*6,1*/0x0,/*7,1*/0x0,/*8,1*/0xff00ff},
	{/*0,2*/0xff00ff,/*1,2*/0xff00ff,/*2,2*/0x0,/*3,2*/0x0,/*4,2*/0xff00ff,/*5,2*/0x0,/*6,2*/0x0,/*7,2*/0xff00ff,/*8,2*/0xff00ff},
	{/*0,3*/0xff00ff,/*1,3*/0xff00ff,/*2,3*/0xff00ff,/*3,3*/0x0,/*4,3*/0x0,/*5,3*/0x0,/*6,3*/0xff00ff,/*7,3*/0xff00ff,/*8,3*/0xff00ff},
	{/*0,4*/0xff00ff,/*1,4*/0xff00ff,/*2,4*/0xff00ff,/*3,4*/0xff00ff,/*4,4*/0x0,/*5,4*/0xff00ff,/*6,4*/0xff00ff,/*7,4*/0xff00ff,/*8,4*/0xff00ff},
	{/*0,5*/0xff00ff,/*1,5*/0x0,/*2,5*/0x0,/*3,5*/0xff00ff,/*4,5*/0xff00ff,/*5,5*/0xff00ff,/*6,5*/0x0,/*7,5*/0x0,/*8,5*/0xff00ff},
	{/*0,6*/0xff00ff,/*1,6*/0xff00ff,/*2,6*/0x0,/*3,6*/0x0,/*4,6*/0xff00ff,/*5,6*/0x0,/*6,6*/0x0,/*7,6*/0xff00ff,/*8,6*/0xff00ff},
	{/*0,7*/0xff00ff,/*1,7*/0xff00ff,/*2,7*/0xff00ff,/*3,7*/0x0,/*4,7*/0x0,/*5,7*/0x0,/*6,7*/0xff00ff,/*7,7*/0xff00ff,/*8,7*/0xff00ff},
	{/*0,8*/0xff00ff,/*1,8*/0xff00ff,/*2,8*/0xff00ff,/*3,8*/0xff00ff,/*4,8*/0x0,/*5,8*/0xff00ff,/*6,8*/0xff00ff,/*7,8*/0xff00ff,/*8,8*/0xff00ff},
	{/*0,9*/0xff00ff,/*1,9*/0xff00ff,/*2,9*/0xff00ff,/*3,9*/0xff00ff,/*4,9*/0xff00ff,/*5,9*/0xff00ff,/*6,9*/0xff00ff,/*7,9*/0xff00ff,/*8,9*/0xff00ff}
};

HBITMAP BitmapFromCode( int iWidth, int iHeight, VOID* ImageBits )
{
        return CreateBitmap(iWidth,iHeight,1,sizeof(RGBQUAD)*8, ImageBits);
}

HB_FUNC( BMPNETDRIVE )
{
    hb_retnl( (long) BitmapFromCode( 16, 16, &netdrive ));
}
HB_FUNC( BMPHDDISK )
{
    hb_retnl( (long) BitmapFromCode( 16, 16, &hddisk ));
}
HB_FUNC( BMPCARPETA )
{
    hb_retnl( (long) BitmapFromCode( 16, 16, &carpeta ));
}
HB_FUNC( BMPCDROM )
{
    hb_retnl( (long) BitmapFromCode( 16, 16, &cdroom ));
}
HB_FUNC( BMPHTML )
{
    hb_retnl( (long) BitmapFromCode( 16, 16, &explorer ));
}

HB_FUNC( BMPBTNDBAR )
{
    hb_retnl( (long) BitmapFromCode( 10, 10, &btndbar ));
}

HB_FUNC( BMPARROWDOWNNET )
{
    hb_retnl( (long) BitmapFromCode( 7, 6, &ardodone ));
}

HB_FUNC( BMPWHITEARROWS )
{
    hb_retnl( (long) BitmapFromCode( 16, 16, &WhiteArrows ));
}

HB_FUNC( BMPBLACKARROWS )
{
    hb_retnl( (long) BitmapFromCode( 16, 16, &BlackArrows ));
}

HB_FUNC( BMPARROWUPWHITE )
{
    hb_retnl( (long) BitmapFromCode( 9,10, &ArrowUpWhite ));
}
HB_FUNC( BMPARROWUPBLACK )
{
    hb_retnl( (long) BitmapFromCode( 9, 10, &ArrowUpBlack ));
}

HB_FUNC( BMPARROWDOWNWHITE )
{
    hb_retnl( (long) BitmapFromCode( 9, 10, &ArrowDownWhite ));
}
HB_FUNC( BMPARROWDOWNBLACK )
{
    hb_retnl( (long) BitmapFromCode( 9, 10, &ArrowDownBlack ));
}

/*
HB_FUNC( FLOOR )
{
   if( HB_ISNUM( 1 ) )
      hb_retnd( floor( hb_parnd( 1 ) ) );
   else
      hb_retnd( 0 );
}
*/

void VerticalGradient(HDC hDC, LPRECT lpRect, COLORREF sColor, COLORREF eColor, BOOL bGamma, double gamma)
{
        // Gradient params
        // int width = lpRect->right - lpRect->left;  // - 1;
        int height = lpRect->bottom - lpRect->top;  // - 1;

        // Draw gradient

        double percent;
        unsigned char red, green, blue;
        COLORREF color;
        RECT rect;
        int i;
        int j;
        char sz[MAX_PATH];
        gamma = 0;

        if( height == 2 )
          height = 3;

        for (i=0; i<height-1; i++)
        {
                j = i;
                // Gradient color percent
                percent = 1 - (double)j / (double)(height-2);

                // Gradient color
                red   = (unsigned char)(GetRValue(sColor)*percent) + (unsigned char)(GetRValue(eColor)*(1-percent));
                green = (unsigned char)(GetGValue(sColor)*percent) + (unsigned char)(GetGValue(eColor)*(1-percent));
                blue  = (unsigned char)(GetBValue(sColor)*percent) + (unsigned char)(GetBValue(eColor)*(1-percent));

                if (bGamma)
                {
                        red   = (unsigned char)(pow((double)red  /255.0, gamma) * 255);
                        green = (unsigned char)(pow((double)green/255.0, gamma) * 255);
                        blue  = (unsigned char)(pow((double)blue /255.0, gamma) * 255);
                }
                color = RGB(red, green, blue);

                // Gradient
                rect.left = lpRect->left; // + 1;
                rect.top = lpRect->top + i + 1;
                rect.right = lpRect->right; // - 1;
                rect.bottom = rect.top + 1;
                FillSolidRect( hDC, &rect, color );
        }
}

HB_FUNC( VERTICALGRADIENT )
{
   RECT rct;
   rct.top    = hb_parvni( 2, 1 );
   rct.left   = hb_parvni( 2, 2 );
   rct.bottom = hb_parvni( 2, 3 );
   rct.right  = hb_parvni( 2, 4 );

   VerticalGradient((HDC) hb_parnl( 1 ), &rct, (COLORREF) hb_parnl( 3 ), (COLORREF) hb_parnl( 4 ), hb_parl( 5 ), hb_parnl( 6 ));
   hb_ret();
}

HB_FUNC( C5ROUNDRECT )
{

  hb_parl( RoundRect( (HDC) hb_parnl( 1 ), hb_parni( 3 ), hb_parni( 2 ), hb_parni( 5 ), hb_parni( 4 ), hb_parni( 6 ), hb_parni( 7 )  ));

}

HB_FUNC( C5ROUNDBOX )

{
   HBRUSH hBrush = (HBRUSH) GetStockObject( 5 );
   HBRUSH hOldBrush = (HBRUSH) SelectObject( (HDC) hb_parnl( 1 ), hBrush );
   HPEN hPen = CreatePen( PS_SOLID, 1, (COLORREF)hb_parnl( 8 ));
   HPEN hOldPen = (HPEN) SelectObject( (HDC) hb_parnl( 1 ), hPen );

   hb_retl( RoundRect( ( HDC ) hb_parnl( 1 ),
                               hb_parni( 2 ),
                               hb_parni( 3 ),
                               hb_parni( 4 ),
                               hb_parni( 5 ),
                               hb_parni( 6 ),
                               hb_parni( 7 ) ) );

   SelectObject( (HDC) hb_parnl( 1 ), hOldBrush );
   SelectObject( (HDC) hb_parnl( 1 ), hOldPen );
   DeleteObject( hPen );

}

HB_FUNC( C5LINE )
{
   HDC hDC = (HDC) hb_parnl( 1 );
   int nTop = hb_parni( 2 );
   int nLeft = hb_parni( 3 );
   int nBottom = hb_parni( 4 );
   int nRight = hb_parni( 5 );
   COLORREF color = (COLORREF) hb_parnl(6);
   HPEN hPen, hOldPen;
   hPen = CreatePen( PS_SOLID, 1, color );
   hOldPen = (HPEN) SelectObject( hDC, hPen );
   MoveToEx( hDC, nLeft, nTop, NULL );
   LineTo( hDC, nRight, nBottom );
   SelectObject( hDC, hOldPen );
   DeleteObject( hPen );
}

HB_FUNC( LINEEX )
{
   HDC hDC = (HDC) hb_parnl( 1 );
   int nTop = hb_parni( 2 );
   int nLeft = hb_parni( 3 );
   int nBottom = hb_parni( 4 );
   int nRight = hb_parni( 5 );
   COLORREF color = (COLORREF) hb_parnl(6);
   int nStyle = hb_parni( 7 );
   HPEN hPen, hOldPen;
   hPen = CreatePen( nStyle, 1, color );
   hOldPen = (HPEN) SelectObject( hDC, hPen );
   MoveToEx( hDC, nLeft, nTop, NULL );
   LineTo( hDC, nRight, nBottom );
   SelectObject( hDC, hOldPen );
   DeleteObject( hPen );
}


// TransparentBlt       - Copies a bitmap transparently onto the destination DC
// hdcDest              - Handle to destination device context
// nXDest               - x-coordinate of destination rectangle's upper-left corner
// nYDest               - y-coordinate of destination rectangle's upper-left corner
// nWidth               - Width of destination rectangle
// nHeight              - height of destination rectangle
// hBitmap              - Handle of the source bitmap
// nXSrc                - x-coordinate of source rectangle's upper-left corner
// nYSrc                - y-coordinate of source rectangle's upper-left corner
// colorTransparent     - The transparent color
// hPal                 - Logical palette to be used with bitmap. Can be NULL

void DrawBitmapTransparent( HDC dc, int nXDest, int nYDest, int nWidth, int nHeight,
                            HBITMAP hBitmap, int nXSrc, int nYSrc, COLORREF colorTransparent,
                            HPALETTE hPal )
{
        HDC memDC, maskDC, tempDC;
        HBITMAP pOldMemBmp, pOldMaskBmp, hOldTempBmp;
        HBITMAP maskBitmap;
        HBITMAP bmpImage;

        maskDC = CreateCompatibleDC( dc );

        //add these to store return of SelectObject() calls

        memDC = CreateCompatibleDC(dc);
        tempDC = CreateCompatibleDC(dc);

        bmpImage = CreateCompatibleBitmap( dc, nWidth, nHeight );
        pOldMemBmp = (HBITMAP ) SelectObject( memDC, bmpImage );

        // Select and realize the palette
        if( GetDeviceCaps(dc, RASTERCAPS) & RC_PALETTE && hPal )
        {
                SelectPalette( dc, hPal, FALSE );
                RealizePalette(dc);

                SelectPalette( memDC, hPal, FALSE );
        }

        hOldTempBmp = (HBITMAP) SelectObject( tempDC, hBitmap );

        BitBlt( memDC, 0,0,nWidth, nHeight, tempDC, nXSrc, nYSrc, SRCCOPY );

        // Create monochrome bitmap for the mask
        maskBitmap = CreateBitmap( nWidth, nHeight, 1, 1, NULL );
        pOldMaskBmp = (HBITMAP) SelectObject( maskDC, maskBitmap );
        SetBkColor( memDC, colorTransparent );

        // Create the mask from the memory DC
        BitBlt( maskDC, 0, 0, nWidth, nHeight, memDC, 0, 0, SRCCOPY );

        // Set the background in memDC to black. Using SRCPAINT with black
        // and any other color results in the other color, thus making
        // black the transparent color
        SetBkColor(memDC, RGB(0,0,0));
        SetTextColor(memDC, RGB(255,255,255));
        BitBlt(memDC, 0, 0, nWidth, nHeight, maskDC, 0, 0, SRCAND);

        // Set the foreground to black. See comment above.
        SetBkColor(dc, RGB(255,255,255));
        SetTextColor(dc, RGB(0,0,0));
        BitBlt(dc, nXDest, nYDest, nWidth, nHeight, maskDC, 0, 0, SRCAND);

        // Combine the foreground with the background
        BitBlt(dc, nXDest, nYDest, nWidth, nHeight, memDC, 0, 0, SRCPAINT);

        if (hOldTempBmp)
                SelectObject( tempDC, hOldTempBmp);

        if (pOldMaskBmp)
                SelectObject( maskDC, pOldMaskBmp );

        if (pOldMemBmp)
                SelectObject( memDC, pOldMemBmp );

        DeleteObject( bmpImage );
        DeleteObject( maskBitmap );

        DeleteDC( maskDC );
        DeleteDC( memDC );
        DeleteDC( tempDC );

}

HB_FUNC( TRANSPARENTBLT2 )
{
   hb_ret();
   DrawBitmapTransparent( (HDC) hb_parnl( 1 ), hb_parni( 3 ), hb_parni( 2 ), hb_parni( 4 ), hb_parni( 5 ),
                          (HBITMAP) hb_parnl( 6 ), hb_parni( 7 ), hb_parni( 8 ),
                        (COLORREF) hb_parnl( 9 ), NULL );

}

#ifdef __GNUC__ 
BOOL WINAPI TransparentBlt(
  HDC hdcDest,
  int xoriginDest,
  int yoriginDest,
  int wDest,
  int hDest,
  HDC hdcSrc,
  int xoriginSrc,
  int yoriginSrc,
  int wSrc,
  int hSrc,
  UINT crTransparent
);
#endif

HB_FUNC( TRANSPARENTBLT )
{

   HDC hDC = (HDC) hb_parnl( 1 );
   HDC hdc0 = CreateDC( "DISPLAY", NULL, NULL, NULL );
   HDC hdcSrc = CreateCompatibleDC( hdc0 );
   HBITMAP hOldBmp;
   HBITMAP hBmp = (HBITMAP) hb_parnl( 2 );
   COLORREF color = hb_parni( 7 );
   BITMAP bm;
   GetObject( ( HGDIOBJ ) hBmp, sizeof( BITMAP ), ( LPSTR ) &bm );

   hOldBmp = (HBITMAP)SelectObject( hdcSrc, hBmp );

   TransparentBlt( hDC,
                   hb_parvni( 4 ),
                   hb_parvni( 3 ),
                   hb_parvni( 5 ),
                   hb_parvni( 6 ),
                   hdcSrc,
                   0,
                   0,
                   bm.bmWidth,
                   bm.bmHeight,
                   color  );
   SelectObject( hdcSrc, hOldBmp );
   DeleteDC( hdcSrc );
   DeleteDC( hdc0 );


   hb_ret();
}


HB_FUNC( C5DRAWTEXT )
{

   RECT rct;


      rct.top    = hb_parvni( 3, 1 );
      rct.left   = hb_parvni( 3, 2 );
      rct.bottom = hb_parvni( 3, 3 );
      rct.right  = hb_parvni( 3, 4 );

      hb_retni( DrawText( ( HDC ) hb_parnl( 1 ),
                                  hb_parc( 2 ),
                                  -1,
                                  &rct,
                                  hb_parnl( 4 ) ) );
      hb_storvni( rct.top, 3, 1 );
      hb_storvni( rct.left, 3, 2 );
      hb_storvni( rct.bottom, 3, 3 );
      hb_storvni( rct.right, 3, 4 );
}

HB_FUNC( CGETFILEEX )   
{
   OPENFILENAME ofn;
   char * buffer = ( char * ) hb_xgrab( 65536 );
   char cFullName[64][MAX_PATH];
   char cCurDir[MAX_PATH];
   char cFileName[MAX_PATH];
   int iPosition = 0;
   int iNumSelected = 0;
   int n;
   LPSTR pFile,pFilter,pTitle,pDir;
   WORD w = 0, wLen;
   int flags =  OFN_ALLOWMULTISELECT | OFN_EXPLORER ; // | OFN_FILEMUSTEXIST

   buffer[0] = 0 ;

   // alloc for title

   pTitle = ( LPSTR ) hb_xgrab( 128 );

   if ( hb_pcount() > 1 && HB_ISCHAR( 2 ) )
   {
      wLen   = min( ( unsigned long ) 127, hb_parclen( 2 ) );
      memcpy( pTitle, ( char * ) hb_parc( 2 ), wLen );
      * ( pTitle + wLen ) = 0;

   }
   else
   {
      pTitle  = Title;
   }

   // alloc for initial dir

   pDir = ( LPSTR ) hb_xgrab( 128 );

   if ( hb_pcount() > 3 && HB_ISCHAR( 4 ) )
   {
      wLen  = min( ( unsigned long ) 127, hb_parclen( 4 ) );
      memcpy( pDir, ( char * ) hb_parc( 4 ), wLen );
      * ( pDir + wLen ) = 0;
   }
   else
   {
      * ( pDir ) = 0;
   }

   // alloc for file

   pFile = ( LPSTR ) hb_xgrab( 255 );

   if ( hb_pcount() > 7 && HB_ISCHAR( 8 ) )
   {
      wLen = min( ( unsigned long ) 254, hb_parclen( 8 ) );
      memcpy( pFile, ( char * ) hb_parc( 8 ), wLen );
   }
   else
   {
      wLen = min( ( unsigned long ) 254, hb_parclen( 1 ) );
      memcpy( pFile, ( char * ) hb_parc( 1 ), wLen );
   }
   * ( pFile + wLen ) = 0;

   // alloc for mask

   pFilter = ( LPSTR ) hb_xgrab( 400 );
   wLen    = min( ( unsigned long ) 398, hb_parclen( 1 ) );
   memcpy( pFilter, ( char * ) hb_parc( 1 ), wLen );
   * ( pFilter + wLen ) = 0;

   #ifndef __FLAT__
   //   _xunlock();
   #endif

   while( * ( pFilter + w ) )
   {
      if( * ( pFilter + w ) == '|' )
      {
         * ( pFilter + w ) = 0;
         if ( hb_pcount() < 8 )
            * (pFile) = 0;
      }
      w++;
   }

   * ( pFilter + wLen  ) = 0;
   * ( pFilter + wLen + 1 ) = 0;

   if( hb_pcount() == 8 )
     strcpy( buffer, hb_parc(8));

        memset( (void*) &ofn, 0, sizeof( OPENFILENAME ) );
        ofn.lStructSize = sizeof(ofn);
        ofn.hwndOwner = GetActiveWindow();
        ofn.lpstrFilter = pFilter;
        ofn.nFilterIndex = 1;
        ofn.lpstrFile = buffer;
        ofn.nMaxFile = sizeof(buffer);
        ofn.lpstrInitialDir = hb_parc(3);
        ofn.lpstrTitle = pTitle;
        ofn.nMaxFileTitle = 512;
        ofn.Flags = flags;

        if( GetOpenFileName( &ofn ) )
        {
                if(ofn.nFileExtension!=0)
                {
                        hb_retc( ofn.lpstrFile );
                }
                else
                {
                        wsprintf(cCurDir,"%s",&buffer[iPosition]);
                        iPosition=iPosition+strlen(cCurDir)+1;

                        do
                        {
                                iNumSelected++;
                                wsprintf(cFileName,"%s",&buffer[iPosition]);
                                iPosition=iPosition+strlen(cFileName)+1;
                                wsprintf(cFullName[iNumSelected],"%s\\%s",cCurDir,cFileName);
                        }
                        while(  (strlen(cFileName)!=0) && ( iNumSelected <= 63 ) );

                        if(iNumSelected > 1)
                        {
                                hb_reta( iNumSelected - 1 );

                                for (n = 1; n < iNumSelected; n++)
                                {
                                        hb_storvc( cFullName[n], -1, n );
                                }
                        }
                        else
                        {
                                hb_retc( &buffer[0] );
                        }
                }
        }
        else
        {
                hb_retc( "" );
        }
   hb_xfree( buffer );     
}

// del msdn
INT CALLBACK BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lp, LPARAM pData)
{
   HB_SYMBOL_UNUSED( lp );
   HB_SYMBOL_UNUSED( pData );
  
   switch(uMsg)
   {
   case BFFM_INITIALIZED:
      if (szDirName)
      {
         // WParam is TRUE since you are passing a path.
         // It would be FALSE if you were passing a pidl.
         SendMessage(hwnd, BFFM_SETSELECTION, TRUE, (LPARAM)szDirName);
      }
      break;
   }
   return 0;
}

HB_FUNC( CGETFOLDER ) // Based Upon Code Contributed By Ryszard Ry�ko & minigui
{
   HWND hwnd = GetActiveWindow();
   BROWSEINFO bi;
   char *lpBuffer = (char*) hb_xgrab( MAX_PATH+1);
   LPITEMIDLIST pidlBrowse;    // PIDL selected by user

    bi.hwndOwner = hwnd;
    bi.pidlRoot = NULL;
    bi.pszDisplayName = lpBuffer;
    bi.lpszTitle = hb_parc(1);
    bi.ulFlags = BIF_RETURNONLYFSDIRS + BIF_DONTGOBELOWDOMAIN + BIF_USENEWUI;
    bi.lpfn = BrowseCallbackProc;
    bi.lParam = 0;

   strcpy( szDirName, (hb_parc( 2 )? hb_parc( 2 ): IniDir) );

    // Browse for a folder and return its PIDL.
    pidlBrowse = SHBrowseForFolder(&bi);
    SHGetPathFromIDList(pidlBrowse,lpBuffer);
    hb_retc(lpBuffer);
    hb_xfree( lpBuffer);
}

HB_FUNC( GETDRAWITEMSTRUCT )
{
   LPDRAWITEMSTRUCT lp = ( LPDRAWITEMSTRUCT ) hb_parnl( 1 );

   hb_reta( 12 );

   hb_storvni( lp->CtlType           ,  -1,  1 );
   hb_storvni( lp->CtlID             ,  -1,  2 );
   hb_storvni( lp->itemID            ,  -1,  3 );
   hb_storvni( lp->itemAction        ,  -1,  4 );
   hb_storvni( lp->itemState         ,  -1,  5 );
   hb_storvni( ( LONG ) lp->hwndItem ,  -1,  6 );
   hb_storvni( ( LONG ) lp->hDC      ,  -1,  7 );
   hb_storvni( lp->rcItem.top        ,  -1,  8 );
   hb_storvni( lp->rcItem.left       ,  -1,  9 );
   hb_storvni( lp->rcItem.bottom     ,  -1, 10 );
   hb_storvni( lp->rcItem.right      ,  -1, 11 );
   hb_storvnd( lp->itemData          ,  -1, 12 );

}

HB_FUNC( LBXMEASUREEX )
{
   HBITMAP hBitmap;
   BITMAP bm;
   LPMEASUREITEMSTRUCT pMeasure = ( LPMEASUREITEMSTRUCT ) hb_parnl( 1 );
   HINSTANCE hLib = (HINSTANCE) hb_parnl( 3 );
   int iItem = (int)pMeasure->itemID+1;
   char szFile[MAX_PATH];
   char szExt[4];
   strcpy( szFile, hb_parvc( 2, iItem ));

   _splitpath(szFile,NULL,NULL,NULL,szExt);

   if( strcmp(szExt, ".ico" ) == 0 )
   {
        pMeasure->itemHeight = 32 + 6;
   }
   else
   {
       if( hLib )
          hBitmap = ( HBITMAP ) LoadImage( hLib, szFile, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION );
       else
          hBitmap = ( HBITMAP ) LoadImage( ( HINSTANCE ) NULL, szFile, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE );

       if( hBitmap )
       {
          GetObject( ( HGDIOBJ ) hBitmap, sizeof( BITMAP ), ( LPSTR ) &bm );
          pMeasure->itemHeight = bm.bmHeight + 6;
          DeleteObject( hBitmap );
       }
       else
       {
          pMeasure->itemHeight = 18;
       }
   }
   hb_retl( TRUE );
}


HB_FUNC( DRAWSTATESTRING )
{

hb_retl( DrawState( ( HDC )  hb_parnl( 1 ),
                    (HBRUSH) hb_parnl( 2 ),
                                      NULL,
                    (LPARAM) hb_parc( 3 ),
                    (WPARAM)           0  ,
                             hb_parni( 4 ),
                             hb_parni( 5 ),
                             hb_parni( 6 ),
                             hb_parni( 7 ),
                             hb_parnl( 8 ) ) );
}

HB_FUNC( POW )
{
   hb_retnd( pow( hb_parnd( 1 ), hb_parnd( 2 ) ) );
}

void DrawAlphaTransparentBitmap(HDC hDC, HBITMAP hBmp, RECT rc, BYTE iAlpha, COLORREF clrTrans)
{
   typedef BOOL (CALLBACK *LPALPHABLEND)(HDC,int, int, int, int,HDC, int, int, int, int, BLENDFUNCTION);
   LPALPHABLEND lpAlphaBlend;   //static LPALPHABLEND lpAlphaBlend = (LPALPHABLEND) GetProcAddress( GetModuleHandle("msimg32.dll"), "AlphaBlend");
   int cx, cy, x, y;
   BOOL bRes;
   LPDWORD pSrcBits, pDest, pSrc;
   DWORD dwKey, dwShowColor;
   BITMAPINFO bmi = { 0 };
   HBITMAP hOldBitmap, bmpDest;
   HDC dcCompat;
   BLENDFUNCTION bf;
   HINSTANCE hLib = LoadLibrary( "msimg32.dll" );
   lpAlphaBlend = ( LPALPHABLEND ) GetProcAddress( hLib, "AlphaBlend" );


   if( lpAlphaBlend == NULL )
   {
      FreeLibrary( hLib );
      return;
   }
   cx = rc.right - rc.left;
   cy = rc.bottom - rc.top;

   bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
   bRes = GetDIBits(hDC, hBmp, 0, cy, NULL, &bmi, DIB_RGB_COLORS);
   if( !bRes )
   {
      FreeLibrary( hLib );
      return;
   }
   pSrcBits = (LPDWORD) LocalAlloc(LPTR, bmi.bmiHeader.biWidth * bmi.bmiHeader.biHeight * sizeof(DWORD));
   bmi.bmiHeader.biBitCount = 32;  // Ask for ARGB codec
   bmi.bmiHeader.biCompression = BI_RGB;
   bRes = GetDIBits(hDC, hBmp, 0, cy, pSrcBits, &bmi, DIB_RGB_COLORS);
   if( !bRes || bmi.bmiHeader.biBitCount != 32 ) {
      LocalFree(pSrcBits);
      FreeLibrary( hLib );
      return;
   }
   pDest = NULL;
   bmpDest = CreateDIBSection(hDC, &bmi, DIB_RGB_COLORS, (LPVOID*) &pDest, NULL, 0);
   if( bmpDest == NULL ) {
      LocalFree(pSrcBits);
      FreeLibrary( hLib );
      return;
   }
   dwKey = RGB(GetBValue(clrTrans), GetGValue(clrTrans), GetRValue(clrTrans));
   pSrc = pSrcBits;
   dwShowColor = 0xFF000000;
   for( y = 0; y < abs(bmi.bmiHeader.biHeight); y++ ) {
      for( x = 0; x < bmi.bmiHeader.biWidth; x++ ) {
         if( *pSrc != dwKey ) *pDest++ = *pSrc | dwShowColor;
         else *pDest++ = 0x00000000;
         pSrc++;
      }
   }

   #ifndef AC_SRC_ALPHA
      #define AC_SRC_ALPHA  0x01
   #endif   

   dcCompat = CreateCompatibleDC(hDC);
   hOldBitmap = (HBITMAP) SelectObject(dcCompat, bmpDest);
   SetStretchBltMode(hDC, COLORONCOLOR);
   bf.BlendOp = AC_SRC_OVER;
   bf.BlendFlags = 0;
   bf.AlphaFormat = AC_SRC_ALPHA;
   bf.SourceConstantAlpha = iAlpha;
   bRes = lpAlphaBlend(hDC, rc.left, rc.top, cx, cy, dcCompat, 0, 0, cx, cy, bf);
   SelectObject(dcCompat, hOldBitmap);
   LocalFree(pSrcBits);
   FreeLibrary( hLib );
   
   HB_SYMBOL_UNUSED( bRes );
}


HB_FUNC( DRAWALPHATRANSPARENTBITMAP )
{
    RECT rc;
    rc.top    = hb_parvni( 3, 1 );
    rc.left   = hb_parvni( 3, 2 );
    rc.bottom = hb_parvni( 3, 3 );
    rc.right  = hb_parvni( 3, 4 );

    DrawAlphaTransparentBitmap((HDC) hb_parnl( 1 ),
                               (HBITMAP) hb_parnl( 2 ),
                               rc,
                               (BYTE) hb_parni( 4 ),
                               (COLORREF) hb_parnl( 5 ));
    hb_ret();
}

void FrameDotEx2( HDC hDC, RECT * pRect, HBRUSH hbr )

{

   HBRUSH hbrPrevious;
   hbrPrevious = ( HBRUSH ) SelectObject( hDC, hbr );
   UnrealizeObject( hbr );
   FrameRect( hDC, pRect, hbr );
   SelectObject( hDC, hbrPrevious );
}

//----------------------------------------------------------------------------//


HB_FUNC( CREATEBRUSHDOTTED )
{
   COLORREF nColor = hb_parnl( 1 );
   COLORREF nColor2 = hb_parnl( 2 );
   HBITMAP hbmp, hOldBitmap;
   HBRUSH hbr, hbrPrevious;
   HDC hDC = CreateDC( "Display",NULL,NULL,NULL);
   HDC hDCMem;
   RECT rcs;
   COLORREF nColor0;

   rcs.top    = 0;
   rcs.left   = 0;
   rcs.bottom = 8;
   rcs.right  = 8;

   hDCMem = CreateCompatibleDC( hDC );
   hbmp = CreateCompatibleBitmap( hDC, 2, 2 );
   hOldBitmap = (HBITMAP) SelectObject( hDCMem, hbmp );

   nColor0 = SetBkColor( hDCMem, nColor2 );
   ExtTextOut( hDCMem, 0, 0, ETO_OPAQUE, &rcs, NULL, 0, NULL);

   SetBkColor( hDCMem, nColor0 );
   SetPixel( hDCMem, 0, 0, nColor );
   SetPixel( hDCMem, 1, 1, nColor );

   SelectObject( hDCMem, hOldBitmap );

   DeleteDC( hDCMem );
   DeleteDC( hDC );
   hbr  = CreatePatternBrush( hbmp );
   DeleteObject( hbmp );
   hb_retnl( (long) hbr );

}


HB_FUNC( FRAMEDOTEX ) // hDC, nTop, nLeft, nBottom, nRight

{

   RECT rct;
   rct.top    = hb_parvni( 2, 1 );
   rct.left   = hb_parvni( 2, 2 );
   rct.bottom = hb_parvni( 2, 3 );
   rct.right  = hb_parvni( 2, 4 );

   FrameDotEx2( ( HDC ) hb_parnl( 1 ), &rct, (HBRUSH) hb_parnl( 3 ) );

}






HB_FUNC( C5CURSORSEP )
{
   int cxWidth = 32;
   int cyHeight = 32;
   int xHotSpot = 8;
   int yHotSpot = 8;

   if( !hC5CursorSep )
   {
      hC5CursorSep = CreateCursor( GetInstance(),
                                     xHotSpot,
                                     yHotSpot,
                                     cxWidth,
                                     cyHeight,
                                     ANDPlaneSep,
                                     XORPlaneSep);
      RegisterResource( hC5CursorSep, "CUR" );
   }
   SetCursor( hC5CursorSep );
}

HB_FUNC( C5CURSORCATCH )
{
   int cxWidth = 32;
   int cyHeight = 32;
   int xHotSpot = 15;
   int yHotSpot = 17;

   if( !hC5Catch )
   {
      hC5Catch = CreateCursor( GetInstance(),
                                     xHotSpot,
                                     yHotSpot,
                                     cxWidth,
                                     cyHeight,
                                     ANDPlaneCatch,
                                     XORPlaneCatch);
      RegisterResource( hC5Catch, "CUR" );
   }
   SetCursor( hC5Catch );
}

HB_FUNC( C5CURSORHAND )
{
   int cxWidth = 32;
   int cyHeight = 32;
   int xHotSpot = 8;
   int yHotSpot = 8;

   if( !hC5Hand )
   {
      hC5Hand = CreateCursor( GetInstance(),
                                     xHotSpot,
                                     yHotSpot,
                                     cxWidth,
                                     cyHeight,
                                     ANDPlaneHand,
                                     XORPlaneHand);

      RegisterResource( hC5Hand, "CUR" );
   }
   SetCursor( hC5Hand );
}

//HB_FUNC( HWNDCOMBO )
//{
//   COMBOBOXINFO cbi      ;
//   ZeroMemory( &cbi, sizeof( COMBOBOXINFO ) );
//   cbi.cbSize = sizeof(COMBOBOXINFO);
//
//   GetComboBoxInfo( (HWND) hb_parnl( 1 ), &cbi );
//
//   hb_retnl( (LONG)cbi.hwndCombo ) ;
//}

//HB_FUNC( HWNDCOMBOEDIT )
//{
//   COMBOBOXINFO cbi      ;
//   ZeroMemory( &cbi, sizeof( COMBOBOXINFO ) );
//   cbi.cbSize = sizeof(COMBOBOXINFO);
//
//   GetComboBoxInfo( (HWND) hb_parnl( 1 ), &cbi );
//
//   hb_retnl( (LONG)cbi.hwndItem ) ;
//   //hb_retnl( (LONG)cbi.hwndList ) ;
//}

//HB_FUNC( HWNDCOMBOLIST )
//{
//   COMBOBOXINFO cbi      ;
//   ZeroMemory( &cbi, sizeof( COMBOBOXINFO ) );
//   cbi.cbSize = sizeof(COMBOBOXINFO);
//
//   GetComboBoxInfo( (HWND) hb_parnl( 1 ), &cbi );
//
//   hb_retnl( (LONG)cbi.hwndList ) ;
//}

HB_FUNC( SETEDITCLIENTRIGHT )
{
   RECT rect;
   GetClientRect( (HWND)hb_parnl(1), &rect );
   rect.right -= hb_parni( 2 ) ;
   SendMessage( (HWND)hb_parnl(1), EM_SETRECTNP, 0, ( LONG ) &rect ); //
}

void ShadeRect( HDC hDC, RECT* rect )
{
	// Bit pattern for a monochrome brush with every
	// other pixel turned off
	WORD Bits[8] = { 0x0055, 0x00aa, 0x0055, 0x00aa,
			 0x0055, 0x00aa, 0x0055, 0x00aa };

	HBITMAP bmBrush;
	HBRUSH brush, hOldBrush;
	COLORREF clrBk, clrText;


	// Need a monochrome pattern bitmap
	bmBrush = CreateBitmap( 8, 8, 1, 1, &Bits );

	// Create the pattern brush
	brush = CreatePatternBrush( bmBrush );

	hOldBrush = (HBRUSH) SelectObject( hDC, brush );

	// Turn every other pixel to black
	clrBk = SetBkColor( hDC, RGB(255,255,255) );
	clrText = SetTextColor( hDC, RGB(0,0,0) );
	// 0x00A000C9 is the ROP code to AND the brush with the destination
	PatBlt(hDC, rect->left, rect->top, rect->right - rect->left, rect->bottom-rect->top,
		(DWORD)0x00A000C9);			//DPa - raster code

	SetBkColor( hDC, RGB(0,0,0) );
	SetTextColor( hDC, GetSysColor(COLOR_HIGHLIGHT) );
	// 0x00FA0089 is the ROP code to OR the brush with the destination
	PatBlt(hDC, rect->left, rect->top, rect->right-rect->left, rect->bottom-rect->top,
		(DWORD)0x00FA0089);			//DPo - raster code

	// Restore the device context
	SelectObject( hDC, hOldBrush );
	SetBkColor( hDC, clrBk );
	SetTextColor( hDC, clrText );
	DeleteObject( bmBrush );
	DeleteObject( brush );
}

HB_FUNC( SHADERECT )
{
   HDC hDC = (HDC) hb_parnl( 1 );
   RECT rect;
   rect.top = hb_parvni( 2, 1 );
   rect.left = hb_parvni( 2, 2 );
   rect.bottom = hb_parvni( 2, 3 );
   rect.right = hb_parvni( 2, 4 );
   ShadeRect( hDC, &rect );
   hb_ret();

}

/*HB_FUNC( SETLAYEREDWINDOWATTRIBUTES )
{
   hb_retl( SetLayeredWindowAttributes( (HWND) hb_parnl( 1 ), (COLORREF) hb_parnl( 2 ),
    (BYTE) hb_parni( 3 ),  (DWORD) hb_parnl( 4 )) );

}*/

void HorizontalGradient(HDC hDC, LPRECT lpRect, COLORREF sColor, COLORREF eColor, BOOL bGamma, double gamma)
{
	// Gradient params
	int width = lpRect->right - lpRect->left - 1;
	// int height = lpRect->bottom - lpRect->top - 1;

	// Draw gradient
	HBRUSH hBrush = NULL;
	double percent;
	unsigned char red, green, blue;
	COLORREF color;
	RECT rect;
	int i;
	for ( i=0; i<width-1; i++)
	{
		// Gradient color percent
		percent = 1 - (double)i / (double)(width-2);

		// Gradient color
		red = (unsigned char)(GetRValue(sColor)*percent) + (unsigned char)(GetRValue(eColor)*(1-percent));
		green = (unsigned char)(GetGValue(sColor)*percent) + (unsigned char)(GetGValue(eColor)*(1-percent));
		blue = (unsigned char)(GetBValue(sColor)*percent) + (unsigned char)(GetBValue(eColor)*(1-percent));
		if (bGamma)
		{
			red = (unsigned char)(pow((double)red/255.0, gamma) * 255);
			green = (unsigned char)(pow((double)green/255.0, gamma) * 255);
			blue = (unsigned char)(pow((double)blue/255.0, gamma) * 255);
		}
		color = RGB(red, green, blue);

		// Gradient
		rect.left = lpRect->left + i + 1;
		rect.top = lpRect->top + 1;
		rect.right = rect.left + 1;
		rect.bottom = lpRect->bottom - 1;
		FillSolidRect( hDC, &rect, color );
		//hBrush = CreateSolidBrush(color);
		//FillRect(hDC, &rect, hBrush);
		//DeleteObject(hBrush);

    HB_SYMBOL_UNUSED( hBrush );
	}
}

HB_FUNC( HORIZONTALGRADIENT )
{
   RECT rct;
   rct.top    = hb_parvni( 2, 1 );
   rct.left   = hb_parvni( 2, 2 );
   rct.bottom = hb_parvni( 2, 3 );
   rct.right  = hb_parvni( 2, 4 );

   HorizontalGradient((HDC) hb_parnl( 1 ), &rct, (COLORREF) hb_parnl( 3 ), (COLORREF) hb_parnl( 4 ), hb_parl( 5 ), hb_parnl( 6 ));
   hb_ret();
}
HB_FUNC( C5_EXTFLOODFILL )
{

   hb_retl( ExtFloodFill( (HDC) hb_parnl( 1 ),      // manipulador de contexto de dispositivo
                       hb_parni( 3 ),            // coordenada x donde comienza el llenado
                       hb_parni( 2 ),            // coordenada x donde comienza el llenado
                       (COLORREF) hb_parnl( 4 ),  // color de llenado
                        FLOODFILLSURFACE ));

}

HB_FUNC( DRAWIMAGEBLEND )
{

   typedef void (CALLBACK* MYPROC)(HDC, int, int, int, int, HDC, int, int, int, int, BLENDFUNCTION );

   HDC hDCMem;
   HDC hDC = (HDC) hb_parnl( 1 );
   HBITMAP hOldBmp;
   BLENDFUNCTION blendFunction;
   MYPROC AlphaBlend;
   HINSTANCE hLib = LoadLibrary( "Msimg32.dll" );

   blendFunction.BlendOp = AC_SRC_OVER;
   blendFunction.BlendFlags = 0;
   blendFunction.SourceConstantAlpha = hb_parni( 11 );
   blendFunction.AlphaFormat = 0;

   if( hLib )
   {
      if( ( AlphaBlend = ( MYPROC ) GetProcAddress( hLib, "AlphaBlend" )) )
      {
	       hDCMem = CreateCompatibleDC( hDC );
	       hOldBmp = ( HBITMAP ) SelectObject( hDCMem, ( HBITMAP ) hb_parnl( 2 ) );

	       AlphaBlend( hDC, hb_parni( 4 ), hb_parni( 3 ), hb_parni( 5 ), hb_parni( 6 ),
		                 hDCMem, hb_parni( 8 ), hb_parni( 7 ), hb_parni( 9 ), hb_parni( 10 ), 
		                 blendFunction );

	       SelectObject( hDCMem, hOldBmp );
	       DeleteDC( hDCMem );
      }
      FreeLibrary( hLib );
   }
   hb_retni( 0 );
}

HB_FUNC( GETSIZETEXT )
{
   HDC hDC = ( HDC ) hb_parnl( 1 );
   SIZE sz;
   GetTextExtentPoint32( hDC, hb_parc( 2 ), hb_parclen( 2 ), &sz );
   hb_reta(2);
   hb_storvni( sz.cx, -1, 1 );
   hb_storvni( sz.cy, -1, 2 );
}

