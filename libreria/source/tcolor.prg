#include "fivewin.ch"

CLASS TColor

      DATA aPaleta
      DATA nColorBase

      DATA nClrText
      DATA nClrText1
      DATA nClrTextOf
      DATA nClrTextIn

      DATA nClrPane0
      DATA nClrPane
      DATA nClrBorde
      DATA nClrBorde2
      DATA nClrBorde3
      DATA nClrBorde4
      DATA nClrBorde5
      DATA nClrBanda1

      DATA nClrOver1
      DATA nClrOver2
      DATA nClrOver3
      DATA nClrOver4
      DATA nClrOver5

      DATA nClrSel1
      DATA nClrSel2
      DATA nClrSel3
      DATA nClrSel4
      DATA nClrSel5
      DATA nClrSel6

      DATA nClrBtn1
      DATA nClrBtn2
      DATA nClrBtn3
      DATA nClrBtn4
      DATA nClrBtn5

      DATA nClrText
      DATA nClrText1
      DATA nClrTextOff
      DATA nClrTextInv

      DATA nClrTab1
      DATA nClrTab2
      DATA nClrTab3
      DATA nClrTab4



      METHOD New() CONSTRUCTOR

      METHOD nEntradas  INLINE len( ::aPaleta )
      METHOD CreatePaleta( nColor )

      METHOD nGetColor( nPorcentaje ) INLINE ::aPaleta[ nPorcentaje ]


ENDCLASS

METHOD New( nColor ) CLASS TColor

  ::aPaleta := array( 100 )
  ::CreatePaleta( nColor )

return self

******************************************************************************************************
  METHOD CreatePaleta( nColor ) CLASS TColor
******************************************************************************************************


   local n
   local nCount := 0
   local nTop := 0
   local nR, nG, nB
   local nRed, nGreen, nBlue
   local nIni := ::nEntradas / 2
   local nEnd := nIni + ::nEntradas % 2


   ::nColorBase := nColor

   n := 0

   nRed   := GetRValue( ::nColorBase )
   nGreen := GetGValue( ::nColorBase )
   nBlue  := GetBValue( ::nColorBase )

   nR     := nRed    / nIni
   nG     := nGreen  / nIni
   nB     := nBlue   / nIni

   for n := nIni to 1 step -1

      nRed   -= nR
      nGreen -= nG
      nBlue  -= nB

      ::aPaleta[n] := RGB( int(nRed), int(nGreen), int(nBlue) )

   next



   nRed   := GetRValue( ::nColorBase )
   nGreen := GetGValue( ::nColorBase )
   nBlue  := GetBValue( ::nColorBase )

   nR     := (255-nRed)    / nEnd
   nG     := (255-nGreen)  / nEnd
   nB     := (255-nBlue)   / nEnd


   for n := nEnd to ::nEntradas

      nRed   += nR
      nGreen += nG
      nBlue  += nB

      ::aPaleta[n] := RGB( int(nRed), int(nGreen), int(nBlue) )

   next

return 0









