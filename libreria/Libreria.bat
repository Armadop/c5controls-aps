REM @echo off
rem

rem


SET HB_DIR=c:\harbour
SET BCC_DIR=c:\bcc55
SET FW_DIR=c:\fwh


if "%1" == "clean" goto CLEAN
if "%1" == "CLEAN" goto CLEAN

:BUILD

   %BCC_DIR%\bin\make -flibh.mak %1 %2 %3 >> make_libh.log

   if errorlevel 1 goto BUILD_ERR



:BUILD_OK

   goto EXIT

:BUILD_ERR

   notepad make_libh.log
   goto EXIT

:CLEAN

   if exist lib\*.lib del lib\*.lib
   if exist source\obj\*.obj del source\obj\*.obj
   if exist source\obj\*.x   del source\obj\*.x
   goto EXIT

:EXIT

del *.log